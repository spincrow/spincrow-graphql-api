# Spincrow api
> This is the backend powering the beast, we run on graphql, so keep it busy.

## Installing and setting up.
### Pre-requistes
There just a few things that need to be installed on your device before we can proceed.
- Prisma cli -v1.34
- node (of course)
- docker (needed to set up a docker instance with a base prisma server running on postgre)
