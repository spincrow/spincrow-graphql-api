"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _this = this;
exports.__esModule = true;
// @ts-ignore
var rave_service_1 = __importDefault(require("./rave-service"));
var utils_1 = require("../utils");
var ip_1 = __importDefault(require("ip"));
// Process funds and funding operations.
exports.processWalletFunding = function (ctx, wallet_id, amount) { return __awaiter(_this, void 0, void 0, function () {
    var wallet, updatedWallet, err_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 5, , 6]);
                return [4 /*yield*/, ctx.prisma.wallet({ id: wallet_id })];
            case 1:
                wallet = _a.sent();
                if (!(wallet.amount >= amount)) return [3 /*break*/, 3];
                return [4 /*yield*/, ctx.prisma.updateWallet({
                        where: {
                            id: wallet_id
                        },
                        data: {
                            amount: wallet.amount - amount
                        }
                    })];
            case 2:
                updatedWallet = _a.sent();
                return [2 /*return*/, updatedWallet];
            case 3: throw new Error("Wallet balance is too low, try topping it up");
            case 4: return [3 /*break*/, 6];
            case 5:
                err_1 = _a.sent();
                throw err_1;
            case 6: return [2 /*return*/];
        }
    });
}); };
exports.processCardFunding = function (ctx, card_id, amount) { return __awaiter(_this, void 0, void 0, function () {
    var raveService, txRef, card, response, err_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                raveService = new rave_service_1["default"]();
                txRef = utils_1.generateUUID();
                _a.label = 1;
            case 1:
                _a.trys.push([1, 5, , 6]);
                return [4 /*yield*/, ctx.prisma.card({ id: card_id }).$fragment("{\n            id\n            token\n            owner {\n                email\n                first_name\n                last_name\n            }\n        }")];
            case 2:
                card = _a.sent();
                if (!card) return [3 /*break*/, 4];
                return [4 /*yield*/, raveService.chargeCard({
                        amount: amount,
                        token: card.token,
                        email: card.owner.email,
                        IP: ip_1["default"].address(),
                        txRef: txRef,
                        currency: "NGN",
                        country: "NG",
                        narration: "Charging a card"
                    })];
            case 3:
                response = _a.sent();
                return [2 /*return*/, response];
            case 4: return [3 /*break*/, 6];
            case 5:
                err_2 = _a.sent();
                throw err_2;
            case 6: return [2 /*return*/];
        }
    });
}); };
exports.withdrawFunds = function (ctx, input) { return __awaiter(_this, void 0, void 0, function () {
    var raveService, txRef, transferData, response, e_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                raveService = new rave_service_1["default"]();
                txRef = utils_1.generateUUID();
                transferData = {
                    "account_bank": input.bank_code,
                    "account_number": input.account_number,
                    "amount": parseInt(input.amount),
                    "currency": "NGN",
                    "reference": txRef,
                    "beneficiary_name": input.account_name,
                    "destination_branch_code": input.bank_code
                };
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, raveService.transferFunds(JSON.stringify(transferData))];
            case 2:
                response = _a.sent();
                if (response.status == "error") {
                    throw new Error(response.message);
                }
                if (response.status == "success") {
                    return [2 /*return*/, { status: "success", txRef: transferData.reference }];
                }
                return [3 /*break*/, 4];
            case 3:
                e_1 = _a.sent();
                throw e_1;
            case 4: return [2 /*return*/];
        }
    });
}); };
//# sourceMappingURL=fund-service.js.map