"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var redis_1 = __importDefault(require("redis"));
var twilio_1 = __importDefault(require("twilio"));
var logger_1 = __importDefault(require("./logger"));
var client = null;
if (process.env.REDIS_URL) {
    client = redis_1["default"].createClient({
        url: process.env.REDIS_URL
    });
}
else {
    client = redis_1["default"].createClient(parseInt(process.env.REDIS_PORT), process.env.REDIS_HOST);
}
client.on('connect', function () {
    logger_1["default"].info('Redis client connected');
});
client.on("error", function (err) {
    logger_1["default"].info(err);
});
var SMSVerify = /** @class */ (function () {
    function SMSVerify(twilloClient, sendingPhonenumber, appHash) {
        this.twilloClient = twilloClient;
        this.sendingPhoneNumber = sendingPhonenumber;
        this.appHash = appHash;
    }
    SMSVerify.prototype.generateOneTimeCode = function () {
        var codelength = 6;
        return Math.floor(Math.random() * (Math.pow(10, (codelength - 1)) * 9)) + Math.pow(10, (codelength - 1));
    };
    SMSVerify.prototype.getExpiration = function () {
        return 900;
    };
    SMSVerify.prototype.request = function (phone) {
        var _this = this;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var otp, smsMessage, message, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        logger_1["default"].info('Requesting SMS to be sent to ' + phone);
                        otp = this.generateOneTimeCode();
                        client.set(phone, otp.toString());
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        smsMessage = "Hello, welcome to spincrow, please use the code " + otp + " as your verification code";
                        logger_1["default"].info(smsMessage);
                        return [4 /*yield*/, this.twilloClient.messages.create({
                                to: phone,
                                from: this.sendingPhoneNumber,
                                body: smsMessage
                            })];
                    case 2:
                        message = _a.sent();
                        if (message) {
                            logger_1["default"].info(message.sid);
                            resolve(message);
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        reject(e_1);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        }); });
    };
    SMSVerify.prototype.normalRequest = function (phone, messageToSend) {
        var _this = this;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var otp, smsMessage, message, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        logger_1["default"].info('Requesting SMS to be sent to ' + phone);
                        otp = this.generateOneTimeCode();
                        client.set(phone, otp.toString());
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        smsMessage = messageToSend;
                        logger_1["default"].info(smsMessage);
                        return [4 /*yield*/, this.twilloClient.messages.create({
                                to: phone,
                                from: this.sendingPhoneNumber,
                                body: smsMessage
                            })];
                    case 2:
                        message = _a.sent();
                        if (message) {
                            logger_1["default"].info(message.sid);
                            resolve(message);
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_2 = _a.sent();
                        reject(e_2);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        }); });
    };
    SMSVerify.prototype.verify = function (phone, otpCode) {
        var _this = this;
        logger_1["default"].info('Verifying ' + phone + ':' + otpCode);
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var otp;
            return __generator(this, function (_a) {
                otp = client.get(phone, function (err, data) {
                    logger_1["default"].info(data);
                    if (err) {
                        reject(err);
                    }
                    if (data == null) {
                        logger_1["default"].info('No cached otp value found for phone: ' + phone);
                        resolve(false);
                    }
                    if (data == otpCode) {
                        logger_1["default"].info('Found otp value in cache');
                        resolve(true);
                    }
                    else {
                        logger_1["default"].info('Mismatch between otp value found and otp value expected');
                        resolve(false);
                    }
                });
                return [2 /*return*/];
            });
        }); });
    };
    SMSVerify.prototype.reset = function (phone) {
        logger_1["default"].info('Resetting code for:  ' + phone);
        var otp = client.get(phone);
        if (otp == null) {
            logger_1["default"].info('No cached otp value found for phone: ' + phone);
            return false;
        }
        client.srem(phone);
        return true;
    };
    return SMSVerify;
}());
var SMSVerification = /** @class */ (function () {
    function SMSVerification() {
        this.sendingPhoneNumber = process.env.SENDING_PHONE_NUMBER;
        this.twilioApiKey = process.env.TWILIO_API_KEY;
        this.twilioApiSecret = process.env.TWILIO_API_SECRET;
        this.twilioAccountSID = process.env.TWILIO_ACCOUNT_SID;
        this.appHash = process.env.APP_HASH;
        this.twilloClient = twilio_1["default"](this.twilioApiKey, this.twilioApiSecret, {
            accountSid: this.twilioAccountSID
        });
        this.SMSVerify = new SMSVerify(this.twilloClient, this.sendingPhoneNumber, this.appHash);
    }
    SMSVerification.prototype.smsRequest = function (phone) {
        return __awaiter(this, void 0, void 0, function () {
            var e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.SMSVerify.request(phone)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, { status: true, time: this.SMSVerify.getExpiration() }];
                    case 2:
                        e_3 = _a.sent();
                        throw e_3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SMSVerification.prototype.verify = function (phone, code) {
        return this.SMSVerify.verify(phone, code);
    };
    SMSVerification.prototype.request = function (phone, message) {
        return __awaiter(this, void 0, void 0, function () {
            var e_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.SMSVerify.normalRequest(phone, message)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, { status: true }];
                    case 2:
                        e_4 = _a.sent();
                        throw e_4;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    return SMSVerification;
}());
exports.SMSVerification = SMSVerification;
//# sourceMappingURL=sms-verfication.js.map