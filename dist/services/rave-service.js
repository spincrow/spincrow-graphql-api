"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var ravepay_1 = __importDefault(require("@legobox/ravepay"));
var request_1 = __importDefault(require("request"));
var logger_1 = __importDefault(require("./logger"));
var RaveService = /** @class */ (function () {
    function RaveService() {
        this.raveBankApi = process.env.RAVE_BASE_URL;
        if ((process.env.RAVE_PRODUCTION == 'true')) {
            this.ravepayObj = new ravepay_1["default"](process.env.RAVE_PUBLIC_KEY, process.env.RAVE_SECRET_KEY, (process.env.RAVE_PRODUCTION == 'true'));
        }
        else {
            this.ravepayObj = new ravepay_1["default"](process.env.RAVE_PUBLIC_TEST_KEY, process.env.RAVE_SECRET_TEST_KEY, (process.env.RAVE_PRODUCTION == 'true'));
        }
    }
    RaveService.prototype.verifyTransactionReference = function (refumber) {
        return __awaiter(this, void 0, void 0, function () {
            var payload, response, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        payload = {
                            txref: refumber
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        logger_1["default"].info(JSON.stringify(payload));
                        return [4 /*yield*/, this.ravepayObj.VerifyTransaction.verify(payload)];
                    case 2:
                        response = _a.sent();
                        return [2 /*return*/, response];
                    case 3:
                        err_1 = _a.sent();
                        logger_1["default"].error(err_1.message);
                        return [2 /*return*/, err_1];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    RaveService.prototype.getBankAccounts = function () {
        var _this = this;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var options;
            return __generator(this, function (_a) {
                try {
                    options = {
                        'method': 'GET',
                        'url': this.raveBankApi + "/banks/NG",
                        'headers': {
                            'Authorization': "Bearer " + process.env.RAVE_SECRET_KEY
                        }
                    };
                    // let resp = await request.get(options.url, options)
                    request_1["default"](options, function (error, response) {
                        if (error)
                            throw new Error(error);
                        var data = JSON.parse(response.body).data.map(function (item) { return ({
                            bankname: item.name,
                            bankcode: item.code,
                            internetbanking: true
                        }); });
                        resolve(data);
                    });
                }
                catch (e) {
                    logger_1["default"].error(e.message);
                    reject(e);
                }
                return [2 /*return*/];
            });
        }); });
    };
    RaveService.prototype.getCardToken = function (txRef) {
        return __awaiter(this, void 0, void 0, function () {
            var payload, response, err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        payload = {
                            txref: txRef
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        logger_1["default"].info(JSON.stringify(payload));
                        return [4 /*yield*/, this.ravepayObj.VerifyTransaction.verify(payload)];
                    case 2:
                        response = _a.sent();
                        if (response.data.card) {
                            return [2 /*return*/, {
                                    token: response.data.card.card_tokens[0].embedtoken,
                                    expirymonth: response.data.card.expirymonth,
                                    expiryyear: response.data.card.expiryyear,
                                    last_numbers: response.data.card.last4digits,
                                    type: response.data.card.type
                                }];
                        }
                        else {
                            throw new Error("Card data missing");
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        err_2 = _a.sent();
                        logger_1["default"].error(err_2.message);
                        throw err_2;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    RaveService.prototype.chargeCard = function (input) {
        return __awaiter(this, void 0, void 0, function () {
            var resp, err_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        logger_1["default"].info(JSON.stringify(input));
                        return [4 /*yield*/, this.ravepayObj.TokenCharge.card(input)];
                    case 1:
                        resp = _a.sent();
                        return [2 /*return*/, resp];
                    case 2:
                        err_3 = _a.sent();
                        logger_1["default"].error(err_3.message);
                        throw err_3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    RaveService.prototype.transferFunds = function (input) {
        return __awaiter(this, void 0, void 0, function () {
            var resp, err_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        logger_1["default"].info(JSON.stringify(input));
                        return [4 /*yield*/, this.ravepayObj.Transfer.initiate(JSON.parse(input))];
                    case 1:
                        resp = _a.sent();
                        return [2 /*return*/, resp];
                    case 2:
                        err_4 = _a.sent();
                        logger_1["default"].error(err_4.message);
                        throw err_4;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    RaveService.prototype.createPermanentVA = function (input) {
        return __awaiter(this, void 0, void 0, function () {
            var resp, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        logger_1["default"].info(JSON.stringify(input));
                        return [4 /*yield*/, this.ravepayObj.VirtualAccount.accountNumber(input)];
                    case 1:
                        resp = _a.sent();
                        logger_1["default"].info(JSON.stringify(resp));
                        return [2 /*return*/, resp];
                    case 2:
                        e_1 = _a.sent();
                        logger_1["default"].error(e_1.message);
                        throw e_1;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    return RaveService;
}());
exports["default"] = RaveService;
//# sourceMappingURL=rave-service.js.map