"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _this = this;
exports.__esModule = true;
var prisma_client_1 = require("../generated/prisma-client");
var utils_1 = require("../utils");
var logger_1 = __importDefault(require("./logger"));
var monnify_service_1 = __importDefault(require("./monnify-service"));
exports.generateAccountNoForWallet = function (input) { return __awaiter(_this, void 0, void 0, function () {
    var user, wallet, monnifyService, accountReference, monnifyAccountResponse, e_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 6, , 7]);
                return [4 /*yield*/, prisma_client_1.prisma.user({ id: input.user_id })];
            case 1:
                user = _a.sent();
                return [4 /*yield*/, prisma_client_1.prisma.wallet({ id: input.wallet_id })];
            case 2:
                wallet = _a.sent();
                monnifyService = new monnify_service_1["default"]();
                accountReference = utils_1.generateUUID();
                if (!wallet) return [3 /*break*/, 5];
                return [4 /*yield*/, monnifyService.createWalletAccountNo({
                        accountReference: accountReference,
                        name: user.first_name,
                        email: input.email,
                        owner_name: user.first_name + " " + user.last_name
                    })];
            case 3:
                monnifyAccountResponse = _a.sent();
                console.log(monnifyAccountResponse);
                return [4 /*yield*/, prisma_client_1.prisma.updateWallet({
                        where: {
                            id: input.wallet_id
                        },
                        data: {
                            va_account_number: monnifyAccountResponse.responseBody.accountNumber,
                            va_bank_name: monnifyAccountResponse.responseBody.bankName,
                            va_created_at: new Date(monnifyAccountResponse.responseBody.createdOn),
                            va_note: "monnify wallet account no",
                            va_account_name: monnifyAccountResponse.responseBody.accountName,
                            va_order_ref: monnifyAccountResponse.responseBody.accountReference
                        }
                    })
                    // an account no had been created for your wallet.
                ];
            case 4:
                _a.sent();
                _a.label = 5;
            case 5: return [3 /*break*/, 7];
            case 6:
                e_1 = _a.sent();
                logger_1["default"].error(e_1.message);
                return [3 /*break*/, 7];
            case 7: return [2 /*return*/];
        }
    });
}); };
//# sourceMappingURL=extra-jobs.js.map