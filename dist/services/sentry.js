"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
exports.__esModule = true;
var Sentry = __importStar(require("@sentry/node"));
Sentry.init({
    dsn: 'https://c02ab1d4637943aebe300c2b8a2ce849@o412238.ingest.sentry.io/5288680',
    environment: "development"
});
exports["default"] = Sentry;
//# sourceMappingURL=sentry.js.map