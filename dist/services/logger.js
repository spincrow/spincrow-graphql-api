"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var winston_1 = __importDefault(require("winston"));
var winston_sentry_log_1 = __importDefault(require("winston-sentry-log"));
// setting up the logger
var logger = winston_1["default"].createLogger({
    level: 'info',
    format: winston_1["default"].format.json(),
    defaultMeta: {
        service: 'user-service'
    },
    transports: [
        new winston_1["default"].transports.File({ filename: './log/error.log', level: "error" }),
        new winston_1["default"].transports.File({ filename: './log/warning.log', level: "warn" }),
        new winston_1["default"].transports.File({ filename: './log/debug.log', level: "debug" }),
        new winston_1["default"].transports.File({ filename: './log/combined.log' }),
        new winston_sentry_log_1["default"]({
            config: {
                dsn: process.env.SENTRY_LOG_URL,
                environment: process.env.NODE_ENV
            },
            level: "error"
        })
    ]
});
exports["default"] = logger;
//# sourceMappingURL=logger.js.map