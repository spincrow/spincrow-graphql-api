"use strict";
exports.__esModule = true;
var nexus_prisma_1 = require("nexus-prisma");
//@ts-ignore
var ContractTransaction = nexus_prisma_1.prismaObjectType({
    name: "ContractTransaction",
    definition: function (t) {
        t.prismaFields([
            'id',
            'amount',
            'contract',
            'flow',
            'details',
            'status',
            'created_at',
            'updated_at'
        ]);
    }
});
exports.ContractTransaction = ContractTransaction;
var WalletTransaction = nexus_prisma_1.prismaObjectType({
    name: "WalletTransaction",
    definition: function (t) {
        t.prismaFields([
            'id',
            'amount',
            'wallet',
            'flow',
            'details',
            'status',
            'created_at',
            'updated_at'
        ]);
    }
});
exports.WalletTransaction = WalletTransaction;
//# sourceMappingURL=Transaction.js.map