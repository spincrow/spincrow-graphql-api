"use strict";
exports.__esModule = true;
/**
 *
 * @param args
 */
function checkMilestones(args) {
    if (args.input.milestones.length > 0) {
        var contractAmount = 0;
        for (var i in args.input.milestones) {
            contractAmount = contractAmount + args.input.milestones[i].target;
        }
        args.input.amount = contractAmount;
    }
    return args;
}
exports.checkMilestones = checkMilestones;
function checkWholePayments(args) {
    if (args.input.whole_amount) {
        args.input.amount = args.input.whole_amount;
    }
    return args;
}
exports.checkWholePayments = checkWholePayments;
//# sourceMappingURL=index.js.map