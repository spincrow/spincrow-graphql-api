"use strict";
exports.__esModule = true;
var nexus_1 = require("nexus");
var Mutation = nexus_1.mutationType({
    definition: function (t) {
        t.field('signupUser', {
            type: 'User',
            args: {
                first_name: nexus_1.stringArg({ required: true }),
                last_name: nexus_1.stringArg({ required: true }),
                email: nexus_1.stringArg(),
                password: nexus_1.stringArg({ required: true }),
                phone: nexus_1.stringArg()
            },
            resolve: function (parent, args, ctx) {
                return ctx.prisma.createUser({
                    first_name: args.first_name,
                    last_name: args.last_name,
                    email: args.email,
                    password: args.password,
                    phone: args.password
                });
            }
        });
        // t.field('createDraft', {
        //   type: 'Post',
        //   args: {
        //     title: stringArg(),
        //     content: stringArg({ nullable: true }),
        //     authorEmail: stringArg(),
        //   },
        //   resolve: (parent, { title, content, authorEmail }, ctx) => {
        //     return ctx.prisma.createPost({
        //       title,
        //       content,
        //       author: {
        //         connect: { email: authorEmail },
        //       },
        //     })
        //   },
        // })
        // t.field('deletePost', {
        //   type: 'Post',
        //   nullable: true,
        //   args: {
        //     id: idArg(),
        //   },
        //   resolve: (parent, { id }, ctx) => {
        //     return ctx.prisma.deletePost({ id })
        //   },
        // })
        // t.field('publish', {
        //   type: 'Post',
        //   nullable: true,
        //   args: {
        //     id: idArg(),
        //   },
        //   resolve: (parent, { id }, ctx) => {
        //     return ctx.prisma.updatePost({ 
        //       where: { id },
        //       data: { published: true },
        //     })
        //   },
        // })
    }
});
exports["default"] = Mutation;
//# sourceMappingURL=mutation.js.map