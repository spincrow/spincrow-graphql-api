"use strict";
exports.__esModule = true;
var nexus_1 = require("nexus");
var graphql_upload_1 = require("graphql-upload");
exports.AuthPayload = nexus_1.objectType({
    name: "AuthPayload",
    definition: function (t) {
        t.string("token");
        t.field("user", {
            type: "User"
        });
    }
});
exports.ConfirmationResponse = nexus_1.objectType({
    name: "ConfirmResponse",
    definition: function (t) {
        t.boolean("state");
    }
});
exports.BankCode = nexus_1.objectType({
    name: "BankCode",
    definition: function (t) {
        t.string("bankname");
        t.string("bankcode");
        t.boolean("internetbanking");
    }
});
exports.File = nexus_1.objectType({
    name: "File",
    definition: function (t) {
        t.id("id");
        t.string("path");
        t.string("filename");
        t.string("mimetype");
        t.string("encoding");
    }
});
exports.Upload = graphql_upload_1.GraphQLUpload;
exports.Stats = nexus_1.objectType({
    name: "Stats",
    definition: function (t) {
        t.float("wallet_balance"),
            t.int("active_contracts"),
            t.int("completed_contracts");
    }
});
//# sourceMappingURL=extraTypes.js.map