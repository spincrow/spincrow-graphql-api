"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _this = this;
exports.__esModule = true;
var nexus_1 = require("nexus");
var utils_1 = require("../../utils");
var utils_2 = require("../../utils");
var bcrypt_1 = require("bcrypt");
var jsonwebtoken_1 = require("jsonwebtoken");
var input_1 = require("../input");
var agenda_service_1 = __importDefault(require("../../services/agenda-service"));
var bcrypt_2 = require("bcrypt");
var sms_verfication_1 = require("../../services/sms-verfication");
var logger_1 = __importDefault(require("../../services/logger"));
exports.signup = nexus_1.mutationField("signup", {
    type: "AuthPayload",
    args: {
        input: input_1.SignupInput.asArg({ required: true })
    },
    resolve: function (parent, _a, ctx) {
        var input = _a.input;
        return __awaiter(_this, void 0, void 0, function () {
            var hashedPassword, verificationCode, user, userWallet;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, bcrypt_1.hash(input.password, 10)];
                    case 1:
                        hashedPassword = _b.sent();
                        verificationCode = Math.floor(100000 + Math.random() * 900000).toString();
                        return [4 /*yield*/, ctx.prisma.createUser({
                                first_name: input.first_name,
                                last_name: input.last_name,
                                email: input.email,
                                password: hashedPassword,
                                wallet: {
                                    create: {
                                        amount: 0
                                    }
                                },
                                verification_code: verificationCode
                            })];
                    case 2:
                        user = _b.sent();
                        return [4 /*yield*/, ctx.prisma.user({ id: user.id }).wallet()];
                    case 3:
                        userWallet = _b.sent();
                        agenda_service_1["default"].now('generate wallet account no', {
                            input: {
                                wallet_id: userWallet.id,
                                user_id: user.id,
                                email: input.email
                            }
                        });
                        agenda_service_1["default"].now('signup mail', {
                            user: {
                                email: user.email,
                                name: user.first_name,
                                verification: verificationCode
                            },
                            subject: "Verify your mail"
                        });
                        // send mail to user concerning code.
                        return [2 /*return*/, {
                                token: jsonwebtoken_1.sign({ userId: user.id }, utils_1.APP_SECRET),
                                user: user
                            }];
                }
            });
        });
    }
});
exports.login = nexus_1.mutationField("login", {
    type: "AuthPayload",
    args: {
        input: input_1.LoginInput.asArg({ required: true })
    },
    resolve: function (parent, _a, ctx) {
        var input = _a.input;
        return __awaiter(_this, void 0, void 0, function () {
            var user, passwordValid;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, ctx.prisma.user({ email: input.email })];
                    case 1:
                        user = _b.sent();
                        if (!user) {
                            throw new Error("No user found for email: " + input.email);
                        }
                        return [4 /*yield*/, bcrypt_2.compare(input.password, user.password)];
                    case 2:
                        passwordValid = _b.sent();
                        if (!passwordValid) {
                            throw new Error('Invalid password');
                        }
                        return [2 /*return*/, {
                                token: jsonwebtoken_1.sign({ userId: user.id }, utils_1.APP_SECRET),
                                user: user
                            }];
                }
            });
        });
    }
});
exports.confirmCode = nexus_1.mutationField("confirmEmailCode", {
    type: "ConfirmResponse",
    args: {
        input: input_1.ConfirmCodeInput.asArg({ required: true })
    },
    resolve: function (parent, _a, ctx) {
        var input = _a.input;
        return __awaiter(_this, void 0, void 0, function () {
            var userId, user, e_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        userId = utils_1.getUserId(ctx);
                        return [4 /*yield*/, ctx.prisma.user({ id: userId })
                            // confirm the value of the input.
                        ];
                    case 1:
                        user = _b.sent();
                        // confirm the value of the input.
                        if (user.verification_code == input.code) {
                            return [2 /*return*/, {
                                    state: true
                                }];
                        }
                        else {
                            return [2 /*return*/, { state: false }];
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _b.sent();
                        logger_1["default"].error(e_1.message);
                        throw e_1;
                    case 3: return [2 /*return*/];
                }
            });
        });
    }
});
exports.sendPhoneConfirmation = nexus_1.mutationField("sendPhoneConfirmation", {
    type: "ConfirmResponse",
    args: {
        input: "String"
    },
    resolve: function (parents, _a, ctx) {
        var input = _a.input;
        return __awaiter(_this, void 0, void 0, function () {
            var userId, user, SMSVerification, response, err_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 4, , 5]);
                        userId = utils_1.getUserId(ctx);
                        return [4 /*yield*/, ctx.prisma.updateUser({
                                data: {
                                    phone: input
                                },
                                where: {
                                    id: userId
                                }
                            })];
                    case 1:
                        user = _b.sent();
                        if (!user) return [3 /*break*/, 3];
                        SMSVerification = new sms_verfication_1.SMSVerification();
                        return [4 /*yield*/, SMSVerification.smsRequest(input.toString())];
                    case 2:
                        response = _b.sent();
                        if (response.status == true) {
                            return [2 /*return*/, {
                                    state: true
                                }];
                        }
                        _b.label = 3;
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        err_1 = _b.sent();
                        logger_1["default"].error(err_1.message);
                        throw err_1;
                    case 5: return [2 /*return*/];
                }
            });
        });
    }
});
exports.verifySMSCode = nexus_1.mutationField("verifySMSCode", {
    type: "ConfirmResponse",
    args: {
        input: input_1.SMSVerificationInput.asArg({ required: true })
    },
    resolve: function (parents, _a, ctx) {
        var input = _a.input;
        return __awaiter(_this, void 0, void 0, function () {
            var userId, user, SMSVerification, response, err_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 4, , 5]);
                        userId = utils_1.getUserId(ctx);
                        return [4 /*yield*/, ctx.prisma.user({ id: userId })];
                    case 1:
                        user = _b.sent();
                        if (!user) return [3 /*break*/, 3];
                        SMSVerification = new sms_verfication_1.SMSVerification();
                        return [4 /*yield*/, SMSVerification.verify(user.phone, input.code)];
                    case 2:
                        response = _b.sent();
                        if (response == true) {
                            return [2 /*return*/, {
                                    state: true
                                }];
                        }
                        _b.label = 3;
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        err_2 = _b.sent();
                        logger_1["default"].error(err_2.message);
                        throw err_2;
                    case 5: return [2 /*return*/];
                }
            });
        });
    }
});
exports.passwordUpdate = nexus_1.mutationField("passwordUpdate", {
    type: "User",
    args: {
        input: input_1.PasswordUpdate.asArg({ required: true })
    },
    resolve: function (parent, _a, ctx) {
        var input = _a.input;
        return __awaiter(_this, void 0, void 0, function () {
            var USER_ID, user, passwordValid, hashedPassword;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        USER_ID = utils_1.getUserId(ctx);
                        if (!USER_ID) {
                            // update the user's password.
                            throw new Error('User not Authenticated');
                        }
                        return [4 /*yield*/, ctx.prisma.user({ id: USER_ID })
                            // let proceed to update the passowrd.
                        ];
                    case 1:
                        user = _b.sent();
                        return [4 /*yield*/, bcrypt_2.compare(input.currentPassword, user.password)];
                    case 2:
                        passwordValid = _b.sent();
                        if (!passwordValid) {
                            throw new Error('Invalid password');
                        }
                        return [4 /*yield*/, bcrypt_1.hash(input.newPassword, 10)];
                    case 3:
                        hashedPassword = _b.sent();
                        return [4 /*yield*/, ctx.prisma.updateUser({
                                data: {
                                    password: hashedPassword
                                },
                                where: {
                                    id: USER_ID
                                }
                            })];
                    case 4:
                        user = _b.sent();
                        return [2 /*return*/, user];
                }
            });
        });
    }
});
exports.forgotPasswordEnterEmail = nexus_1.mutationField("forgotPasswordEnterEmail", {
    type: "Boolean",
    args: {
        input: input_1.ForgotPasswordEmailInput.asArg({ required: true })
    },
    resolve: function (parent, _a, ctx) {
        var input = _a.input;
        return __awaiter(_this, void 0, void 0, function () {
            var verificationCode, users, user, e_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 5, , 6]);
                        verificationCode = utils_2.generateUUID();
                        return [4 /*yield*/, ctx.prisma.users({ where: {
                                    email: input.email
                                } })];
                    case 1:
                        users = _b.sent();
                        if (!users[0]) return [3 /*break*/, 3];
                        return [4 /*yield*/, ctx.prisma.updateUser({
                                where: {
                                    id: users[0].id
                                },
                                data: {
                                    password_verification_code: verificationCode
                                }
                            })];
                    case 2:
                        user = _b.sent();
                        if (user) {
                            agenda_service_1["default"].now('forgot password mail', {
                                user: {
                                    email: user.email,
                                    name: user.first_name,
                                    verification: verificationCode
                                },
                                subject: "Spincrow - Forgot Password?, Reset it"
                            });
                            return [2 /*return*/, true];
                        }
                        else {
                            throw new Error("Verification code could not be created");
                        }
                        return [3 /*break*/, 4];
                    case 3: throw new Error("no user with that email exists");
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        e_2 = _b.sent();
                        logger_1["default"].error(e_2.message);
                        throw e_2;
                    case 6: return [2 /*return*/];
                }
            });
        });
    }
});
exports.updateForgottenPassword = nexus_1.mutationField("updateForgottenPassword", {
    type: "Boolean",
    args: {
        input: input_1.ForgotPasswordUpdateInput.asArg({ required: true })
    },
    resolve: function (parent, _a, ctx) {
        var input = _a.input;
        return __awaiter(_this, void 0, void 0, function () {
            var users, hashedPassword, e_3;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 6, , 7]);
                        return [4 /*yield*/, ctx.prisma.users({ where: {
                                    password_verification_code: input.verification_code
                                } })];
                    case 1:
                        users = _b.sent();
                        return [4 /*yield*/, bcrypt_1.hash(input.newpassword, 10)];
                    case 2:
                        hashedPassword = _b.sent();
                        if (!users[0]) return [3 /*break*/, 4];
                        // update the user.
                        return [4 /*yield*/, ctx.prisma.updateUser({
                                where: {
                                    id: users[0].id
                                },
                                data: {
                                    password: hashedPassword,
                                    password_verification_code: null
                                }
                            })];
                    case 3:
                        // update the user.
                        _b.sent();
                        return [2 /*return*/, true];
                    case 4: throw new Error("The verification code doesn't belong to any user.");
                    case 5: return [3 /*break*/, 7];
                    case 6:
                        e_3 = _b.sent();
                        logger_1["default"].error(e_3.message);
                        throw e_3;
                    case 7: return [2 /*return*/];
                }
            });
        });
    }
});
//# sourceMappingURL=auth-mutations.js.map