"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _this = this;
exports.__esModule = true;
// @ts-nocheck
var nexus_1 = require("nexus");
var utils_1 = require("../../utils");
var input_1 = require("../input");
var agenda_service_1 = __importDefault(require("../../services/agenda-service"));
var helpers_1 = require("../helpers");
var rave_service_1 = __importDefault(require("../../services/rave-service"));
var logger_1 = __importDefault(require("../../services/logger"));
/**
 * Creating a contract
 */
exports.updateUserInformation = nexus_1.mutationField("createContract", {
    type: "Contract",
    args: {
        input: input_1.ContractInput.asArg({ required: true })
    },
    resolve: function (parents, args, ctx) { return __awaiter(_this, void 0, void 0, function () {
        var userId, client_code, user, contract, err_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    userId = utils_1.getUserId(ctx), client_code = Math.floor(100000 + Math.random() * 900000);
                    args = helpers_1.checkMilestones(args);
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 4, , 5]);
                    return [4 /*yield*/, ctx.prisma.users({ where: { id: userId } })];
                case 2:
                    user = _a.sent();
                    return [4 /*yield*/, ctx.prisma.createContract(__assign({}, args.input, { owner: { connect: { id: userId } }, milestones: { create: args.input.milestones }, client_code: client_code.toString() }))];
                case 3:
                    contract = _a.sent();
                    if (contract) {
                        agenda_service_1["default"].now('send contract mail', {
                            user: {
                                email: args.input.client_email,
                                name: args.input.client
                            },
                            contract: contract,
                            subject: "New mail from contracts creation"
                        });
                        return [2 /*return*/, contract];
                    }
                    else {
                        throw new Error("Something went horribly wrong.");
                    }
                    return [3 /*break*/, 5];
                case 4:
                    err_1 = _a.sent();
                    logger_1["default"].error(err_1.message);
                    throw err_1;
                case 5: return [2 /*return*/];
            }
        });
    }); }
});
exports.addCard = nexus_1.mutationField("addCard", {
    type: "Card",
    args: {
        input: input_1.CardInput.asArg({ required: true })
    },
    resolve: function (parents, _a, ctx) {
        var input = _a.input;
        return __awaiter(_this, void 0, void 0, function () {
            var USER_ID, user, raveService, resp, card, currentWalletAmount, e_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        USER_ID = utils_1.getUserId(ctx);
                        if (!USER_ID) return [3 /*break*/, 9];
                        return [4 /*yield*/, ctx.prisma.user({ id: USER_ID }).$fragment("{\n                id \n                email\n                wallet {\n                    id\n                    amount\n                }\n            }")];
                    case 1:
                        user = _b.sent();
                        _b.label = 2;
                    case 2:
                        _b.trys.push([2, 7, , 8]);
                        raveService = new rave_service_1["default"]();
                        return [4 /*yield*/, raveService.getCardToken(input.txRef)];
                    case 3:
                        resp = _b.sent();
                        if (!resp) return [3 /*break*/, 6];
                        return [4 /*yield*/, ctx.prisma.createCard({
                                expirymonth: resp.expirymonth,
                                expiryyear: resp.expiryyear,
                                owner: {
                                    connect: {
                                        id: USER_ID
                                    }
                                },
                                token: resp.token,
                                last_number: resp.last_numbers,
                                type: resp.type
                            })];
                    case 4:
                        card = _b.sent();
                        currentWalletAmount = user.wallet.amount;
                        return [4 /*yield*/, ctx.prisma.updateWallet({
                                data: {
                                    amount: currentWalletAmount + 100,
                                    transactions: {
                                        create: {
                                            amount: parseFloat("100"),
                                            flow: "CREDIT",
                                            details: "Added a new card",
                                            status: "SUCCESSFUL"
                                        }
                                    }
                                },
                                where: {
                                    id: user.wallet.id
                                }
                            })];
                    case 5:
                        _b.sent();
                        return [2 /*return*/, card];
                    case 6: return [3 /*break*/, 8];
                    case 7:
                        e_1 = _b.sent();
                        throw e_1;
                    case 8: return [3 /*break*/, 10];
                    case 9: throw new Error("UNAUTHENTICATED USER");
                    case 10: return [2 /*return*/];
                }
            });
        });
    }
});
exports.addBankAccount = nexus_1.mutationField("addBankAccount", {
    type: "BankAccount",
    args: {
        input: input_1.BankAccountInput.asArg({ required: true })
    },
    resolve: function (parents, args, ctx) { return __awaiter(_this, void 0, void 0, function () {
        var USER_ID, user, account;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    USER_ID = utils_1.getUserId(ctx);
                    if (!USER_ID) return [3 /*break*/, 3];
                    return [4 /*yield*/, ctx.prisma.user({ id: USER_ID })];
                case 1:
                    user = _a.sent();
                    return [4 /*yield*/, ctx.prisma.createBankAccount(__assign({}, args.input, { owner: {
                                connect: {
                                    id: USER_ID
                                }
                            } }))];
                case 2:
                    account = _a.sent();
                    return [2 /*return*/, account];
                case 3: throw new Error("UNAUTHENTICATED USER");
            }
        });
    }); }
});
exports.deleteCard = nexus_1.mutationField("deleteCard", {
    type: "Card",
    args: {
        input: "ID"
    },
    resolve: function (parents, _a, ctx) {
        var input = _a.input;
        return __awaiter(_this, void 0, void 0, function () {
            var USER_ID, user, card, err_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        USER_ID = utils_1.getUserId(ctx);
                        if (!USER_ID) return [3 /*break*/, 6];
                        return [4 /*yield*/, ctx.prisma.user({ id: USER_ID })];
                    case 1:
                        user = _b.sent();
                        _b.label = 2;
                    case 2:
                        _b.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, ctx.prisma.deleteCard({ id: input })];
                    case 3:
                        card = _b.sent();
                        return [2 /*return*/, card];
                    case 4:
                        err_2 = _b.sent();
                        throw new Error("Card doesn't exist for this user.");
                    case 5: return [3 /*break*/, 7];
                    case 6: throw new Error("UNAUTHENTICATED USER");
                    case 7: return [2 /*return*/];
                }
            });
        });
    }
});
exports.deleteBankAccount = nexus_1.mutationField("deleteBankAccount", {
    type: "BankAccount",
    args: {
        input: "ID"
    },
    resolve: function (parents, _a, ctx) {
        var input = _a.input;
        return __awaiter(_this, void 0, void 0, function () {
            var USER_ID, user, account;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        USER_ID = utils_1.getUserId(ctx);
                        if (!USER_ID) return [3 /*break*/, 3];
                        return [4 /*yield*/, ctx.prisma.user({ id: USER_ID })];
                    case 1:
                        user = _b.sent();
                        return [4 /*yield*/, ctx.prisma.deleteBankAccount({ id: input })];
                    case 2:
                        account = _b.sent();
                        return [2 /*return*/, account];
                    case 3: throw new Error("UNAUTHENTICATED USER");
                }
            });
        });
    }
});
//# sourceMappingURL=settings-mutations.js.map