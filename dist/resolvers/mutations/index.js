"use strict";
exports.__esModule = true;
var auth_mutations_1 = require("./auth-mutations");
var contracts_mutations_1 = require("./contracts-mutations");
var tour_mutations_1 = require("./tour-mutations");
var settings_mutations_1 = require("./settings-mutations");
var profile_mutations_1 = require("./profile-mutations");
var wallet_mutation_1 = require("./wallet-mutation");
exports.Mutation = {
    signup: auth_mutations_1.signup,
    login: auth_mutations_1.login,
    updateForgottenPassword: auth_mutations_1.updateForgottenPassword,
    forgotPasswordEnterEmail: auth_mutations_1.forgotPasswordEnterEmail,
    addCard: settings_mutations_1.addCard,
    topUpWallet: wallet_mutation_1.topUpWallet,
    withdrawFunds: wallet_mutation_1.withdrawFunds,
    deleteCard: settings_mutations_1.deleteCard,
    sendPhoneConfirmation: auth_mutations_1.sendPhoneConfirmation,
    verifySMSCode: auth_mutations_1.verifySMSCode,
    confirmCode: auth_mutations_1.confirmCode,
    createContract: contracts_mutations_1.createContract,
    addContractClientAccount: contracts_mutations_1.addContractClientAccount,
    withdrawFromContractToAccount: contracts_mutations_1.withdrawFromContractToAccount,
    withdrawFromMilestoneToAccount: contracts_mutations_1.withdrawFromMilestoneToAccount,
    uploadProfilePic: profile_mutations_1.uploadProfilePic,
    addBankAccount: settings_mutations_1.addBankAccount,
    deleteBankAccount: settings_mutations_1.deleteBankAccount,
    passwordUpdate: auth_mutations_1.passwordUpdate,
    updateProfile: profile_mutations_1.updateProfile,
    updateContractMilestone: contracts_mutations_1.updateContractMilestone,
    disburseContractMilestoneForClient: contracts_mutations_1.disburseContractMilestoneForClient,
    fundContractAsClient: contracts_mutations_1.fundContractAsClient,
    fundWithCard: contracts_mutations_1.fundWithCard,
    disburseContractWholePayment: contracts_mutations_1.disburseContractWholePayment,
    fundContractAsUser: contracts_mutations_1.fundContractAsUser,
    homeTourUpdate: tour_mutations_1.homeTourUpdate,
    contractsTourUpdate: tour_mutations_1.contractsTourUpdate,
    singleContractTourUpdate: tour_mutations_1.singleContractTourUpdate,
    walletTourUpdate: tour_mutations_1.walletTourUpdate,
    requestRetractionAsClient: contracts_mutations_1.requestRetractionAsClient,
    requestRetractionAsUser: contracts_mutations_1.requestRetractionAsUser,
    approveRetractionAsClient: contracts_mutations_1.approveRetractionAsClient,
    approveRetractionAsUser: contracts_mutations_1.approveRetractionAsUser,
    cancelRetractionAsClient: contracts_mutations_1.cancelRetractionAsClient,
    cancelRetractionAsUser: contracts_mutations_1.cancelRetractionAsUser
};
exports["default"] = exports.Mutation;
//# sourceMappingURL=index.js.map