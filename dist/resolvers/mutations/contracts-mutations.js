"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _this = this;
exports.__esModule = true;
// @ts-nocheck
var nexus_1 = require("nexus");
var utils_1 = require("../../utils");
var input_1 = require("../input");
var monnify_service_1 = __importDefault(require("../../services/monnify-service"));
var agenda_service_1 = __importDefault(require("../../services/agenda-service"));
var helpers_1 = require("../helpers");
var logger_1 = __importDefault(require("../../services/logger"));
var rave_service_1 = __importDefault(require("../../services/rave-service"));
var utils_2 = require("../../utils");
var fund_service_1 = require("../../services/fund-service");
/**
 * Creating a contract
 */
exports.createContract = nexus_1.mutationField("createContract", {
    type: "Contract",
    args: {
        input: input_1.ContractInput.asArg({ required: true })
    },
    resolve: function (parents, args, ctx) { return __awaiter(_this, void 0, void 0, function () {
        var userId, client_code, user, monnifyService, accountReference, monnifyAccountResponse, contract, err_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    userId = utils_1.getUserId(ctx), client_code = Math.floor(100000 + Math.random() * 900000);
                    switch (args.input.payment_type) {
                        case "whole":
                            args = helpers_1.checkWholePayments(args);
                            break;
                        case "spread":
                            args = helpers_1.checkMilestones(args);
                            break;
                        default:
                            break;
                    }
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 7, , 8]);
                    return [4 /*yield*/, ctx.prisma.user({ id: userId })
                        // get virtual accounts here
                    ];
                case 2:
                    user = _a.sent();
                    monnifyService = new monnify_service_1["default"]();
                    accountReference = utils_2.generateBaseUUID();
                    return [4 /*yield*/, monnifyService.createContractAccountNo({
                            email: user.email,
                            accountReference: accountReference,
                            name: args.input.name.replace(/ /g, "_"),
                            owner_name: user.first_name + " " + user.last_name
                        })];
                case 3:
                    monnifyAccountResponse = _a.sent();
                    if (!monnifyAccountResponse.requestSuccessful) return [3 /*break*/, 5];
                    return [4 /*yield*/, ctx.prisma.createContract(__assign({}, args.input, { va_created_at: new Date(monnifyAccountResponse.responseBody.createdOn), va_account_number: monnifyAccountResponse.responseBody.accountNumber, va_order_ref: monnifyAccountResponse.responseBody.accountReference, va_account_name: monnifyAccountResponse.responseBody.accountName, va_bank_name: monnifyAccountResponse.responseBody.bankName, va_note: "monnify contract account no", owner: { connect: { id: userId } }, milestones: { create: args.input.milestones }, client_code: client_code.toString() }))];
                case 4:
                    contract = _a.sent();
                    if (contract) {
                        agenda_service_1["default"].now('send contract mail', {
                            user: {
                                email: args.input.client_email,
                                name: args.input.client
                            },
                            contract: contract,
                            owner: user,
                            subject: "New mail from contracts creation"
                        });
                        return [2 /*return*/, contract];
                    }
                    else {
                        throw new Error("Something went horribly wrong.");
                    }
                    return [3 /*break*/, 6];
                case 5: throw new Error("Couldn't create your contract's account no");
                case 6: return [3 /*break*/, 8];
                case 7:
                    err_1 = _a.sent();
                    logger_1["default"].error(err_1.message);
                    throw err_1;
                case 8: return [2 /*return*/];
            }
        });
    }); }
});
exports.updateContract = nexus_1.mutationField("updateContract", {
    type: "Contract",
    args: {
        input: input_1.ContractUpdateInput
    },
    resolve: function (parent, args, ctx) { return __awaiter(_this, void 0, void 0, function () {
        var contract;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, ctx.prisma.updateContract({
                        where: {
                            id: args.input.contract_id
                        },
                        data: __assign({}, args.input.data)
                    })];
                case 1:
                    contract = _a.sent();
                    return [2 /*return*/, contract];
            }
        });
    }); }
});
exports.updateContractMilestone = nexus_1.mutationField("updateContractMilestone", {
    type: "MileStone",
    args: {
        input: input_1.MilestoneUpdateInput
    },
    resolve: function (parents, args, ctx) { return __awaiter(_this, void 0, void 0, function () {
        var milestone;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, ctx.prisma.updateMileStone({
                        where: {
                            id: args.input.milestone_id
                        },
                        data: {
                            progress: args.input.progress,
                            state: args.input.state
                        }
                    })];
                case 1:
                    milestone = _a.sent();
                    return [2 /*return*/, milestone];
            }
        });
    }); }
});
// disburse funds for client milestones
exports.disburseContractMilestoneForClient = nexus_1.mutationField("disburseContractMilestoneForClient", {
    type: "Contract",
    args: {
        input: input_1.MilestoneUpdateInputClient
    },
    resolve: function (parents, args, ctx) { return __awaiter(_this, void 0, void 0, function () {
        var updatedContract, milestone, contract, user, _a, updatedContract, e_1;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _b.trys.push([0, 11, , 12]);
                    updatedContract = null;
                    return [4 /*yield*/, ctx.prisma.updateMileStone({
                            where: {
                                id: args.input.milestone_id
                            },
                            data: {
                                disburseStatus: args.input.disburseStatus,
                                status: args.input.status
                            }
                        })];
                case 1:
                    milestone = _b.sent();
                    return [4 /*yield*/, ctx.prisma.contract({ id: args.input.contract_id }).$fragment("{ id amount owner {id first_name} owner_type milestones { disburseStatus } name status client_phone client client_email}")];
                case 2:
                    contract = _b.sent();
                    return [4 /*yield*/, ctx.prisma.user({ id: contract.owner.id }).$fragment("{id wallet {amount id} first_name last_name}")];
                case 3:
                    user = _b.sent();
                    _a = contract.owner_type;
                    switch (_a) {
                        case "buyer": return [3 /*break*/, 4];
                        case "seller": return [3 /*break*/, 6];
                    }
                    return [3 /*break*/, 8];
                case 4: return [4 /*yield*/, buyerDisbursementMilestoneProcess(ctx, user, milestone, contract)];
                case 5:
                    _b.sent();
                    return [3 /*break*/, 9];
                case 6: return [4 /*yield*/, sellerDisbursementMilestoneProcess(ctx, user, milestone, contract)];
                case 7:
                    _b.sent();
                    return [3 /*break*/, 9];
                case 8: return [3 /*break*/, 9];
                case 9: return [4 /*yield*/, ctx.prisma.contract({ id: args.input.contract_id })];
                case 10:
                    updatedContract = _b.sent();
                    return [2 /*return*/, updatedContract];
                case 11:
                    e_1 = _b.sent();
                    logger_1["default"].error(e_1.message);
                    throw e_1;
                case 12: return [2 /*return*/];
            }
        });
    }); }
});
// Whole disbursements for buyers and sellers
exports.disburseContractWholePayment = nexus_1.mutationField("disburseContractWholePayment", {
    type: "Contract",
    args: {
        input: input_1.ContractWholeInput.asArg({ required: true })
    },
    resolve: function (parents, args, ctx) { return __awaiter(_this, void 0, void 0, function () {
        var contract, user, _a, updatedContract, e_2;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _b.trys.push([0, 9, , 10]);
                    return [4 /*yield*/, ctx.prisma.contract({ id: args.input.contract_id }).$fragment("{ id amount owner {id first_name} owner_type milestones { disburseStatus } name status client_phone client_email client }")];
                case 1:
                    contract = _b.sent();
                    return [4 /*yield*/, ctx.prisma.user({ id: contract.owner.id }).$fragment("{id wallet {amount id} first_name last_name}")];
                case 2:
                    user = _b.sent();
                    _a = contract.owner_type;
                    switch (_a) {
                        case "buyer": return [3 /*break*/, 3];
                        case "seller": return [3 /*break*/, 5];
                    }
                    return [3 /*break*/, 6];
                case 3: return [4 /*yield*/, buyerDisbursementWholeProcess(ctx, user, args, contract)];
                case 4:
                    _b.sent();
                    return [3 /*break*/, 7];
                case 5:
                    sellerDisbursementWholeProcess(ctx, user, args, contract);
                    return [3 /*break*/, 7];
                case 6: return [3 /*break*/, 7];
                case 7: return [4 /*yield*/, ctx.prisma.contract({ id: args.input.contract_id })];
                case 8:
                    updatedContract = _b.sent();
                    return [2 /*return*/, updatedContract];
                case 9:
                    e_2 = _b.sent();
                    logger_1["default"].error(e_2.message);
                    throw e_2;
                case 10: return [2 /*return*/];
            }
        });
    }); }
});
exports.fundContractAsClient = nexus_1.mutationField("fundContractAsClient", {
    type: "Contract",
    args: {
        input: input_1.ContractStateUpdate.asArg({ required: true })
    },
    resolve: function (parents, args, ctx) { return __awaiter(_this, void 0, void 0, function () {
        var contractUpdated, raveService, resp;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    contractUpdated = null;
                    if (!(args.input.contract_state == 'ACTIVE' && args.input.amount)) return [3 /*break*/, 5];
                    raveService = new rave_service_1["default"]();
                    return [4 /*yield*/, raveService.verifyTransactionReference(args.input.transactionCode)];
                case 1:
                    resp = _a.sent();
                    if (!(resp.data.status == "successful")) return [3 /*break*/, 3];
                    return [4 /*yield*/, ctx.prisma.updateContract({
                            where: {
                                id: args.input.contract_id
                            },
                            data: {
                                status: args.input.contract_state,
                                transactions: {
                                    create: {
                                        amount: args.input.amount,
                                        flow: "CREDIT",
                                        details: "FUNDED CONTRACT",
                                        transaction_code: args.input.transactionCode,
                                        status: "SUCCESSFUL"
                                    }
                                }
                            }
                        })];
                case 2:
                    contractUpdated = _a.sent();
                    return [3 /*break*/, 4];
                case 3:
                    logger_1["default"].error("Transaction is invalid");
                    throw new Error("The Transaction is invalid");
                case 4: return [3 /*break*/, 7];
                case 5: return [4 /*yield*/, ctx.prisma.updateContract({
                        where: {
                            id: args.input.contract_id
                        },
                        data: {
                            status: args.input.contract_state
                        }
                    })];
                case 6:
                    contractUpdated = _a.sent();
                    _a.label = 7;
                case 7: return [2 /*return*/, contractUpdated];
            }
        });
    }); }
});
exports.fundContractAsUser = nexus_1.mutationField("fundContractAsUser", {
    type: "Contract",
    args: {
        input: input_1.ContractStateUpdateWithPay.asArg({ required: true })
    },
    resolve: function (parents, args, ctx) { return __awaiter(_this, void 0, void 0, function () {
        var contractUpdated, userId, user, contractStatus, _a, resp, resp, e_3;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    contractUpdated = null;
                    userId = utils_1.getUserId(ctx);
                    return [4 /*yield*/, ctx.prisma.user({ id: userId }).$fragment("{\n            id\n            wallet {\n                id\n            }\n        }")
                        // make a payment withdrawal from the payment method, card or wallet.
                        // if payment succeed update the contract accordingly.
                    ];
                case 1:
                    user = _b.sent();
                    _b.label = 2;
                case 2:
                    _b.trys.push([2, 17, , 18]);
                    return [4 /*yield*/, ctx.prisma.contract({ id: args.input.contract_id })];
                case 3:
                    contractStatus = _b.sent();
                    if (!contractStatus) return [3 /*break*/, 16];
                    if (!(contractStatus.status == "INACTIVE")) return [3 /*break*/, 15];
                    _a = args.input.funding_type;
                    switch (_a) {
                        case "WALLET": return [3 /*break*/, 4];
                        case "CARD": return [3 /*break*/, 8];
                    }
                    return [3 /*break*/, 13];
                case 4: return [4 /*yield*/, fund_service_1.processWalletFunding(ctx, user.wallet.id, args.input.amount)];
                case 5:
                    resp = _b.sent();
                    if (!resp) return [3 /*break*/, 7];
                    return [4 /*yield*/, ctx.prisma.updateContract({
                            where: {
                                id: args.input.contract_id
                            },
                            data: {
                                status: "ACTIVE",
                                transactions: {
                                    create: {
                                        amount: args.input.amount,
                                        flow: "CREDIT",
                                        details: "FUNDED CONTRACT by wallet",
                                        status: "SUCCESSFUL"
                                    }
                                }
                            }
                        })];
                case 6:
                    contractUpdated = _b.sent();
                    _b.label = 7;
                case 7: return [3 /*break*/, 14];
                case 8: return [4 /*yield*/, fund_service_1.processCardFunding(ctx, args.input.card_id, args.input.amount)];
                case 9:
                    resp = _b.sent();
                    if (!(resp.status == "success")) return [3 /*break*/, 11];
                    return [4 /*yield*/, ctx.prisma.updateContract({
                            where: {
                                id: args.input.contract_id
                            },
                            data: {
                                status: "ACTIVE",
                                transactions: {
                                    create: {
                                        amount: args.input.amount,
                                        transaction_code: resp.txRef,
                                        flow: "CREDIT",
                                        details: "FUNDED CONTRACT by card",
                                        status: "SUCCESSFUL"
                                    }
                                }
                            }
                        })];
                case 10:
                    contractUpdated = _b.sent();
                    return [3 /*break*/, 12];
                case 11: throw new Error("YOUR CARD BALANCE MUST BE TOO LOW");
                case 12: return [3 /*break*/, 14];
                case 13: return [3 /*break*/, 14];
                case 14: return [2 /*return*/, contractUpdated];
                case 15: throw new Error("This contract has already been funded");
                case 16: return [3 /*break*/, 18];
                case 17:
                    e_3 = _b.sent();
                    logger_1["default"].error(e_3.message);
                    throw e_3;
                case 18: return [2 /*return*/];
            }
        });
    }); }
});
exports.fundWithCard = nexus_1.mutationField("fundWithCard", {
    type: "Contract",
    args: {
        input: input_1.ContractStateUpdateWithCard.asArg({ required: true })
    },
    resolve: function (parents, _a, ctx) {
        var input = _a.input;
        return __awaiter(_this, void 0, void 0, function () {
            var USER_ID, user, raveService, resp, contractUpdated, e_4;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        USER_ID = utils_1.getUserId(ctx);
                        if (!USER_ID) return [3 /*break*/, 9];
                        return [4 /*yield*/, ctx.prisma.user({ id: USER_ID }).$fragment("{\n                id \n                email\n                wallet {\n                    id\n                    amount\n                }\n            }")];
                    case 1:
                        user = _b.sent();
                        _b.label = 2;
                    case 2:
                        _b.trys.push([2, 7, , 8]);
                        raveService = new rave_service_1["default"]();
                        return [4 /*yield*/, raveService.getCardToken(input.tx_ref)];
                    case 3:
                        resp = _b.sent();
                        if (!resp) return [3 /*break*/, 6];
                        // create a card with the transaction reference.
                        return [4 /*yield*/, ctx.prisma.createCard({
                                expirymonth: resp.expirymonth,
                                expiryyear: resp.expiryyear,
                                owner: {
                                    connect: {
                                        id: USER_ID
                                    }
                                },
                                token: resp.token,
                                last_number: resp.last_numbers,
                                type: resp.type
                            })];
                    case 4:
                        // create a card with the transaction reference.
                        _b.sent();
                        return [4 /*yield*/, ctx.prisma.updateContract({
                                where: {
                                    id: input.contract_id
                                },
                                data: {
                                    status: "ACTIVE",
                                    transactions: {
                                        create: {
                                            amount: input.amount,
                                            flow: "CREDIT",
                                            details: "FUNDED CONTRACT by card",
                                            status: "SUCCESSFUL"
                                        }
                                    }
                                }
                            })];
                    case 5:
                        contractUpdated = _b.sent();
                        return [2 /*return*/, contractUpdated];
                    case 6: return [3 /*break*/, 8];
                    case 7:
                        e_4 = _b.sent();
                        throw e_4;
                    case 8: return [3 /*break*/, 10];
                    case 9: throw new Error("UNAUTHENTICATED USER");
                    case 10: return [2 /*return*/];
                }
            });
        });
    }
});
exports.addContractClientAccount = nexus_1.mutationField("addContractClientAccount", {
    type: "Contract",
    args: {
        input: input_1.AccountDetail.asArg({ required: true })
    },
    resolve: function (parents, args, ctx) { return __awaiter(_this, void 0, void 0, function () {
        var updatedContract, e_5;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, ctx.prisma.updateContract({
                            where: {
                                id: args.input.contract_id
                            },
                            data: {
                                client_account_no: args.input.account_no,
                                client_bank_code: args.input.bank_code,
                                client_account_name: args.input.account_name,
                                automaticWithdrawal: true
                            }
                        })
                        // if contract is set let go
                    ];
                case 1:
                    updatedContract = _a.sent();
                    // if contract is set let go
                    return [2 /*return*/, updatedContract];
                case 2:
                    e_5 = _a.sent();
                    logger_1["default"].error(e_5.message);
                    throw e_5;
                case 3: return [2 /*return*/];
            }
        });
    }); }
});
exports.withdrawFromMilestoneToAccount = nexus_1.mutationField("withdrawFromMilestoneToAccount", {
    type: "Contract",
    args: {
        input: input_1.withdrawalRequestInput.asArg({ required: true })
    },
    resolve: function (parents, _a, ctx) {
        var input = _a.input;
        return __awaiter(_this, void 0, void 0, function () {
            var contract, milestones, milestone, withdrawalStatus, updatedcontract, e_6;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 9, , 10]);
                        return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id }).$fragment("{\n                id\n                client_code\n                client_account_no\n                client_bank_code\n                client_account_name\n                milestones {\n                    id\n                    target\n                    paid\n                    available\n                }\n            }")];
                    case 1:
                        contract = _b.sent();
                        if (!(input.client_code == contract.client_code)) return [3 /*break*/, 7];
                        milestones = contract.milestones.filter(function (milestone) {
                            return (milestone.id == input.milestone_id) && (milestone.available == true);
                        });
                        if (!(milestones.length > 0)) return [3 /*break*/, 5];
                        milestone = milestones[0];
                        // make sure its available.
                        // make the withdrawal for the milestone
                        if (milestone.paid == true) {
                            throw new Error("funds have already been paid for this milestone");
                        }
                        return [4 /*yield*/, fund_service_1.withdrawFunds(ctx, {
                                bank_code: contract.client_bank_code,
                                account_name: contract.client_account_name,
                                account_number: contract.client_account_no,
                                amount: milestone.target
                            })
                            // update the milestone.
                        ];
                    case 2:
                        withdrawalStatus = _b.sent();
                        if (!(withdrawalStatus.status == "success")) return [3 /*break*/, 4];
                        return [4 /*yield*/, ctx.prisma.updateContract({
                                where: { id: input.contract_id },
                                data: {
                                    transactions: {
                                        create: {
                                            amount: input.amount,
                                            transaction_code: withdrawalStatus.txRef,
                                            milestone: {
                                                connect: {
                                                    id: input.milestone_id
                                                }
                                            },
                                            flow: "DEBIT",
                                            details: "Milestone funds of " + input.amount + " have been withdrawn",
                                            status: "PENDING"
                                        }
                                    }
                                }
                            })];
                    case 3:
                        updatedcontract = _b.sent();
                        return [2 /*return*/, updatedcontract];
                    case 4: return [3 /*break*/, 6];
                    case 5: throw new Error("Something went wrong, we are unable to process withdrawal");
                    case 6: return [3 /*break*/, 8];
                    case 7: throw new Error("you don't have the credentials to manipulate this contract.");
                    case 8: return [3 /*break*/, 10];
                    case 9:
                        e_6 = _b.sent();
                        logger_1["default"].error(e_6.message);
                        throw e_6;
                    case 10: return [2 /*return*/];
                }
            });
        });
    }
});
exports.withdrawFromContractToAccount = nexus_1.mutationField("withdrawFromContractToAccount", {
    type: "Contract",
    args: {
        input: input_1.withdrawalRequestInput.asArg({ required: true })
    },
    resolve: function (parents, _a, ctx) {
        var input = _a.input;
        return __awaiter(_this, void 0, void 0, function () {
            var contract, withdrawalStatus, e_7;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 7, , 8]);
                        return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id }).$fragment("{\n                id\n                client_code\n                client_account_no\n                client_bank_code\n                paid\n                payment_type\n                amount\n                client_account_name\n                milestones {\n                    id\n                    target\n                    paid\n                    available\n                }\n            }")];
                    case 1:
                        contract = _b.sent();
                        if (!((input.client_code == contract.client_code) && (contract.payment_type == "whole"))) return [3 /*break*/, 5];
                        // the client can make the withdrawal.
                        // check if the contract has already been withdrawn
                        if (contract.paid == true) {
                            throw new Error("funds have already been paid for this contract");
                        }
                        return [4 /*yield*/, fund_service_1.withdrawFunds(ctx, {
                                bank_code: contract.client_bank_code,
                                account_name: contract.client_account_name,
                                account_number: contract.client_account_no,
                                amount: contract.amount
                            })];
                    case 2:
                        withdrawalStatus = _b.sent();
                        if (!(withdrawalStatus.status == "success")) return [3 /*break*/, 4];
                        return [4 /*yield*/, ctx.prisma.updateContract({
                                where: { id: input.contract_id },
                                data: {
                                    transactions: {
                                        create: {
                                            amount: input.amount,
                                            transaction_code: withdrawalStatus.txRef,
                                            flow: "DEBIT",
                                            details: "Contract funds of " + input.amount + " have been withdrawn",
                                            status: "PENDING"
                                        }
                                    }
                                }
                            })];
                    case 3:
                        contract = _b.sent();
                        return [2 /*return*/, contract];
                    case 4: return [3 /*break*/, 6];
                    case 5: throw new Error("you don't have the credentials to manipulate this contract.");
                    case 6: return [3 /*break*/, 8];
                    case 7:
                        e_7 = _b.sent();
                        logger_1["default"].error(e_7.message);
                        throw e_7;
                    case 8: return [2 /*return*/];
                }
            });
        });
    }
});
// The retraction feature set
exports.requestRetractionAsClient = nexus_1.mutationField("requestRetractionAsClient", {
    type: "Contract",
    args: {
        input: input_1.RetractionRequestInput.asArg({ required: true })
    },
    resolve: function (parent, _a, ctx) {
        var input = _a.input;
        return __awaiter(_this, void 0, void 0, function () {
            var contract, milestones, updatedMilestone, updatedContract;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id }).$fragment("{ name id client client_code owner_type amount status retraction_status client_email owner {\n            first_name\n            last_name\n            phone\n            email\n        }}")];
                    case 1:
                        contract = _b.sent();
                        return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id }).milestones({ where: { id: input.milestone_id } })
                            // the request is not from a user.
                            // request the retraction for a client side user
                            // make sure the contract is a seller type.
                        ];
                    case 2:
                        milestones = _b.sent();
                        if (!(contract.owner_type == "seller")) return [3 /*break*/, 8];
                        if (!input.milestone_id) return [3 /*break*/, 5];
                        return [4 /*yield*/, ctx.prisma.updateMileStone({
                                where: {
                                    id: milestones[0].id
                                },
                                data: {
                                    retraction_status: "REQUESTED"
                                }
                            })
                            // send mail to request retraction.
                        ];
                    case 3:
                        updatedMilestone = _b.sent();
                        // send mail to request retraction.
                        agenda_service_1["default"].now("retractionForMilestone", {
                            contract: __assign({}, contract),
                            recipient: {
                                name: contract.owner.first_name,
                                email: contract.owner.email
                            },
                            actor: {
                                name: contract.client.split(" ")[0],
                                sendcode: false
                            },
                            subject: contract.client + " just requested a retraction from the contract " + contract.name
                        });
                        return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id })];
                    case 4: return [2 /*return*/, _b.sent()];
                    case 5: return [4 /*yield*/, ctx.prisma.updateContract({
                            where: {
                                id: input.contract_id
                            },
                            data: {
                                retraction_status: "REQUESTED"
                            }
                        })];
                    case 6:
                        updatedContract = _b.sent();
                        agenda_service_1["default"].now("retractionForContract", {
                            contract: __assign({}, contract),
                            recipient: {
                                name: contract.owner.first_name,
                                email: contract.owner.email
                            },
                            actor: {
                                name: contract.client.split(" ")[0],
                                sendcode: false
                            },
                            subject: contract.client + " just requested a retraction from the contract " + contract.name
                        });
                        return [2 /*return*/, updatedContract];
                    case 7: return [3 /*break*/, 9];
                    case 8: throw new Error("You can only request retractions on contracts where you are the buyer");
                    case 9: return [2 /*return*/];
                }
            });
        });
    }
});
exports.requestRetractionAsUser = nexus_1.mutationField("requestRetractionAsUser", {
    type: "Contract",
    args: {
        input: input_1.RetractionRequestInput.asArg({ required: true })
    },
    resolve: function (parent, _a, ctx) {
        var input = _a.input;
        return __awaiter(_this, void 0, void 0, function () {
            var userId, contract, milestones, updatedContract;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        userId = utils_1.getUserId(ctx);
                        return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id }).$fragment("{ name id client client_code owner_type amount status retraction_status client_email owner {\n            first_name\n            last_name\n            phone\n            email\n        }}")];
                    case 1:
                        contract = _b.sent();
                        return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id }).milestones({ where: { id: input.milestone_id } })];
                    case 2:
                        milestones = _b.sent();
                        if (!userId) return [3 /*break*/, 10];
                        if (!(contract.owner_type == "buyer")) return [3 /*break*/, 8];
                        if (!input.milestone_id) return [3 /*break*/, 5];
                        // we are retracting for a milestone.
                        // send mail request to approve a retraction for the milestone.
                        // if a milestone retraction is in progress request mail to get the milestone retracted.
                        return [4 /*yield*/, ctx.prisma.updateMileStone({
                                where: {
                                    id: milestones[0].id
                                },
                                data: {
                                    retraction_status: "REQUESTED"
                                }
                            })
                            // send mail to request retraction.
                        ];
                    case 3:
                        // we are retracting for a milestone.
                        // send mail request to approve a retraction for the milestone.
                        // if a milestone retraction is in progress request mail to get the milestone retracted.
                        _b.sent();
                        // send mail to request retraction.
                        agenda_service_1["default"].now("retractionForMilestone", {
                            contract: __assign({}, contract),
                            recipient: {
                                name: contract.client.split(" ")[0],
                                email: contract.client_email
                            },
                            actor: {
                                name: contract.owner.first_name,
                                sendcode: true
                            },
                            subject: contract.owner.first_name + " just requested a retraction from the contract " + contract.name
                        });
                        return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id })];
                    case 4: return [2 /*return*/, _b.sent()];
                    case 5: return [4 /*yield*/, ctx.prisma.updateContract({
                            where: {
                                id: input.contract_id
                            },
                            data: {
                                retraction_status: "REQUESTED"
                            }
                        })];
                    case 6:
                        updatedContract = _b.sent();
                        agenda_service_1["default"].now("retractionForContract", {
                            contract: __assign({}, contract),
                            recipient: {
                                name: contract.client.split(" ")[0],
                                email: contract.client_email
                            },
                            actor: {
                                name: contract.owner.first_name,
                                sendcode: true
                            },
                            subject: contract.owner.first_name + " just requested a retraction from the contract " + contract.name
                        });
                        return [2 /*return*/, updatedContract];
                    case 7: return [3 /*break*/, 9];
                    case 8: throw new Error("You can only request retractions on contracts where you are the buyer");
                    case 9: return [3 /*break*/, 11];
                    case 10: throw new Error("The user is not authenticated");
                    case 11: return [2 /*return*/];
                }
            });
        });
    }
});
// Approve retractions
/**'
 * The approval is being done by the user for the client, so the funds would be transferred to the clients bank account
 * A mail is sent to the client based on the approval state.
 */
exports.approveRetractionAsUser = nexus_1.mutationField("approveRetractionAsUser", {
    type: "Contract",
    args: {
        input: input_1.RetractionApprovalRequestInput.asArg({ required: true })
    },
    resolve: function (parent, _a, ctx) {
        var input = _a.input;
        return __awaiter(_this, void 0, void 0, function () {
            var userId, contract, milestones, _b, withdrawalStatus, updatedContract, _c, withdrawalStatus, updatedContract_1, updatedContract;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        userId = utils_1.getUserId(ctx);
                        return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id }).$fragment("{ \n            name \n            id \n            client \n            client_code \n            owner_type \n            amount \n            status \n            retraction_status \n            client_email \n            client_bank_code\n            client_account_name\n            client_account_no\n            owner {\n                first_name\n                last_name\n                phone\n                email\n            }\n        }")];
                    case 1:
                        contract = _d.sent();
                        return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id }).milestones({ where: { id: input.milestone_id } })];
                    case 2:
                        milestones = _d.sent();
                        if (!userId) return [3 /*break*/, 30];
                        if (!(contract.owner_type == "seller")) return [3 /*break*/, 28];
                        if (!input.milestone_id) return [3 /*break*/, 17];
                        if (!(milestones[0].retraction_status == "REQUESTED" || milestones[0].retraction_status == "REJECTED")) return [3 /*break*/, 15];
                        _b = input.approve_status;
                        switch (_b) {
                            case "APPROVED": return [3 /*break*/, 3];
                            case "REJECTED": return [3 /*break*/, 8];
                        }
                        return [3 /*break*/, 11];
                    case 3: return [4 /*yield*/, fund_service_1.withdrawFunds(ctx, {
                            bank_code: contract.client_bank_code,
                            account_name: contract.client_account_name,
                            account_number: contract.client_account_no,
                            amount: milestones[0].target
                        })];
                    case 4:
                        withdrawalStatus = _d.sent();
                        if (!(withdrawalStatus.status == "success")) return [3 /*break*/, 7];
                        return [4 /*yield*/, ctx.prisma.updateMileStone({
                                where: {
                                    id: input.milestone_id
                                },
                                data: {
                                    retraction_status: "PENDING"
                                }
                            })];
                    case 5:
                        _d.sent();
                        return [4 /*yield*/, ctx.prisma.updateContract({
                                where: { id: input.contract_id },
                                data: {
                                    transactions: {
                                        create: {
                                            amount: milestones[0].target,
                                            milestone: {
                                                connect: {
                                                    id: input.milestone_id
                                                }
                                            },
                                            transaction_code: withdrawalStatus.txRef,
                                            flow: "DEBIT",
                                            details: "Milestone funds of " + milestones[0].target + " have been retracted",
                                            status: "PENDING"
                                        }
                                    }
                                }
                            })];
                    case 6:
                        updatedContract = _d.sent();
                        return [2 /*return*/, updatedContract];
                    case 7: return [3 /*break*/, 13];
                    case 8: 
                    // reject retraction and wait
                    return [4 /*yield*/, ctx.prisma.updateMileStone({
                            where: {
                                id: input.milestone_id
                            },
                            data: {
                                retraction_status: "REJECTED"
                            }
                        })];
                    case 9:
                        // reject retraction and wait
                        _d.sent();
                        return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id })];
                    case 10: return [2 /*return*/, _d.sent()];
                    case 11: return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id })];
                    case 12: return [2 /*return*/, _d.sent()];
                    case 13: return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id })];
                    case 14: return [2 /*return*/, _d.sent()];
                    case 15: throw new Error("You can't approve a retraction for this milestone at the moment");
                    case 16: return [3 /*break*/, 27];
                    case 17:
                        _c = input.approve_status;
                        switch (_c) {
                            case "APPROVED": return [3 /*break*/, 18];
                            case "REJECTED": return [3 /*break*/, 23];
                        }
                        return [3 /*break*/, 25];
                    case 18: return [4 /*yield*/, fund_service_1.withdrawFunds(ctx, {
                            bank_code: contract.client_bank_code,
                            account_name: contract.client_account_name,
                            account_number: contract.client_account_no,
                            amount: contract.amount
                        })];
                    case 19:
                        withdrawalStatus = _d.sent();
                        if (!(withdrawalStatus.status == "success")) return [3 /*break*/, 22];
                        return [4 /*yield*/, ctx.prisma.updateContract({
                                where: {
                                    id: input.contract_id
                                },
                                data: {
                                    retraction_status: "PENDING"
                                }
                            })];
                    case 20:
                        _d.sent();
                        return [4 /*yield*/, ctx.prisma.updateContract({
                                where: { id: input.contract_id },
                                data: {
                                    transactions: {
                                        create: {
                                            amount: contract.amount,
                                            transaction_code: withdrawalStatus.txRef,
                                            flow: "DEBIT",
                                            details: "Contract funds " + contract.amount + " are being retracted",
                                            status: "PENDING"
                                        }
                                    },
                                    status: "COMPLETED"
                                }
                            })];
                    case 21:
                        updatedContract_1 = _d.sent();
                        return [2 /*return*/, updatedContract_1];
                    case 22: return [3 /*break*/, 27];
                    case 23: return [4 /*yield*/, ctx.prisma.updateContract({
                            where: {
                                id: input.contract_id
                            },
                            data: {
                                retraction_status: "REJECTED"
                            }
                        })];
                    case 24:
                        updatedContract = _d.sent();
                        return [2 /*return*/, updatedContract];
                    case 25: return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id })];
                    case 26: return [2 /*return*/, _d.sent()];
                    case 27: return [3 /*break*/, 29];
                    case 28: throw new Error("You can only request retractions on contracts where you are the buyer");
                    case 29: return [3 /*break*/, 31];
                    case 30: throw new Error("The user is not authenticated");
                    case 31: return [2 /*return*/];
                }
            });
        });
    }
});
/**
 * The Approval here is from the client, so the money is transfered to the user's wallet and an approval message is sent to the user.
 */
exports.approveRetractionAsClient = nexus_1.mutationField("approveRetractionAsClient", {
    type: "Contract",
    args: {
        input: input_1.RetractionApprovalRequestInput.asArg({ required: true })
    },
    resolve: function (parent, _a, ctx) {
        var input = _a.input;
        return __awaiter(_this, void 0, void 0, function () {
            var contract, milestones, _b, contractFilter, updatedContract, _c, updatedContract, updatedContract;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0: return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id }).$fragment("{ \n            name \n            id \n            client \n            client_code \n            owner_type \n            amount \n            status \n            retraction_status \n            client_email \n            client_bank_code\n            client_account_name\n            client_account_no\n            owner {\n                wallet {\n                    id\n                    amount\n                }\n                first_name\n                last_name\n                phone\n                email\n            }\n        }")];
                    case 1:
                        contract = _d.sent();
                        return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id }).milestones({ where: { id: input.milestone_id } })];
                    case 2:
                        milestones = _d.sent();
                        if (!(contract.owner_type == "buyer")) return [3 /*break*/, 26];
                        if (!input.milestone_id) return [3 /*break*/, 17];
                        if (!(milestones[0].retraction_status == "REQUESTED" || milestones[0].retraction_status == "REJECTED")) return [3 /*break*/, 15];
                        _b = input.approve_status;
                        switch (_b) {
                            case "APPROVED": return [3 /*break*/, 3];
                            case "REJECTED": return [3 /*break*/, 8];
                        }
                        return [3 /*break*/, 11];
                    case 3: return [4 /*yield*/, ctx.prisma.updateWallet({
                            data: {
                                amount: (parseFloat(contract.owner.wallet.amount) + parseFloat(milestones[0].target)),
                                transactions: {
                                    create: {
                                        amount: parseFloat(milestones[0].target),
                                        flow: "CREDIT",
                                        transaction_code: input.tx_ref,
                                        details: "Wallet top-up from from retraction on " + contract.name,
                                        status: "SUCCESSFUL"
                                    }
                                }
                            },
                            where: {
                                id: contract.owner.wallet.id
                            }
                        })];
                    case 4:
                        _d.sent();
                        return [4 /*yield*/, ctx.prisma.updateMileStone({
                                where: {
                                    id: input.milestone_id
                                },
                                data: {
                                    retraction_status: "SUCCESSFUL",
                                    disburseStatus: "CONFIRMED"
                                }
                            })];
                    case 5:
                        _d.sent();
                        return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id }).$fragment("{ id amount owner {id first_name} owner_type milestones { disburseStatus } name status client_phone client client_email}")];
                    case 6:
                        contractFilter = _d.sent();
                        return [4 /*yield*/, ctx.prisma.updateContract({
                                where: { id: input.contract_id },
                                data: {
                                    transactions: {
                                        create: {
                                            amount: milestones[0].target,
                                            milestone: {
                                                connect: {
                                                    id: input.milestone_id
                                                }
                                            },
                                            flow: "DEBIT",
                                            details: "Milestone funds of " + milestones[0].target + " have been retracted",
                                            status: "SUCCESSFUL"
                                        },
                                        status: contractFilter.milestones.filter(function (item) { return item.disburseStatus != "CONFIRMED"; }).length == 0 ? "COMPLETED" : contractFilter.status
                                    }
                                }
                            })];
                    case 7:
                        updatedContract = _d.sent();
                        return [2 /*return*/, updatedContract];
                    case 8: 
                    // reject retraction and wait
                    return [4 /*yield*/, ctx.prisma.updateMileStone({
                            where: {
                                id: input.milestone_id
                            },
                            data: {
                                retraction_status: "REJECTED"
                            }
                        })];
                    case 9:
                        // reject retraction and wait
                        _d.sent();
                        return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id })];
                    case 10: return [2 /*return*/, _d.sent()];
                    case 11: return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id })];
                    case 12: return [2 /*return*/, _d.sent()];
                    case 13: return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id })];
                    case 14: return [2 /*return*/, _d.sent()];
                    case 15: throw new Error("You can't approve a retraction for this milestone at the moment");
                    case 16: return [3 /*break*/, 25];
                    case 17:
                        _c = input.approve_status;
                        switch (_c) {
                            case "APPROVED": return [3 /*break*/, 18];
                            case "REJECTED": return [3 /*break*/, 21];
                        }
                        return [3 /*break*/, 23];
                    case 18: 
                    // approve retraction and fund account pending
                    return [4 /*yield*/, ctx.prisma.updateWallet({
                            data: {
                                amount: (parseFloat(contract.owner.wallet.amount) + parseFloat(contract.amount)),
                                transactions: {
                                    create: {
                                        amount: parseFloat(contract.amount),
                                        flow: "CREDIT",
                                        transaction_code: input.tx_ref,
                                        details: "Wallet top-up from from retraction on " + contract.name,
                                        status: "SUCCESSFUL"
                                    }
                                }
                            },
                            where: {
                                id: contract.owner.wallet.id
                            }
                        })];
                    case 19:
                        // approve retraction and fund account pending
                        _d.sent();
                        return [4 /*yield*/, ctx.prisma.updateContract({
                                where: { id: input.contract_id },
                                data: {
                                    retraction_status: "SUCCESSFUL",
                                    transactions: {
                                        create: {
                                            amount: contract.amount,
                                            // transaction_code: withdrawalStatus.txRef,
                                            flow: "DEBIT",
                                            details: "Contract funds " + contract.amount + " are being retracted",
                                            status: "SUCCESSFUL"
                                        }
                                    },
                                    status: "COMPLETED"
                                }
                            })];
                    case 20:
                        updatedContract = _d.sent();
                        return [2 /*return*/, updatedContract];
                    case 21: return [4 /*yield*/, ctx.prisma.updateContract({
                            where: {
                                id: input.contract_id
                            },
                            data: {
                                retraction_status: "REJECTED"
                            }
                        })];
                    case 22:
                        updatedContract = _d.sent();
                        return [2 /*return*/, updatedContract];
                    case 23: return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id })];
                    case 24: return [2 /*return*/, _d.sent()];
                    case 25: return [3 /*break*/, 27];
                    case 26: throw new Error("You can only request retractions on contracts where you are the buyer");
                    case 27: return [2 /*return*/];
                }
            });
        });
    }
});
// Mutations to cancel retractions.
exports.cancelRetractionAsUser = nexus_1.mutationField("cancelRetractionAsUser", {
    type: "Contract",
    args: {
        input: input_1.CancelRetractionInput.asArg({ required: true })
    },
    resolve: function (parent, _a, ctx) {
        var input = _a.input;
        return __awaiter(_this, void 0, void 0, function () {
            var userId, contract, milestones, contractBase, updatedContract;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        userId = utils_1.getUserId(ctx);
                        if (!userId) return [3 /*break*/, 11];
                        return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id }).$fragment("{ \n                name \n                id \n                client \n                client_code \n                owner_type \n                amount \n                status \n                retraction_status \n                client_email \n                client_bank_code\n                client_account_name\n                client_account_no\n                owner {\n                    wallet {\n                        id\n                        amount\n                    }\n                    first_name\n                    last_name\n                    phone\n                    email\n                }\n            }")];
                    case 1:
                        contract = _b.sent();
                        return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id }).milestones({ where: { id: input.milestone_id } })
                            // for a particular retraction object controact or milestone
                        ];
                    case 2:
                        milestones = _b.sent();
                        if (!(contract.owner_type == "buyer")) return [3 /*break*/, 10];
                        if (!input.milestone_id) return [3 /*break*/, 7];
                        if (!(milestones[0].retraction_status == "REQUESTED" || milestones[0].retraction_status == "REJECTED")) return [3 /*break*/, 5];
                        // approving a retraction means the money is sent to the clients bank account, it's like a withdrawal.
                        return [4 /*yield*/, ctx.prisma.updateMileStone({
                                where: {
                                    id: input.milestone_id
                                },
                                data: {
                                    retraction_status: "INACTIVE"
                                }
                            })];
                    case 3:
                        // approving a retraction means the money is sent to the clients bank account, it's like a withdrawal.
                        _b.sent();
                        return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id })];
                    case 4:
                        contractBase = _b.sent();
                        return [2 /*return*/, contractBase];
                    case 5: throw new Error("You can't cancel this retraction for this milestone at the moment");
                    case 6: return [3 /*break*/, 9];
                    case 7: return [4 /*yield*/, ctx.prisma.updateContract({
                            where: { id: input.contract_id },
                            data: {
                                retraction_status: "INACTIVE"
                            }
                        })];
                    case 8:
                        updatedContract = _b.sent();
                        return [2 /*return*/, updatedContract
                            // return await ctx.prisma.contract({id: input.contract_id})
                        ];
                    case 9: return [3 /*break*/, 11];
                    case 10: throw new Error("You can only cancel retractions on contracts where you are the buyer");
                    case 11: return [2 /*return*/];
                }
            });
        });
    }
});
exports.cancelRetractionAsClient = nexus_1.mutationField("cancelRetractionAsClient", {
    type: "Contract",
    args: {
        input: input_1.CancelRetractionInput.asArg({ required: true })
    },
    resolve: function (parent, args, ctx) { return __awaiter(_this, void 0, void 0, function () {
        var contract, milestones, contractBase, updatedContract;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id }).$fragment("{ \n            name \n            id \n            client \n            client_code \n            owner_type \n            amount \n            status \n            retraction_status \n            client_email \n            client_bank_code\n            client_account_name\n            client_account_no\n            owner {\n                wallet {\n                    id\n                    amount\n                }\n                first_name\n                last_name\n                phone\n                email\n            }\n        }")];
                case 1:
                    contract = _a.sent();
                    return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id }).milestones({ where: { id: input.milestone_id } })
                        // for a particular retraction object controact or milestone
                    ];
                case 2:
                    milestones = _a.sent();
                    if (!(contract.owner_type == "seller")) return [3 /*break*/, 10];
                    if (!input.milestone_id) return [3 /*break*/, 7];
                    if (!(milestones[0].retraction_status == "REQUESTED" || milestones[0].retraction_status == "REJECTED")) return [3 /*break*/, 5];
                    // approving a retraction means the money is sent to the clients bank account, it's like a withdrawal.
                    return [4 /*yield*/, ctx.prisma.updateMileStone({
                            where: {
                                id: input.milestone_id
                            },
                            data: {
                                retraction_status: "INACTIVE"
                            }
                        })];
                case 3:
                    // approving a retraction means the money is sent to the clients bank account, it's like a withdrawal.
                    _a.sent();
                    return [4 /*yield*/, ctx.prisma.contract({ id: input.contract_id })];
                case 4:
                    contractBase = _a.sent();
                    return [2 /*return*/, contractBase];
                case 5: throw new Error("You can't cancel this retraction for this milestone at the moment");
                case 6: return [3 /*break*/, 9];
                case 7: return [4 /*yield*/, ctx.prisma.updateContract({
                        where: { id: input.contract_id },
                        data: {
                            retraction_status: "INACTIVE"
                        }
                    })];
                case 8:
                    updatedContract = _a.sent();
                    return [2 /*return*/, updatedContract
                        // return await ctx.prisma.contract({id: input.contract_id})
                    ];
                case 9: return [3 /*break*/, 11];
                case 10: throw new Error("You can only cancel retractions on contracts where you are the buyer");
                case 11: return [2 /*return*/];
            }
        });
    }); }
});
// helper functions
var sellerDisbursementMilestoneProcess = function (ctx, user, milestone, contract) { return __awaiter(_this, void 0, void 0, function () {
    var currentWalletAmount;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: 
            // to update the wallet of the contract owner, perform a withdrawal.
            // update wallet.
            // because we are updating wallet directly
            return [4 /*yield*/, ctx.prisma.updateMileStone({
                    where: {
                        id: milestone.id
                    },
                    data: {
                        available: false,
                        paid: true
                    }
                })];
            case 1:
                // to update the wallet of the contract owner, perform a withdrawal.
                // update wallet.
                // because we are updating wallet directly
                _a.sent();
                currentWalletAmount = user.wallet.amount;
                return [4 /*yield*/, ctx.prisma.updateWallet({
                        data: {
                            amount: currentWalletAmount + milestone.target,
                            transactions: {
                                create: {
                                    amount: milestone.target,
                                    flow: "CREDIT",
                                    details: "Disburse to wallet from " + contract.name,
                                    status: "SUCCESSFUL"
                                }
                            }
                        },
                        where: {
                            id: user.wallet.id
                        }
                    })
                    // create transaction 
                ];
            case 2:
                _a.sent();
                return [4 /*yield*/, ctx.prisma.updateContract({
                        data: {
                            transactions: {
                                create: {
                                    amount: milestone.target,
                                    flow: "DEBIT",
                                    details: "Disbursed funds for milestone " + milestone.target,
                                    milestone: {
                                        connect: {
                                            id: milestone.id
                                        }
                                    },
                                    status: "SUCCESSFUL"
                                }
                            },
                            status: contract.milestones.filter(function (item) { return item.disburseStatus != "CONFIRMED"; }).length == 0 ? "COMPLETED" : contract.status
                        },
                        where: {
                            id: contract.id
                        }
                    })];
            case 3: 
            // create transaction 
            return [2 /*return*/, _a.sent()];
        }
    });
}); };
var buyerDisbursementMilestoneProcess = function (ctx, user, milestone, contract) { return __awaiter(_this, void 0, void 0, function () {
    var milestoneUpdate, updatedContract, e_8;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 5, , 6]);
                return [4 /*yield*/, ctx.prisma.updateMileStone({
                        where: {
                            id: milestone.id
                        },
                        data: {
                            available: true,
                            paid: false
                        }
                    })];
            case 1:
                milestoneUpdate = _a.sent();
                if (!milestoneUpdate) return [3 /*break*/, 3];
                return [4 /*yield*/, ctx.prisma.updateContract({
                        data: {
                            transactions: {
                                create: {
                                    amount: milestone.target,
                                    milestone: {
                                        connect: {
                                            id: milestone.id
                                        }
                                    },
                                    flow: "DEBIT",
                                    details: "Disbursed funds for milestone " + milestone.target + " to " + contract.client,
                                    status: "SUCCESSFUL"
                                }
                            },
                            status: contract.milestones.filter(function (item) { return item.disburseStatus != "CONFIRMED"; }).length == 0 ? "COMPLETED" : contract.status
                        },
                        where: {
                            id: contract.id
                        }
                    })];
            case 2:
                updatedContract = _a.sent();
                agenda_service_1["default"].now("sendDisburseNotification", {
                    user: {
                        email: updatedContract.client_email,
                        name: updatedContract.client
                    },
                    contract: contract,
                    owner: user,
                    subject: "Funds have been disbursed to you - Attempt a withdrawal"
                });
                return [2 /*return*/, exports.updateContract];
            case 3: throw new Error("The milestone has already been disbursed");
            case 4: return [3 /*break*/, 6];
            case 5:
                e_8 = _a.sent();
                logger_1["default"].error(e_8.message);
                throw new Error(e_8.message + "- Your milestone couldn't be updated updated");
            case 6: return [2 /*return*/];
        }
    });
}); };
var sellerDisbursementWholeProcess = function (ctx, user, args, contract) { return __awaiter(_this, void 0, void 0, function () {
    var currentWalletAmount, e_9;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 4]);
                currentWalletAmount = user.wallet.amount;
                return [4 /*yield*/, ctx.prisma.updateWallet({
                        data: {
                            amount: currentWalletAmount + contract.amount,
                            transactions: {
                                create: {
                                    amount: contract.amount,
                                    flow: "CREDIT",
                                    details: "Disburse to wallet from " + contract.name,
                                    status: "SUCCESSFUL"
                                }
                            }
                        },
                        where: {
                            id: user.wallet.id
                        }
                    })
                    // create transaction 
                ];
            case 1:
                _a.sent();
                return [4 /*yield*/, ctx.prisma.updateContract({
                        where: {
                            id: args.input.contract_id
                        },
                        data: {
                            disburseStatus: "CONFIRMED",
                            status: args.input.status,
                            transactions: {
                                create: {
                                    amount: contract.amount,
                                    flow: "DEBIT",
                                    details: "Disbursed funds for contract " + contract.id + " -  " + contract.name,
                                    status: "SUCCESSFUL"
                                }
                            },
                            status: "COMPLETED"
                        }
                    })];
            case 2: 
            // create transaction 
            return [2 /*return*/, _a.sent()];
            case 3:
                e_9 = _a.sent();
                logger_1["default"].error(e_9.message);
                throw new Error(e_9.message + "- Your contract couldn't be updated updated");
            case 4: return [2 /*return*/];
        }
    });
}); };
var buyerDisbursementWholeProcess = function (ctx, user, args, contract) { return __awaiter(_this, void 0, void 0, function () {
    var updatedContract, e_10;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, ctx.prisma.updateContract({
                        where: {
                            id: args.input.contract_id
                        },
                        data: {
                            disburseStatus: "CONFIRMED",
                            available: true,
                            status: args.input.status,
                            transactions: {
                                create: {
                                    amount: contract.amount,
                                    flow: "DEBIT",
                                    details: "Disbursed funds for contract " + contract.id + " -  " + contract.name,
                                    status: "SUCCESSFUL"
                                }
                            },
                            status: "COMPLETED"
                        }
                    })];
            case 1:
                updatedContract = _a.sent();
                agenda_service_1["default"].now("sendDisburseNotification", {
                    user: {
                        email: updatedContract.client_email,
                        name: updatedContract.client
                    },
                    contract: contract,
                    owner: user,
                    subject: "Funds have been disbursed to you - Attempt a withdrawal"
                });
                return [2 /*return*/, exports.updateContract];
            case 2:
                e_10 = _a.sent();
                logger_1["default"].error(e_10.message);
                throw new Error(e_10.message + "- Your contract couldn't be updated updated");
            case 3: return [2 /*return*/];
        }
    });
}); };
//# sourceMappingURL=contracts-mutations.js.map