"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _this = this;
exports.__esModule = true;
// @ts-nocheck
var nexus_1 = require("nexus");
var utils_1 = require("../../utils");
var input_1 = require("../input");
var rave_service_1 = __importDefault(require("../../services/rave-service"));
var logger_1 = __importDefault(require("../../services/logger"));
var ip_1 = __importDefault(require("ip"));
var utils_2 = require("../../utils");
/**
 * Top up a wallet
 */
exports.topUpWallet = nexus_1.mutationField("topupWallet", {
    type: "Wallet",
    args: {
        input: input_1.TopUpInput.asArg({ required: true })
    },
    resolve: function (parents, _a, ctx) {
        var input = _a.input;
        return __awaiter(_this, void 0, void 0, function () {
            var userId, raveService, walletUser, _b, resp, wallet, txRef, card, response, wallet, e_1;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        userId = utils_1.getUserId(ctx);
                        raveService = new rave_service_1["default"]();
                        _c.label = 1;
                    case 1:
                        _c.trys.push([1, 17, , 18]);
                        if ((input.amount <= 0) || input.amount == null) {
                            throw new Error("Enter an amount ");
                        }
                        return [4 /*yield*/, ctx.prisma.user({ id: userId }).$fragment("{\n                id \n                email\n                wallet {\n                    id\n                    amount\n                }\n            }")];
                    case 2:
                        walletUser = _c.sent();
                        _b = input.card_type;
                        switch (_b) {
                            case "NEW_CARD": return [3 /*break*/, 3];
                            case "CARD": return [3 /*break*/, 8];
                        }
                        return [3 /*break*/, 15];
                    case 3: return [4 /*yield*/, raveService.getCardToken(input.tx_ref)];
                    case 4:
                        resp = _c.sent();
                        if (!resp) return [3 /*break*/, 7];
                        return [4 /*yield*/, ctx.prisma.createCard({
                                expirymonth: resp.expirymonth,
                                expiryyear: resp.expiryyear,
                                owner: {
                                    connect: {
                                        id: userId
                                    }
                                },
                                token: resp.token,
                                last_number: resp.last_numbers,
                                type: resp.type
                            })];
                    case 5:
                        _c.sent();
                        return [4 /*yield*/, ctx.prisma.updateWallet({
                                data: {
                                    amount: (parseFloat(walletUser.wallet.amount) + parseFloat(input.amount)),
                                    transactions: {
                                        create: {
                                            amount: parseFloat(input.amount),
                                            flow: "CREDIT",
                                            transaction_code: input.tx_ref,
                                            details: "Wallet top-up from new card",
                                            status: "SUCCESSFUL"
                                        }
                                    }
                                },
                                where: {
                                    id: walletUser.wallet.id
                                }
                            })];
                    case 6:
                        wallet = _c.sent();
                        return [2 /*return*/, wallet];
                    case 7: return [3 /*break*/, 16];
                    case 8:
                        txRef = utils_2.generateUUID();
                        return [4 /*yield*/, ctx.prisma.card({ id: input.card_id })
                            // charge the card with the token to top up the wallet
                        ];
                    case 9:
                        card = _c.sent();
                        if (!card) return [3 /*break*/, 14];
                        return [4 /*yield*/, raveService.chargeCard({
                                token: card.token,
                                amount: input.amount,
                                email: walletUser.email,
                                IP: ip_1["default"].address(),
                                txRef: txRef
                            })];
                    case 10:
                        response = _c.sent();
                        if (!(response.status == "success")) return [3 /*break*/, 12];
                        return [4 /*yield*/, ctx.prisma.updateWallet({
                                data: {
                                    amount: (parseFloat(walletUser.wallet.amount) + parseFloat(input.amount)),
                                    transactions: {
                                        create: {
                                            amount: parseFloat(input.amount),
                                            flow: "CREDIT",
                                            transaction_code: txRef,
                                            details: "Wallet top-up from card",
                                            status: "SUCCESSFUL"
                                        }
                                    }
                                },
                                where: {
                                    id: walletUser.wallet.id
                                }
                            })];
                    case 11:
                        wallet = _c.sent();
                        return [2 /*return*/, wallet];
                    case 12: throw new Error("Wallet wasn't funded, " + response.message);
                    case 13: return [3 /*break*/, 15];
                    case 14: throw new Error("card doesn't exist");
                    case 15: throw new Error("You have to select a card type in your request");
                    case 16: return [3 /*break*/, 18];
                    case 17:
                        e_1 = _c.sent();
                        logger_1["default"].error(e_1.message);
                        throw e_1;
                    case 18: return [2 /*return*/];
                }
            });
        });
    }
});
/**
 * Withdraw funds from a wallet.
 */
exports.withdrawFunds = nexus_1.mutationField("withdrawFunds", {
    type: "Wallet",
    args: {
        input: input_1.WithdrawInput.asArg({ required: true })
    },
    resolve: function (parents, _a, ctx) {
        var input = _a.input;
        return __awaiter(_this, void 0, void 0, function () {
            var userId, raveService, txRef, user, walletUser, _b, bankAccount, transferData, response, wallet, bankAccount, transferData, response, wallet, e_2;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        userId = utils_1.getUserId(ctx);
                        raveService = new rave_service_1["default"]();
                        txRef = utils_2.generateUUID();
                        _c.label = 1;
                    case 1:
                        _c.trys.push([1, 16, , 17]);
                        return [4 /*yield*/, ctx.prisma.user({ id: userId })];
                    case 2:
                        user = _c.sent();
                        return [4 /*yield*/, ctx.prisma.user({ id: userId }).$fragment("{\n                id \n                email\n                wallet {\n                    id\n                    amount\n                }\n            }")];
                    case 3:
                        walletUser = _c.sent();
                        _b = input.withdrawal_type;
                        switch (_b) {
                            case "NORMAL_WITHDRAWAL": return [3 /*break*/, 4];
                            case "ADD_ACCOUNT": return [3 /*break*/, 9];
                        }
                        return [3 /*break*/, 14];
                    case 4: return [4 /*yield*/, ctx.prisma.bankAccount({ id: input.bank_account_id })];
                    case 5:
                        bankAccount = _c.sent();
                        transferData = {
                            "account_bank": bankAccount.bank_code,
                            "account_number": bankAccount.account_number,
                            "amount": parseInt(input.amount),
                            "currency": "NGN",
                            "reference": txRef,
                            "beneficiary_name": bankAccount.account_name,
                            "destination_branch_code": bankAccount.bank_code
                        };
                        return [4 /*yield*/, raveService.transferFunds(JSON.stringify(transferData))];
                    case 6:
                        response = _c.sent();
                        if (!(response.status == "success")) return [3 /*break*/, 8];
                        return [4 /*yield*/, ctx.prisma.updateWallet({
                                data: {
                                    // amount:( parseFloat(walletUser.wallet.amount) - parseFloat(input.amount)) ,
                                    transactions: {
                                        create: {
                                            amount: parseFloat(input.amount),
                                            flow: "DEBIT",
                                            transaction_code: txRef,
                                            details: "Withdrawal from wallet initiated",
                                            status: "PENDING"
                                        }
                                    }
                                },
                                where: {
                                    id: walletUser.wallet.id
                                }
                            })];
                    case 7:
                        wallet = _c.sent();
                        return [2 /*return*/, wallet];
                    case 8: throw new Error(response.message);
                    case 9: return [4 /*yield*/, ctx.prisma.createBankAccount(__assign({
                            account_number: input.account_number,
                            bank_name: input.bank_name,
                            account_name: input.account_name,
                            bank_code: input.bank_code
                        }, { owner: {
                                connect: {
                                    id: userId
                                }
                            } }))];
                    case 10:
                        bankAccount = _c.sent();
                        transferData = {
                            "account_bank": bankAccount.bank_code,
                            "account_number": bankAccount.account_number,
                            "amount": parseInt(input.amount),
                            "currency": "NGN",
                            "reference": txRef,
                            "beneficiary_name": bankAccount.account_name,
                            "destination_branch_code": bankAccount.bank_code
                        };
                        return [4 /*yield*/, raveService.transferFunds(JSON.stringify(transferData))];
                    case 11:
                        response = _c.sent();
                        if (!(response.status == "success")) return [3 /*break*/, 13];
                        return [4 /*yield*/, ctx.prisma.updateWallet({
                                data: {
                                    // amount:( parseFloat(walletUser.wallet.amount) - parseFloat(input.amount)) ,
                                    transactions: {
                                        create: {
                                            amount: parseFloat(input.amount),
                                            flow: "DEBIT",
                                            transaction_code: txRef,
                                            details: "Withdrawal from wallet initiated",
                                            status: "PENDING"
                                        }
                                    }
                                },
                                where: {
                                    id: walletUser.wallet.id
                                }
                            })];
                    case 12:
                        wallet = _c.sent();
                        return [2 /*return*/, wallet];
                    case 13: throw new Error(response.message);
                    case 14: throw new Error("You selected no withdrawal type, please select one");
                    case 15: return [3 /*break*/, 17];
                    case 16:
                        e_2 = _c.sent();
                        logger_1["default"].error(e_2.message);
                        throw e_2;
                    case 17: return [2 /*return*/];
                }
            });
        });
    }
});
//# sourceMappingURL=wallet-mutation.js.map