"use strict";
exports.__esModule = true;
var nexus_prisma_1 = require("nexus-prisma");
// @ts-ignore
var Wallet = nexus_prisma_1.prismaObjectType({
    name: "Wallet",
    definition: function (t) {
        t.prismaFields([
            'id',
            'owner',
            'amount',
            'transactions',
            'created_at',
            'updated_at',
            'va_flw_ref',
            'va_frequency',
            'va_expirydate',
            'va_order_ref',
            'va_account_number',
            'va_tx_ref',
            'va_bank_name',
            'va_account_name',
            'va_created_at',
            'va_note'
        ]);
    }
});
exports["default"] = Wallet;
//# sourceMappingURL=Wallet.js.map