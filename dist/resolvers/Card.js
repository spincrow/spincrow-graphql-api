"use strict";
exports.__esModule = true;
var nexus_prisma_1 = require("nexus-prisma");
// @ts-ignore
var Card = nexus_prisma_1.prismaObjectType({
    name: "Card",
    definition: function (t) {
        t.prismaFields([
            'id',
            'owner',
            'token',
            'card_name',
            'expirymonth',
            'expiryyear',
            'last_number',
            'type'
        ]);
    }
});
exports["default"] = Card;
//# sourceMappingURL=Card.js.map