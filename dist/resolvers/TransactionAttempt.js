"use strict";
exports.__esModule = true;
var nexus_prisma_1 = require("nexus-prisma");
// @ts-ignore
var TransactionAttempt = nexus_prisma_1.prismaObjectType({
    name: "TransactionAttempt",
    definition: function (t) {
        t.prismaFields([
            'id',
            'transaction',
            'status',
            'amount',
            'created_at',
            'updated_at',
            'hash'
        ]);
    }
});
exports["default"] = TransactionAttempt;
//# sourceMappingURL=TransactionAttempt.js.map