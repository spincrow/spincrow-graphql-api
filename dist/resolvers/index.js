"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var User_1 = __importDefault(require("./User"));
var Wallet_1 = __importDefault(require("./Wallet"));
var Contract_1 = __importDefault(require("./Contract"));
var Card_1 = __importDefault(require("./Card"));
var BankAccount_1 = __importDefault(require("./BankAccount"));
var Transaction_1 = require("./Transaction");
var MileStone_1 = __importDefault(require("./MileStone"));
var query_1 = __importDefault(require("./query"));
var mutations_1 = __importDefault(require("./mutations"));
var extraTypes_1 = require("./extraTypes");
exports["default"] = [
    User_1["default"],
    Contract_1["default"],
    extraTypes_1.BankCode,
    Card_1["default"],
    BankAccount_1["default"],
    extraTypes_1.File,
    extraTypes_1.Upload,
    MileStone_1["default"],
    Transaction_1.WalletTransaction,
    Transaction_1.ContractTransaction,
    extraTypes_1.ConfirmationResponse,
    query_1["default"],
    Wallet_1["default"],
    mutations_1["default"],
    extraTypes_1.AuthPayload,
    extraTypes_1.Stats
];
//# sourceMappingURL=index.js.map