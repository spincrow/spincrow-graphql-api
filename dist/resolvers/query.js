"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var nexus_1 = require("nexus");
var utils_1 = require("../utils");
var rave_service_1 = __importDefault(require("../services/rave-service"));
var logger_1 = __importDefault(require("../services/logger"));
var Query = nexus_1.queryType({
    definition: function (t) {
        var _this = this;
        t.field("me", {
            type: "User",
            resolve: function (parent, args, context) {
                var userId = utils_1.getUserId(context);
                return context.prisma.user({ id: userId });
            }
        });
        t.field("wallet", {
            type: "Wallet",
            resolve: function (parents, args, ctx) { return __awaiter(_this, void 0, void 0, function () {
                var userID, user, wallet;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            userID = utils_1.getUserId(ctx);
                            if (!userID) return [3 /*break*/, 3];
                            return [4 /*yield*/, ctx.prisma.user({
                                    id: userID
                                }).$fragment("{id wallet { id } }")];
                        case 1:
                            user = _a.sent();
                            return [4 /*yield*/, ctx.prisma.wallet({ id: user.wallet.id })];
                        case 2:
                            wallet = _a.sent();
                            return [2 /*return*/, wallet];
                        case 3: throw new Error("Unauthenticated user");
                    }
                });
            }); }
        });
        t.list.field("cards", {
            type: "Card",
            resolve: function (parent, args, context) {
                var userId = utils_1.getUserId(context);
                var cards = context.prisma.cards({
                    where: {
                        owner: {
                            id: userId
                        }
                    }
                });
                return cards;
            }
        });
        // Get all contracts that belong to a certain user.
        t.list.field('contracts', {
            type: 'Contract',
            resolve: function (parent, args, ctx) {
                var userId = utils_1.getUserId(ctx);
                return ctx.prisma.contracts({
                    where: { owner: { id: userId } }
                });
            }
        });
        t.field('contract', {
            type: 'Contract',
            nullable: true,
            args: { id: nexus_1.idArg() },
            resolve: function (parent, _a, ctx) {
                var id = _a.id;
                return ctx.prisma.contract({ id: id });
            }
        });
        t.list.field('bankcodes', {
            type: 'BankCode',
            resolve: function (parents, args, ctx) { return __awaiter(_this, void 0, void 0, function () {
                var raveService, response, e_1;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            _a.trys.push([0, 2, , 3]);
                            raveService = new rave_service_1["default"]();
                            return [4 /*yield*/, raveService.getBankAccounts()];
                        case 1:
                            response = _a.sent();
                            return [2 /*return*/, response];
                        case 2:
                            e_1 = _a.sent();
                            logger_1["default"].error(e_1.message);
                            throw e_1;
                        case 3: return [2 /*return*/];
                    }
                });
            }); }
        });
        t.list.field('bankaccounts', {
            type: 'BankAccount',
            resolve: function (parents, args, ctx) { return __awaiter(_this, void 0, void 0, function () {
                var userId, bankaccounts;
                return __generator(this, function (_a) {
                    try {
                        userId = utils_1.getUserId(ctx);
                        bankaccounts = ctx.prisma.bankAccounts({
                            where: {
                                owner: {
                                    id: userId
                                }
                            }
                        });
                        return [2 /*return*/, bankaccounts];
                    }
                    catch (err) {
                        logger_1["default"].error(err.message);
                        throw err;
                    }
                    return [2 /*return*/];
                });
            }); }
        });
        t.field("stats", {
            type: "Stats",
            resolve: function (parent, args, context) { return __awaiter(_this, void 0, void 0, function () {
                var userId, wallet, contracts, completed_contracts;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            userId = utils_1.getUserId(context);
                            if (!userId) return [3 /*break*/, 4];
                            return [4 /*yield*/, context.prisma.user({ id: userId }).wallet()];
                        case 1:
                            wallet = _a.sent();
                            return [4 /*yield*/, context.prisma.contracts({ where: { owner: { id: userId }, status_not: "INACTIVE" } })];
                        case 2:
                            contracts = _a.sent();
                            return [4 /*yield*/, context.prisma.contracts({ where: { owner: { id: userId }, status: "COMPLETED" } })];
                        case 3:
                            completed_contracts = _a.sent();
                            return [2 /*return*/, {
                                    wallet_balance: wallet.amount,
                                    active_contracts: contracts.length,
                                    completed_contracts: completed_contracts.length
                                }];
                        case 4: throw new Error("UNAUTHENTICATED USER");
                    }
                });
            }); }
        });
    }
});
exports["default"] = Query;
//# sourceMappingURL=query.js.map