"use strict";
exports.__esModule = true;
var nexus_prisma_1 = require("nexus-prisma");
// @ts-ignore
var MileStone = nexus_prisma_1.prismaObjectType({
    name: "MileStone",
    definition: function (t) {
        t.prismaFields([
            'id',
            'target',
            'paid',
            'available',
            'status',
            'disburseStatus',
            'state',
            'progress',
            'retraction_status'
        ]);
    }
});
exports["default"] = MileStone;
//# sourceMappingURL=MileStone.js.map