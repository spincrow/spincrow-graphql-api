"use strict";
exports.__esModule = true;
var nexus_prisma_1 = require("nexus-prisma");
// @ts-ignore
var BankAccount = nexus_prisma_1.prismaObjectType({
    name: "BankAccount",
    definition: function (t) {
        t.prismaFields([
            'id',
            'account_number',
            'bank_name',
            'bank_code',
            'created_at',
            'updated_at',
            'owner'
        ]);
    }
});
exports["default"] = BankAccount;
//# sourceMappingURL=BankAccount.js.map