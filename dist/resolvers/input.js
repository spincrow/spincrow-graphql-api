"use strict";
exports.__esModule = true;
var nexus_1 = require("nexus");
// Input types
var FundingTypeEnum = nexus_1.enumType({
    name: "FundingTypeEnum",
    members: ["WALLET", "CARD"]
});
var RetractionApprovedEnum = nexus_1.enumType({
    name: "RetractionApprovedEnum",
    members: ["APPROVED", "REJECTED"]
});
var TopUpCardTypeEnum = nexus_1.enumType({
    name: "TopUpCardTypeEnum",
    members: ["NEW_CARD", "CARD"]
});
var WithdrawalTypeEnum = nexus_1.enumType({
    name: "WithdrawalTypeEnum",
    members: ["NORMAL_WITHDRAWAL", "ADD_ACCOUNT"]
});
//  -- AUTHENTICATION INPUTS --
exports.SignupInput = nexus_1.inputObjectType({
    name: 'SignupInput',
    definition: function (t) {
        t.string('first_name', { nullable: false }),
            t.string('last_name', { nullable: false }),
            t.string('email', { nullable: false }),
            t.string('password', { nullable: false });
        t.string('phone', { nullable: true });
    }
});
exports.LoginInput = nexus_1.inputObjectType({
    name: 'LoginInput',
    definition: function (t) {
        t.string("password", { nullable: false }),
            t.string("email", { nullable: false });
    }
});
exports.PasswordUpdate = nexus_1.inputObjectType({
    name: "PasswordUpdateInput",
    definition: function (t) {
        t.string("currentPassword", { required: true });
        t.string("newPassword", { required: true });
    }
});
exports.ForgotPasswordEmailInput = nexus_1.inputObjectType({
    name: "ForgotPasswordEmailInput",
    definition: function (t) {
        t.string("email");
    }
});
exports.ForgotPasswordUpdateInput = nexus_1.inputObjectType({
    name: "ForgotPasswordUpdateInput",
    definition: function (t) {
        t.string("newpassword"),
            t.string("verification_code");
    }
});
// -- CONTRACT INPUTS -- 
exports.MilestoneInput = nexus_1.inputObjectType({
    name: 'MileStoneInput',
    definition: function (t) {
        t.float("target"),
            t.field("status", {
                type: "TransactionStateEnum",
                description: "Status represented by the TransactionStateEnum",
                required: false
            });
    }
});
exports.AccountDetail = nexus_1.inputObjectType({
    name: "AccountNoInput",
    definition: function (t) {
        t.string("account_no");
        t.string("bank_code");
        t.id("contract_id", { required: true });
        t.string("bank_name");
        t.string("account_name");
    }
});
exports.withdrawalRequestInput = nexus_1.inputObjectType({
    name: "WithdrawRequestInput",
    definition: function (t) {
        t.id("milestone_id");
        t.id("contract_id", { required: true });
        t.string("client_code");
        t.string("client_email");
        t.float("amount");
    }
});
exports.ContractInput = nexus_1.inputObjectType({
    name: "ContractInput",
    definition: function (t) {
        t.string("name", { nullable: false }),
            t.float("amount", { nullable: false });
        t.string('description', { nullable: true }),
            t.string('duration_start', { nullable: false }),
            t.string('duration_end', { nullable: false }),
            t.field('status', {
                type: "ContractStateEnum",
                description: "The contract's state",
                required: false,
                "default": 'INACTIVE'
            }),
            t.string('owner_type', { nullable: false }),
            t.string('payment_type', { nullable: false }),
            t.string('spread_type', { nullable: false });
        t.float('periodic_amount', { nullable: false }),
            t.float('whole_amount', { nullable: false }),
            t.string('periodic_type', { nullable: false }),
            t.list.field("milestones", { type: exports.MilestoneInput }),
            t.string("client", { nullable: false }),
            t.string("client_email", { nullable: false }),
            t.string("client_phone", { nullable: false });
    }
});
exports.TransactionInput = nexus_1.inputObjectType({
    name: "TransactionInput",
    definition: function (t) {
        t.string("contract", { nullable: false }),
            t.float("amount", { nullable: false }),
            t.field("status", {
                type: "TransactionStateEnum",
                description: "Status represented by the TransactionStateEnum"
            }),
            t.string("type", { nullable: false });
    }
});
exports.MilestoneUpdateInput = nexus_1.inputObjectType({
    name: "MilestoneUpdateInput",
    definition: function (t) {
        t.id('contract_id', { required: true }),
            t.id('milestone_id', { required: true }),
            t.float("progress", { required: false }),
            t.field("state", {
                type: "MilestoneStateEnum",
                description: "Detects if the state of the project is completed or incomplete",
                required: false
            });
    }
});
exports.MilestoneUpdateInputClient = nexus_1.inputObjectType({
    name: "MilestoneUpdateInputClient",
    definition: function (t) {
        t.id('contract_id', { required: true }),
            t.id('milestone_id', { required: true }),
            t.field("disburseStatus", {
                type: "DisburseStatusEnum",
                description: "Status of the disbursement",
                required: false
            }),
            t.field("status", {
                type: "TransactionStateEnum",
                description: "this set's the transaction state of the milestone PENDING, SUCCESSFUL, or FAILED ",
                required: false
            });
    }
});
exports.ContractWholeInput = nexus_1.inputObjectType({
    name: "ContractWholeInput",
    definition: function (t) {
        t.id("contract_id", { required: true });
    }
});
exports.ContractUpdateInput = nexus_1.inputObjectType({
    name: "ContractUpdateInput",
    definition: function (t) {
        t.id('contract_id', { required: true }),
            t.field('data', { type: "ContractInput" });
    }
});
exports.ContractStateUpdate = nexus_1.inputObjectType({
    name: "ContractActivateInput",
    definition: function (t) {
        t.id("contract_id", { required: true }),
            t.float('amount', { nullable: true }),
            t.string('transactionCode'),
            t.field('contract_state', {
                type: "ContractStateEnum",
                required: true
            });
    }
});
exports.ContractStateUpdateWithPay = nexus_1.inputObjectType({
    name: "ContractActivateWithPay",
    definition: function (t) {
        t.id("contract_id", { required: true });
        t.float('amount', { nullable: true });
        t.field('funding_type', { type: FundingTypeEnum, required: true });
        t.id('card_id', { required: false });
        // t.id('wallet_id', {required: false})
    }
});
exports.ContractStateUpdateWithCard = nexus_1.inputObjectType({
    name: "ContractActivateWithCard",
    definition: function (t) {
        t.id("contract_id", { required: true });
        t.float('amount', { nullable: true });
        t.string('tx_ref', { required: false });
        // t.id('wallet_id', {required: false})
    }
});
//  ---- USER INFORMATION INPUTS -- 
exports.userUpdateInput = nexus_1.inputObjectType({
    name: "UserUpdateInput",
    definition: function (t) {
        t.id("user_id", { required: true });
        t.string("first_name");
        t.string("last_name");
        t.string("phone_num");
    }
});
exports.SMSVerificationInput = nexus_1.inputObjectType({
    name: "SMSVerificationInput",
    definition: function (t) {
        t.string("code", { required: true });
        t.float('amount', { nullable: true });
    }
});
exports.CardInput = nexus_1.inputObjectType({
    name: "CardInput",
    definition: function (t) {
        t.string("tx_ref", { required: true });
    }
});
exports.CardFullInput = nexus_1.inputObjectType({
    name: "CardFullInput",
    definition: function (t) {
        t.string("txRef");
    }
});
exports.ConfirmCodeInput = nexus_1.inputObjectType({
    name: "ConfirmCodeInput",
    definition: function (t) {
        t.string("code", { required: true });
    }
});
exports.BankAccountInput = nexus_1.inputObjectType({
    name: "BankAccountInput",
    definition: function (t) {
        t.string('account_number', { required: true });
        t.string('bank_name', { required: true });
        t.string("account_name");
        t.string('bank_code');
    }
});
exports.TopUpInput = nexus_1.inputObjectType({
    name: "TopupInput",
    definition: function (t) {
        t.id("card_id");
        t.string("tx_ref");
        t.field('card_type', { type: TopUpCardTypeEnum, required: true, "default": "CARD" });
        t.float("amount", { required: true });
    }
});
exports.WithdrawInput = nexus_1.inputObjectType({
    name: "WithdrawInput",
    definition: function (t) {
        t.id("bank_account_id");
        t.field('withdrawal_type', { type: WithdrawalTypeEnum, required: true, "default": "NORMAL_WITHDRAWAL" });
        t.string('account_number');
        t.string('bank_name');
        t.string("account_name");
        t.string('bank_code');
        t.float("amount", { required: true });
    }
});
exports.RetractionRequestInput = nexus_1.inputObjectType({
    name: "RetractionRequestInput",
    definition: function (t) {
        t.id("contract_id", { required: true });
        t.id("milestone_id");
    }
});
exports.RetractionApprovalRequestInput = nexus_1.inputObjectType({
    name: "RetractionApprovalRequestInput",
    definition: function (t) {
        t.id("contract_id", { required: true });
        t.id("milestone_id"),
            t.field("approve_status", { type: RetractionApprovedEnum, required: true, "default": "APPROVED" });
    }
});
exports.CancelRetractionInput = nexus_1.inputObjectType({
    name: "CancelRetractionInput",
    definition: function (t) {
        t.id("contract_id", { required: true });
        t.id("milestone_id");
    }
});
//# sourceMappingURL=input.js.map