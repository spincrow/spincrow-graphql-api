"use strict";
exports.__esModule = true;
var nexus_prisma_1 = require("nexus-prisma");
// @ts-ignore
var User = nexus_prisma_1.prismaObjectType({
    name: "User",
    definition: function (t) {
        t.prismaFields([
            'id',
            'first_name',
            'last_name',
            'phone',
            'profile_image',
            'email',
            'verification_code',
            'wallet',
            'contracts',
            'home_tour',
            'contract_tour',
            "single_contract_tour",
            "settings_tour",
            "wallet_tour",
            "password_verification_code"
        ]);
    }
});
exports["default"] = User;
//# sourceMappingURL=User.js.map