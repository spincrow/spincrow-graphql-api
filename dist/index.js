"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _this = this;
exports.__esModule = true;
require('dotenv').config();
var apollo_server_express_1 = require("apollo-server-express");
var nexus_prisma_1 = require("nexus-prisma");
var path = __importStar(require("path"));
var nexus_prisma_2 = __importDefault(require("./generated/nexus-prisma"));
var prisma_client_1 = require("./generated/prisma-client");
var Types = __importStar(require("./resolvers"));
var permissions_1 = require("./permissions");
var agenda_service_1 = __importDefault(require("./services/agenda-service"));
var sentry_1 = __importDefault(require("./services/sentry"));
// sentry logger
var webhooks_1 = __importDefault(require("./webhooks"));
sentry_1["default"].init({
    dsn: process.env.SENTRY_LOG_URL,
    environment: process.env.NODE_ENV
});
var schema = nexus_prisma_1.makePrismaSchema({
    // Provide all the GraphQL types we've implemented
    types: Types,
    // Configure the interface to Prisma
    prisma: {
        datamodelInfo: nexus_prisma_2["default"],
        client: prisma_client_1.prisma
    },
    // Specify where Nexus should put the generated files
    outputs: {
        schema: path.join(__dirname, './generated/schema.graphql'),
        typegen: path.join(__dirname, './generated/nexus.ts')
    },
    // Configure nullability of input arguments: All arguments are non-nullable by default
    nonNullDefaults: {
        input: false,
        output: false
    },
    // Configure automatic type resolution for the TS representations of the associated types
    typegenAutoConfig: {
        sources: [
            {
                source: path.join(__dirname, './types.ts'),
                alias: 'types'
            },
        ],
        contextType: 'types.Context'
    }
});
var server = new apollo_server_express_1.ApolloServer({
    schema: schema,
    middleware: [permissions_1.permissions],
    context: function (request) { return (__assign({}, request, { prisma: prisma_client_1.prisma })); },
    introspection: true,
    playground: true
});
// server.listen({ port: process.env.PORT || 5000 }, async () => {
//   await agenda.start();
//   console.log(`🚀 Server ready, let roll 😎 on port ${process.env.PORT || 5000}`)
// })
server.applyMiddleware({ app: webhooks_1["default"] });
webhooks_1["default"].listen({ port: process.env.PORT || 5000 }, function () { return __awaiter(_this, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, agenda_service_1["default"].start()];
            case 1:
                _a.sent();
                console.log("\uD83D\uDE80 Server ready at http://localhost:5000" + server.graphqlPath);
                console.log("\uD83D\uDE80 Webhook running on http://localhost:5000/webhook");
                return [2 /*return*/];
        }
    });
}); });
//# sourceMappingURL=index.js.map