"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var keymirror_1 = __importDefault(require("keymirror"));
exports["default"] = keymirror_1["default"]({
    BANK_TRANSFER_TRANSACTION: null,
    CARD_TRANSACTION: null,
    BARTER_TRANSACTION: null,
    ACCOUNT_TRANSACTION: null,
    Transfer: null
});
//# sourceMappingURL=transaction_types.js.map