"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _this = this;
exports.__esModule = true;
var express_1 = __importDefault(require("express"));
var body_parser_1 = __importDefault(require("body-parser"));
var transaction_types_1 = __importDefault(require("./transaction_types"));
var logger_1 = __importDefault(require("../services/logger"));
var agenda_service_1 = __importDefault(require("../services/agenda-service"));
var monnify_nodejs_1 = __importDefault(require("@legobox/monnify-nodejs"));
var prisma_client_1 = require("../generated/prisma-client");
var manageTransferWebHooksForContracts = function (contractTransaction, transferObject) { return __awaiter(_this, void 0, void 0, function () {
    var contractTransactionFragmented, contractTransactionUpdate, _a, contractTransactionUpdate, contract, _b;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0: return [4 /*yield*/, prisma_client_1.prisma.contractTransaction({ id: contractTransaction.id }).$fragment("\n        amount\n        id\n        milestone {\n            id\n        }\n        status\n    ")
                // check if tis a milestone's baby.
            ];
            case 1:
                contractTransactionFragmented = _c.sent();
                if (!contractTransactionFragmented.milestone) return [3 /*break*/, 10];
                contractTransactionUpdate = null;
                _a = transferObject.status;
                switch (_a) {
                    case "SUCCESSFUL": return [3 /*break*/, 2];
                    case "FAILED": return [3 /*break*/, 5];
                }
                return [3 /*break*/, 7];
            case 2: return [4 /*yield*/, prisma_client_1.prisma.updateContractTransaction({
                    data: {
                        status: "SUCCESSFUL"
                    },
                    where: {
                        id: contractTransactionFragmented.id
                    }
                })
                // update contract state
            ];
            case 3:
                contractTransactionUpdate = _c.sent();
                // update contract state
                return [4 /*yield*/, prisma_client_1.prisma.updateMileStone({
                        where: {
                            id: contractTransactionFragmented.milestone.id
                        },
                        data: {
                            state: "COMPLETE",
                            status: "SUCCESSFUL",
                            disburseStatus: "CONFIRMED",
                            paid: true
                        }
                    })
                    // return contractTransaction;
                ];
            case 4:
                // update contract state
                _c.sent();
                // return contractTransaction;
                return [3 /*break*/, 9];
            case 5: return [4 /*yield*/, prisma_client_1.prisma.updateContractTransaction({
                    data: {
                        status: "FAILED"
                    },
                    where: {
                        id: contractTransactionFragmented.id
                    }
                })
                // return contractTransaction;
            ];
            case 6:
                contractTransactionUpdate = _c.sent();
                // return contractTransaction;
                return [3 /*break*/, 9];
            case 7: return [4 /*yield*/, prisma_client_1.prisma.updateContractTransaction({
                    data: {
                        status: "FAILED"
                    },
                    where: {
                        id: contractTransactionFragmented.id
                    }
                })];
            case 8:
                contractTransactionUpdate = _c.sent();
                return [3 /*break*/, 9];
            case 9: return [3 /*break*/, 19];
            case 10:
                contractTransactionUpdate = null;
                return [4 /*yield*/, prisma_client_1.prisma.contractTransaction({
                        id: contractTransactionFragmented.id
                    }).contract()];
            case 11:
                contract = _c.sent();
                _b = transferObject.status;
                switch (_b) {
                    case "SUCCESSFUL": return [3 /*break*/, 12];
                    case "FAILED": return [3 /*break*/, 15];
                }
                return [3 /*break*/, 17];
            case 12: return [4 /*yield*/, prisma_client_1.prisma.updateContractTransaction({
                    data: {
                        status: "SUCCESSFUL"
                    },
                    where: {
                        id: contractTransactionFragmented.id
                    }
                })
                // update contract state
            ];
            case 13:
                contractTransactionUpdate = _c.sent();
                // update contract state
                return [4 /*yield*/, prisma_client_1.prisma.updateContract({
                        where: { id: contract.id },
                        data: {
                            paid: true,
                            disburseStatus: "CONFIRMED",
                            status: "COMPLETED"
                        }
                    })];
            case 14:
                // update contract state
                _c.sent();
                return [3 /*break*/, 19];
            case 15: return [4 /*yield*/, prisma_client_1.prisma.updateContractTransaction({
                    data: {
                        status: "FAILED"
                    },
                    where: {
                        id: contractTransaction.id
                    }
                })
                // return contractTransaction;
            ];
            case 16:
                contractTransactionUpdate = _c.sent();
                // return contractTransaction;
                return [3 /*break*/, 19];
            case 17: return [4 /*yield*/, prisma_client_1.prisma.updateContractTransaction({
                    data: {
                        status: "FAILED"
                    },
                    where: {
                        id: contractTransaction.id
                    }
                })];
            case 18:
                contractTransactionUpdate = _c.sent();
                return [3 /*break*/, 19];
            case 19: return [2 /*return*/];
        }
    });
}); };
var manageTransferWebhooksForWallets = function (walletTransaction, transferObject) { return __awaiter(_this, void 0, void 0, function () {
    var walletUpdate, _a, wallet;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                walletUpdate = null;
                _a = transferObject.status;
                switch (_a) {
                    case "SUCCESSFUL": return [3 /*break*/, 1];
                    case "FAILED": return [3 /*break*/, 5];
                }
                return [3 /*break*/, 7];
            case 1: return [4 /*yield*/, prisma_client_1.prisma.updateWalletTransaction({
                    data: {
                        status: "SUCCESSFUL"
                    },
                    where: {
                        id: walletTransaction.id
                    }
                })];
            case 2:
                walletUpdate = _b.sent();
                return [4 /*yield*/, prisma_client_1.prisma.walletTransaction({ id: walletTransaction.id }).wallet()];
            case 3:
                wallet = _b.sent();
                return [4 /*yield*/, prisma_client_1.prisma.updateWallet({
                        data: {
                            amount: wallet.amount - transferObject.amount
                        },
                        where: {
                            id: wallet.id
                        }
                    })
                    // return walletUpdate;
                ];
            case 4:
                _b.sent();
                // return walletUpdate;
                return [3 /*break*/, 9];
            case 5: return [4 /*yield*/, prisma_client_1.prisma.updateWalletTransaction({
                    data: {
                        status: "FAILED"
                    },
                    where: {
                        id: walletTransaction.id
                    }
                })
                // return walletUpdate;
            ];
            case 6:
                walletUpdate = _b.sent();
                // return walletUpdate;
                return [3 /*break*/, 9];
            case 7: return [4 /*yield*/, prisma_client_1.prisma.updateWalletTransaction({
                    data: {
                        status: "FAILED"
                    },
                    where: {
                        id: walletTransaction.id
                    }
                })];
            case 8:
                walletUpdate = _b.sent();
                return [3 /*break*/, 9];
            case 9: return [2 /*return*/, null];
        }
    });
}); };
// Match the raw body to content type application/json
var app = express_1["default"]();
app.post('/webhook/rave', body_parser_1["default"].raw({ type: 'application/json' }), function (request, response) { return __awaiter(_this, void 0, void 0, function () {
    var hash, secret_hash, request_json, _a, contracts, contract, wallets, wallet, updatedContract, transactions, updatedWallet, transferObject, walletTransactions, contractTransactions, e_1;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                hash = request.headers["verif-hash"];
                if (!hash) {
                    // discard the request,only a post with rave signature header gets our attention 
                    return [2 /*return*/, response.send("NOT A VALID REQUEST")];
                }
                secret_hash = process.env.SPINCROW_WEBHOOK_HASH;
                // check if signatures match
                if (hash !== secret_hash) {
                    // silently exit, or check that you are passing the write hash on your server.
                    return [2 /*return*/, response.send("NOT A VALID REQUEST")];
                }
                _b.label = 1;
            case 1:
                _b.trys.push([1, 21, , 22]);
                request_json = JSON.parse(request.body);
                _a = request_json['event.type'];
                switch (_a) {
                    case transaction_types_1["default"].CARD_TRANSACTION: return [3 /*break*/, 2];
                    case transaction_types_1["default"].BANK_TRANSFER_TRANSACTION: return [3 /*break*/, 3];
                    case transaction_types_1["default"].Transfer: return [3 /*break*/, 12];
                }
                return [3 /*break*/, 19];
            case 2: return [3 /*break*/, 20];
            case 3:
                if (!(request_json.status == "successful")) return [3 /*break*/, 11];
                return [4 /*yield*/, prisma_client_1.prisma.contracts({ where: { va_order_ref: request_json.orderRef } })];
            case 4:
                contracts = _b.sent();
                contract = contracts[0];
                return [4 /*yield*/, prisma_client_1.prisma.wallets({ where: { va_order_ref: request_json.orderRef } })];
            case 5:
                wallets = _b.sent();
                wallet = wallets[0];
                if (!contract) return [3 /*break*/, 8];
                if (!(contract.amount == request_json.charged_amount)) return [3 /*break*/, 7];
                return [4 /*yield*/, prisma_client_1.prisma.updateContract({
                        where: {
                            id: contract.id
                        },
                        data: {
                            status: "ACTIVE",
                            transactions: {
                                create: {
                                    amount: request_json.charged_amount,
                                    flow: "CREDIT",
                                    details: "FUNDED CONTRACT by Bank Transfer by " + request_json.customer.email,
                                    transaction_code: request_json.txRef,
                                    status: "SUCCESSFUL"
                                }
                            }
                        }
                    })];
            case 6:
                updatedContract = _b.sent();
                return [3 /*break*/, 8];
            case 7:
                logger_1["default"].info("Contract " + contract.id + " was not updated on payment of " + request_json.charged_amount);
                _b.label = 8;
            case 8:
                if (!wallet) return [3 /*break*/, 11];
                return [4 /*yield*/, prisma_client_1.prisma.walletTransactions({ where: { transaction_code: request_json.txRef } })];
            case 9:
                transactions = _b.sent();
                if (!transactions[0]) return [3 /*break*/, 11];
                return [4 /*yield*/, prisma_client_1.prisma.updateWallet({
                        where: {
                            id: wallet.id
                        },
                        data: {
                            amount: (wallet.amount + request_json.charged_amount),
                            transactions: {
                                create: {
                                    amount: request_json.charged_amount,
                                    flow: "CREDIT",
                                    details: "FUNDED wallet by Bank Transfer by " + request_json.customer.email,
                                    transaction_code: request_json.txRef,
                                    status: "SUCCESSFUL"
                                }
                            }
                        }
                    }).$fragment("{\n                                id \n                                owner {\n                                    id\n                                    first_name\n                                    last_name\n                                    phone\n                                }\n                            }")];
            case 10:
                updatedWallet = _b.sent();
                if (updatedWallet) {
                    // start new agenda to inform user of wallet funding status.
                    agenda_service_1["default"].now("sendWalletTransferFunding", {
                        user: {
                            name: updatedWallet.owner.first_name,
                            amount: request_json.charged_amount,
                            phone: updatedWallet.owner.phone
                        }
                    });
                }
                _b.label = 11;
            case 11: 
            // update contract according to transaction
            return [3 /*break*/, 20];
            case 12:
                transferObject = request_json.transfer;
                return [4 /*yield*/, prisma_client_1.prisma.walletTransactions({
                        where: { transaction_code: transferObject.reference }
                    })];
            case 13:
                walletTransactions = _b.sent();
                return [4 /*yield*/, prisma_client_1.prisma.contractTransactions({
                        where: { transaction_code: transferObject.reference }
                    })];
            case 14:
                contractTransactions = _b.sent();
                if (!walletTransactions[0]) return [3 /*break*/, 16];
                // there exists a piece. so we can make the transactions come true.
                // check the state of the contract and update
                return [4 /*yield*/, manageTransferWebhooksForWallets(walletTransactions[0], transferObject)];
            case 15:
                // there exists a piece. so we can make the transactions come true.
                // check the state of the contract and update
                _b.sent();
                _b.label = 16;
            case 16:
                if (!(contractTransactions.length > 0)) return [3 /*break*/, 18];
                // update the first one
                // update the contract
                return [4 /*yield*/, manageTransferWebHooksForContracts(contractTransactions[0], transferObject)];
            case 17:
                // update the first one
                // update the contract
                _b.sent();
                _b.label = 18;
            case 18: 
            // update the transferObject from prisma to update the  transfer object.
            return [3 /*break*/, 20];
            case 19: return [3 /*break*/, 20];
            case 20:
                response.sendStatus(200);
                return [3 /*break*/, 22];
            case 21:
                e_1 = _b.sent();
                logger_1["default"].error(e_1.message);
                response.send("e.message");
                return [3 /*break*/, 22];
            case 22: return [2 /*return*/];
        }
    });
}); });
app.get('/webhook', body_parser_1["default"].raw({ type: 'application/json' }), function (request, response) {
    return response.json({ text: "Spincrow api webhook" });
});
app.post('/webhook/monnify', body_parser_1["default"].raw({ type: 'application/json' }), function (request, response) { return __awaiter(_this, void 0, void 0, function () {
    var monifyBase, request_json, hashedSet, _a, contracts, contract, wallets, wallet, updatedContract, updatedWallet, e_2;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                monifyBase = new monnify_nodejs_1["default"]();
                request_json = JSON.parse(request.body);
                hashedSet = monifyBase.generateTransactionHash({
                    paymentReference: request_json['paymentReference'],
                    amountPaid: request_json['amountPaid'],
                    paidOn: request_json['paidOn'],
                    transactionReference: request_json['transactionReference']
                });
                if (request_json['transactionHash'] !== hashedSet) {
                    // discard the request,only a post with rave signature header gets our attention 
                    return [2 /*return*/, response.send("NOT A VALID REQUEST")];
                }
                _b.label = 1;
            case 1:
                _b.trys.push([1, 11, , 12]);
                _a = request_json['paymentMethod'];
                switch (_a) {
                    case "ACCOUNT_TRANSFER": return [3 /*break*/, 2];
                }
                return [3 /*break*/, 9];
            case 2: return [4 /*yield*/, prisma_client_1.prisma.contracts({ where: { va_order_ref: request_json.product.reference } })];
            case 3:
                contracts = _b.sent();
                contract = contracts[0];
                return [4 /*yield*/, prisma_client_1.prisma.wallets({ where: { va_order_ref: request_json.product.reference } })];
            case 4:
                wallets = _b.sent();
                wallet = wallets[0];
                if (!contract) return [3 /*break*/, 7];
                if (!(contract.amount <= parseFloat(request_json.amountPaid))) return [3 /*break*/, 6];
                return [4 /*yield*/, prisma_client_1.prisma.updateContract({
                        where: {
                            id: contract.id
                        },
                        data: {
                            status: "ACTIVE",
                            transactions: {
                                create: {
                                    amount: parseFloat(request_json.amountPaid),
                                    flow: "CREDIT",
                                    details: "FUNDED CONTRACT by Bank Transfer by " + request_json.customer.email,
                                    transaction_code: request_json.transactionReference,
                                    status: "SUCCESSFUL"
                                }
                            }
                        }
                    })];
            case 5:
                updatedContract = _b.sent();
                console.log(updatedContract);
                return [3 /*break*/, 7];
            case 6:
                logger_1["default"].info("Contract " + contract.id + " was not updated on payment of " + request_json.amountPaid);
                _b.label = 7;
            case 7:
                if (!wallet) return [3 /*break*/, 9];
                return [4 /*yield*/, prisma_client_1.prisma.updateWallet({
                        where: {
                            id: wallet.id
                        },
                        data: {
                            amount: (wallet.amount + parseFloat(request_json.amountPaid)),
                            transactions: {
                                create: {
                                    amount: parseFloat(request_json.amountPaid),
                                    flow: "CREDIT",
                                    details: "FUNDED wallet by Bank Transfer by " + request_json.customer.email,
                                    transaction_code: request_json.transactionReference,
                                    status: "SUCCESSFUL"
                                }
                            }
                        }
                    }).$fragment("{\n                        id \n                        owner {\n                            id\n                            first_name\n                            last_name\n                            phone\n                        }\n                    }")];
            case 8:
                updatedWallet = _b.sent();
                if (updatedWallet) {
                    // start new agenda to inform user of wallet funding status.
                    agenda_service_1["default"].now("sendWalletTransferFunding", {
                        user: {
                            name: updatedWallet.owner.first_name,
                            amount: parseFloat(request_json.amountPaid),
                            phone: updatedWallet.owner.phone
                        }
                    });
                }
                _b.label = 9;
            case 9: return [3 /*break*/, 10];
            case 10:
                response.sendStatus(200);
                return [3 /*break*/, 12];
            case 11:
                e_2 = _b.sent();
                logger_1["default"].error(e_2.message);
                response.send("e.message");
                return [3 /*break*/, 12];
            case 12: return [2 /*return*/];
        }
    });
}); });
app.get('/webhook/monnify', body_parser_1["default"].raw({ type: 'application/json' }), function (request, response) {
    return response.json({ text: "Spincrow api webhook for monnify" });
});
// app.listen(8000, () => console.log('Running on port 8000'));
exports["default"] = app;
//# sourceMappingURL=index.js.map