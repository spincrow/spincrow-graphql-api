require('dotenv').config()
import Monnify from "../services/monnify-service"


describe("test monnify service", () => {
    let monnifyService: any = null
    beforeAll(() => {
        monnifyService = new Monnify()
    })

    it("Test the authentication", async () => {
        // let monifyService = new Monnify
        try{
            let response = await monnifyService.authenticate()
            console.log(response)
        }catch(e){
            console.log(e.message)
        }
    })
})