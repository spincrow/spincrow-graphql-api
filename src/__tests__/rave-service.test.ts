require('dotenv').config()
import RaveService from "../services/rave-service"

type BankDetailResponse = {
    account_number: string | String,
    account_name: string | String
}

describe("test monnify service", () => {
    let raveService: any = null
    it("Test the bank resolving", async () => {
        // let monifyService = new Monnify
        let raveService = new RaveService()
        try{
            let response = await raveService.getBankDetails({
                account_no: "3388308230",
                account_bank:"057"
            })
            console.log(response)
            let baseObject = {
                account_number: "93e93939349",
                account_name: "Your account name"
            }
            expect(response).toMatchObject<BankDetailResponse>({...baseObject})
        }catch(e){
            console.log(`caught error here ${e.stack}`)
        }
    })
})