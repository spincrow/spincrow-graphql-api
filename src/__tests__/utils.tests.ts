/**
 * Testing the utils
 */
require('dotenv').config()
import * as utils from "../utils";
import { prisma } from '../generated/prisma-client'
import { Context } from "../types";
import {mockRequest} from "mock-req-res"
import RaveService from "../services/rave-service"


describe("testing the utils functions", () => {

    let context:Context = {
        prisma: null,
        req: null
    }
    beforeEach(() => {
        context  = {
            ...context,
            prisma: prisma,
            req: mockRequest({
                headers: {
                    Authorization: "OUR AUTH KEY"
                }
            })
        }
    });

    test("test the getUserId function", () => {
        // create a context and sent it to get getUser ID and review the result
        let response = utils.getUserId(context);
        console.log(response)
        expect(response).toBe(true)
    })

    test("test that rave returns bank details", async () => {
        try {
            let raveService = new RaveService();
            let response = await raveService.getBankAccounts();
            expect(response).toBeInstanceOf(Array)
        } catch (e) {
            throw e
        }
    })
})