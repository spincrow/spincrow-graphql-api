import { Prisma } from './generated/prisma-client'
import {Request} from "express"

export interface Context {
  prisma: Prisma
  req: Request 
}


export interface File {
  id: string,
  path: string,
  filename: string,
  mimetype: string,
  encoding: string
}