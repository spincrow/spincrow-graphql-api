import express from "express"
import bodyParser from "body-parser"
import webhooksType from "./transaction_types";
import logger from "../services/logger"
import agenda from "../services/agenda-service"

import monnify from "@legobox/monnify-nodejs"

import {prisma, Contract, Wallet, WalletTransaction, ContractTransaction} from "../generated/prisma-client"
import { generateActivityForUser } from "../services/extra-jobs";


const manageTransferWebHooksForContracts = async (contractTransaction: ContractTransaction, transferObject: {status: string, amount: number}) : Promise<any> => {
    // manage contracts transfer for milestones or whole payments
    // let contractTransaction = await prisma.contractTransaction({id: contractTransaction.id})
    let contract = await prisma.contractTransaction({id: contractTransaction.id}).contract()
    let owner = await prisma.contractTransaction({id: contractTransaction.id}).contract().owner();
    let milestone = await await prisma.contractTransaction({id: contractTransaction.id}).milestone()
    // check if tis a milestone's baby.
    if(milestone){
        // if there is a miestone linked to the transaction. 
        let contractTransactionUpdate = null;
        switch (transferObject.status) {
            case "SUCCESSFUL":
                contractTransactionUpdate = await prisma.updateContractTransaction({
                    data: {
                        status: "SUCCESSFUL"
                    },
                    where: {
                        id: contractTransaction.id
                    }
                })
                // update contract state
                await prisma.updateMileStone({
                    where: {
                        id: milestone.id
                    },
                    data: {
                        state: "COMPLETE",
                        status: "SUCCESSFUL",
                        withdrawalStatus: "COMPLETED",
                        disburseStatus: "CONFIRMED",
                        paid: true
                    }
                })
                await generateActivityForUser({
                    user_id: owner.id,
                    detail: `Disbursed on milestone for contract ${contract.name} has been successfully withdrawn`,
                    amount: parseFloat(transferObject.amount.toString())
                })
                // return contractTransaction;
                break;
            case "FAILED":
                contractTransactionUpdate = await prisma.updateContractTransaction({
                    data: {
                        status: "FAILED"
                    },
                    where: {
                        id: contractTransaction.id
                    }
                })
                await prisma.updateMileStone({
                    where: {
                        id: milestone.id
                    },
                    data: {
                        withdrawalStatus: "FAILED"
                    }
                })
                await generateActivityForUser({
                    user_id: owner.id,
                    detail: `Attempts to withdraw funds for milstone on contract ${contract.name} failed`,
                    amount: parseFloat(transferObject.amount.toString())
                })
                // return contractTransaction;
                break;
            default:
                contractTransactionUpdate = await prisma.updateContractTransaction({
                    data: {
                        status: "FAILED"
                    },
                    where: {
                        id: contractTransaction.id
                    }
                })
                break;
        }
    }else{
        let contractTransactionUpdate = null;
        let contract = await prisma.contractTransaction({
            id: contractTransaction.id
        }).contract()
        switch (transferObject.status) {
            case "SUCCESSFUL":
                contractTransactionUpdate = await prisma.updateContractTransaction({
                    data: {
                        status: "SUCCESSFUL"
                    },
                    where: {
                        id: contractTransaction.id
                    }
                })
                // update contract state
                await prisma.updateContract({
                    where:{id: contract.id},
                    data: {
                        paid: true,
                        disburseStatus: "CONFIRMED",
                        status: "COMPLETED",
                        withdrawalStatus: "COMPLETED"
                    }
                })
                await generateActivityForUser({
                    user_id: owner.id,
                    detail: `Disbursed  contract ${contract.name} has been successfully withdrawn`,
                    amount: parseFloat(transferObject.amount.toString())
                })
                break;
            case "FAILED":
                contractTransactionUpdate = await prisma.updateContractTransaction({
                    data: {
                        status: "FAILED",
                    },
                    where: {
                        id: contractTransaction.id
                    }
                })
                await prisma.updateContract({
                    where:{id: contract.id},
                    data: {
                        withdrawalStatus: "FAILED"
                    }
                })
                await generateActivityForUser({
                    user_id: owner.id,
                    detail: `Attempts to withdraw funds for contract ${contract.name} failed`,
                    amount: parseFloat(transferObject.amount.toString())
                })
                // return contractTransaction;
                break;
            default:
                contractTransactionUpdate = await prisma.updateContractTransaction({
                    data: {
                        status: "FAILED"
                    },
                    where: {
                        id: contractTransaction.id
                    }
                })
                break;
        }
        // it's a milestone transaction.
    }
}

const manageTransferWebhooksForWallets : any = async (walletTransaction: WalletTransaction, transferObject: {status: string, amount: number}): Promise<any> => {
     // manage contracts transfer for milestones or whole payments
     let walletUpdate = null
     let owner = await prisma.walletTransaction({id: walletTransaction.id}).wallet().owner()
     switch (transferObject.status) {
         case "SUCCESSFUL":
             walletUpdate = await prisma.updateWalletTransaction({
                 data: {
                     status: "SUCCESSFUL"
                 },
                 where: {
                     id: walletTransaction.id
                 }
             })
             await generateActivityForUser({
                user_id: owner.id,
                detail: `Wallet withdrawal successful`,
                amount: parseFloat(transferObject.amount.toString())
            })
             // return walletUpdate;
             break;
         case "FAILED":
             walletUpdate = await prisma.updateWalletTransaction({
                 data: {
                     status: "FAILED",
                     details: "Withdrawal failed for this transaction"
                 },
                 where: {
                     id: walletTransaction.id
                 }
             })
             let wallet = await prisma.walletTransaction({id: walletTransaction.id}).wallet()
             await prisma.updateWallet({
                 data: {
                     amount: wallet.amount + transferObject.amount
                 },
                 where: {
                     id: wallet.id
                 }
             })
             await generateActivityForUser({
                user_id: owner.id,
                detail: `Wallet withdrawal unsuccessful`,
                amount: parseFloat(transferObject.amount.toString())
            })
             // return walletUpdate;
             break;
         default:
            let walletUpdateDefault = await prisma.updateWalletTransaction({
                data: {
                    status: "FAILED",
                    details: "Withdrawal failed for this transaction"
                },
                where: {
                    id: walletTransaction.id
                }
            })
            let baseWallet = await prisma.walletTransaction({id: walletTransaction.id}).wallet()
            await prisma.updateWallet({
                data: {
                    amount: baseWallet.amount + transferObject.amount
                },
                where: {
                    id: baseWallet.id
                }
            })
            await generateActivityForUser({
               user_id: owner.id,
               detail: `Wallet withdrawal unsuccessful`,
               amount: parseFloat(transferObject.amount.toString())
           })
             break;
     }
     return null;
}

// Match the raw body to content type application/json
const app = express();
app.post('/webhook/rave', bodyParser.raw({type: 'application/json'}), async (request, response) => {

    // logger.info(JSON.stringify(request.body))
    var hash = request.headers["verif-hash"];

    if(!hash) {
    // discard the request,only a post with rave signature header gets our attention 
    return response.send("NOT A VALID REQUEST")
    }

    // Get signature stored as env variable on your server
    const secret_hash = process.env.SPINCROW_WEBHOOK_HASH;

    // check if signatures match

    if(hash !== secret_hash) {
    // silently exit, or check that you are passing the write hash on your server.
    return response.send("NOT A VALID REQUEST")
    }

    // Retrieve the request's body
    
    try {
        var request_json = JSON.parse(request.body);
        logger.info(JSON.stringify({name:"Webhook request", ...request_json}))
        switch(request_json['event.type']){
            case webhooksType.CARD_TRANSACTION:
                break;
            case webhooksType.BANK_TRANSFER_TRANSACTION:
                // a transfer from a bank account
                // a bank transfer transaction is either a contract funded or a wallet funded
                if(request_json.status == "successful"){
                    let contracts = await (await prisma.contracts({where: {va_order_ref: request_json.orderRef }}))
                    let contract = contracts[0]
    
                    let wallets = await prisma.wallets({where: {va_order_ref: request_json.orderRef}})
                    let wallet = wallets[0]
                    // if contract exists then we update the contract status to funded
                    if(contract){
                        let transactions = await prisma.contract({id: contract.id}).transactions({where: {transaction_code: request_json.txRef}})
                        let owner = await prisma.contract({id: contract.id}).owner()
                        if(transactions.length < 1){
                            if(contract.amount == request_json.charged_amount){
                                let updatedContract = await prisma.updateContract({
                                    where: {
                                        id: contract.id
                                    },
                                    data: {
                                        status: "ACTIVE",
                                        transactions: {
                                            create: {
                                                amount: request_json.charged_amount,
                                                flow: "CREDIT",
                                                details: `FUNDED CONTRACT by Bank Transfer by ${request_json.customer.email}`,
                                                transaction_code: request_json.txRef,
                                                status: "SUCCESSFUL"
                                            }
                                        }
                                    }
                                })
                                await agenda.now('funded_contract_by_transfer', {
                                    user: {
                                        name: updatedContract.client.split(" ")[0],
                                        phone: updatedContract.client_phone,
                                        email: updatedContract.client_email
                                    },
                                    actor: {
                                        name: owner.first_name,
                                        sendcode: true
                                    },
                                    contract: {
                                        name: updatedContract.name,
                                        amount: updatedContract.amount,
                                        client_code : updatedContract.client_code,
                                        id: updatedContract.id
                                    }
                                })
                                await agenda.now('funded_contract_by_transfer', {
                                    user: {
                                        name: owner.first_name,
                                        phone: owner.phone,
                                        email: owner.email
                                    },
                                    actor: {
                                        name: updatedContract.client.split(" ")[0],
                                        sendcode: false
                                    },
                                    contract: {
                                        name: updatedContract.name,
                                        amount: updatedContract.amount,
                                        client_code : updatedContract.client_code,
                                        id: updatedContract.id
                                    }
                                })

                                await generateActivityForUser({
                                    user_id: owner.id,
                                    detail: `Your contract ${contract.name} has just been funded by Bank transfer`,
                                    amount: parseFloat(request_json.charged_amount.toString())
                                })
                            }else{
                                logger.info(`Contract ${contract.id} was not updated on payment of ${request_json.charged_amount}`);
                            }
                        }else{
                            throw new Error("This transaction has already been processed")
                        }
                    }
    
                    if(wallet){
                        let transactions = await prisma.walletTransactions({where: {transaction_code: request_json.txRef}})
                        if(transactions[0]){
                            let updatedWallet = await prisma.updateWallet({
                                where: {
                                    id: wallet.id
                                },
                                data: {
                                    amount: ( wallet.amount + request_json.charged_amount),
                                    transactions: {
                                        create: {
                                            amount: request_json.charged_amount,
                                            flow: "CREDIT",
                                            details: `FUNDED wallet by Bank Transfer by ${request_json.customer.email}`,
                                            transaction_code: request_json.txRef,
                                            status: "SUCCESSFUL"
                                        }
                                    }
                                }
                            }).$fragment(`{
                                id 
                                owner {
                                    id
                                    first_name
                                    last_name
                                    email
                                    phone
                                }
                            }`)
                            if(updatedWallet){
                                // start new agenda to inform user of wallet funding status.
                                agenda.now("sendWalletTransferFunding", {
                                    user: {
                                        name: updatedWallet.owner.first_name,
                                        amount: request_json.charged_amount,
                                        phone: updatedWallet.owner.phone,
                                        email: updatedWallet.owner.email
                                    }
                                })
                            }
                        }
                    }
    
                }
                // update contract according to transaction
    
                break;
            case webhooksType.Transfer:
                let transferObject = request_json.transfer;
                // this is a motherfucking transfer response
                // there are multiple paths for transfers.
                // Milestones transfers - reference matches a milestone transaction reference so we match it to the contract which owns it.
                // Wallet transfers - reference matches a wallet transaction so we match it to the wallet to update the transaction state.
                // Whole contracts transfers - reference matches a contract transaction without wallet.

                // if it's a wallet let roll.
                let walletTransactions = await prisma.walletTransactions({ 
                    where: {transaction_code: transferObject.reference}
                })
                let contractTransactions = await prisma.contractTransactions({ 
                    where: {transaction_code: transferObject.reference}
                })
                if(walletTransactions[0]){
                    // there exists a piece. so we can make the transactions come true.
                    // check the state of the contract and update
                    await manageTransferWebhooksForWallets(walletTransactions[0], transferObject);
                }
                if(contractTransactions.length > 0) {
                    // update the first one
                    // update the contract
                    await manageTransferWebHooksForContracts(contractTransactions[0], transferObject)
                }
                // update the transferObject from prisma to update the  transfer object.
                break;
            default:
                break;
        }
        response.sendStatus(200);
    } catch (e) {
        logger.error(e.message)
        response.send("e.message")
    }
    // Give value to your customer but don't give any output
    // Remember that this is a call from rave's servers and 
    // Your customer is not seeing the response here at all
});
app.get('/webhook', bodyParser.raw({type: 'application/json'}), (request, response) => {
    return response.json({text: "Spincrow api webhook"})
});

app.post('/webhook/monnify', bodyParser.raw({type: 'application/json'}), async (request, response) => {

    let monifyBase = new monnify();
    var request_json = JSON.parse(request.body);
    logger.info(JSON.stringify(request_json))
    let hashedSet = monifyBase.generateTransactionHash({
        paymentReference: request_json['paymentReference'],
        amountPaid: request_json['amountPaid'],
        paidOn: request_json['paidOn'],
        transactionReference: request_json['transactionReference']
    })
    if(request_json['transactionHash'] !== hashedSet) {
    // discard the request,only a post with rave signature header gets our attention 
        return response.send("NOT A VALID REQUEST")
    }
    
    try {
        switch (request_json['paymentMethod']) {
            case "ACCOUNT_TRANSFER":
                let contracts = await prisma.contracts({where: {va_order_ref: request_json.product.reference }})
                let contract = contracts[0]

                let wallets = await prisma.wallets({where: {va_order_ref: request_json.product.reference}})
                let wallet = wallets[0]
                if(contract){
                    if(contract.amount <= parseFloat(request_json.amountPaid)){

                        // check to make sure a transaction like that doesn exist.
                        let transactionsOnContract = await prisma.contract({id: contract.id}).transactions({
                            where: {
                                transaction_code: request_json.transactionReference
                            }
                        })
                        let owner = await prisma.contract({id: contract.id}).owner()
                        if(transactionsOnContract.length < 1){
                            let updatedContract = await prisma.updateContract({
                                where: {
                                    id: contract.id
                                },
                                data: {
                                    status: "ACTIVE",
                                    transactions: {
                                        create: {
                                            amount: parseFloat(request_json.amountPaid),
                                            flow: "CREDIT",
                                            details: `FUNDED CONTRACT by Bank Transfer by ${request_json.customer.email}`,
                                            transaction_code: request_json.transactionReference,
                                            status: "SUCCESSFUL"
                                        }
                                    }
                                }
                            })
                            await agenda.now('funded_contract_by_transfer', {
                                user: {
                                    name: updatedContract.client.split(" ")[0],
                                    phone: updatedContract.client_phone,
                                    email: updatedContract.client_email
                                },
                                actor: {
                                    name: owner.first_name,
                                    sendcode: true
                                },
                                contract: {
                                    name: updatedContract.name,
                                    amount: updatedContract.amount,
                                    client_code : updatedContract.client_code,
                                    id: updatedContract.id
                                }
                            })
                            await agenda.now('funded_contract_by_transfer', {
                                user: {
                                    name: owner.first_name,
                                    phone: owner.phone,
                                    email: owner.email
                                },
                                actor: {
                                    name: updatedContract.client.split(" ")[0],
                                    sendcode: false
                                },
                                contract: {
                                    name: updatedContract.name,
                                    amount: updatedContract.amount,
                                    client_code : updatedContract.client_code,
                                    id: updatedContract.id
                                }
                            })
                            await generateActivityForUser({
                                user_id: owner.id,
                                detail: `Your contract ${contract.name} has just been funded by Bank transfer`,
                                amount: parseFloat(request_json.amountPaid.toString())
                            })
                            // console.log(updatedContract)
                        }else{
                            throw new Error("The transaction has already been processed.")
                        }
                    }else{
                        logger.info(`Contract ${contract.id} was not updated on payment of ${request_json.amountPaid}`);
                    }
                }
    
                if(wallet){
                    let transactionsOnWallet = await prisma.wallet({id: wallet.id}).transactions({
                        where: {
                            transaction_code: request_json.transactionReference
                        }
                    })
                    let owner = await prisma.wallet({id: wallet.id}).owner()
                    if(transactionsOnWallet.length < 1){
                        let updatedWallet = await prisma.updateWallet({
                            where: {
                                id: wallet.id
                            },
                            data: {
                                amount: ( wallet.amount + parseFloat(request_json.amountPaid)),
                                transactions: {
                                    create: {
                                        amount: parseFloat(request_json.amountPaid),
                                        flow: "CREDIT",
                                        details: `FUNDED wallet by Bank Transfer by ${request_json.customer.email}`,
                                        transaction_code: request_json.transactionReference,
                                        status: "SUCCESSFUL"
                                    }
                                }
                            }
                        })
                        if(updatedWallet){
                            // start new agenda to inform user of wallet funding status.
                            await generateActivityForUser({
                                user_id: owner.id,
                                detail: `Your wallet has been funded by bank transfer`,
                                amount: parseFloat(request_json.amountPaid.toString())
                            })
                            agenda.now("sendWalletTransferFunding", {
                                user: {
                                    name: owner.first_name,
                                    amount: parseFloat(request_json.amountPaid),
                                    phone: owner.phone,
                                    email: owner.email
                                }
                            })
                        }
                    }else{
                        throw new Error("The transaction has already been processed.")
                    }
                }
            default:
                break;
        }
        response.sendStatus(200);
    } catch (e) {
        logger.error(e.message)
        response.send(e.message)
    }
    // Give value to your customer but don't give any output
    // Remember that this is a call from rave's servers and 
    // Your customer is not seeing the response here at all
});

app.get('/webhook/monnify', bodyParser.raw({type: 'application/json'}), (request, response) => {
    return response.json({text: "Spincrow api webhook for monnify"})
});

// app.listen(8000, () => console.log('Running on port 8000'));
export default app;
