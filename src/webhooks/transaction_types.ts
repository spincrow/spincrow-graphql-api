import keymirror from "keymirror";
export default keymirror({
    BANK_TRANSFER_TRANSACTION: null,
    CARD_TRANSACTION: null,
    BARTER_TRANSACTION: null,
    ACCOUNT_TRANSACTION: null,
    Transfer: null
});
