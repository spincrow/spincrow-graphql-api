import RavePay from "@legobox/ravepay"
import request from "request"
import logger from "./logger"
// import * as Axios from "axios"
import {ResolvedAccount} from "../resolvers/extraTypes"

interface CardData {
    cardno: String
    cvv: String
    pin: String
    expiryyear: String
    expirymonth: String
    amount: String
    email: String
    firstname: String
    lastname: String
    txRef: String
    IP: String
}

interface BankAccount {
    bankname: String
    bankcode: String
    internetbanking: Boolean
}

type chargeInput = {
    token: String,
    currency: String | null,
    country: String | null,
    amount: Number,
    email: String,
    IP: String,
    narration: String | null,
    txRef: String,
    device_fingerprint?: String|null,
    subaccounts?: Object | null
}

type transferInput = {
    account_bank: String,
    account_number: String,
    amount: Number,
    narration: String,
    destination_bank_code: String,
    beneficiary_name: String,
    currency: String | "NGN",
    reference: String
}


class RaveService {
    private ravepayObj : RavePay

    private raveBankApi: String

    // private axiosClient: Axios.AxiosInstance

    constructor(){
        this.raveBankApi = process.env.RAVE_BASE_URL
        if((process.env.RAVE_PRODUCTION == 'true')){
            this.ravepayObj = new RavePay(process.env.RAVE_PUBLIC_KEY, process.env.RAVE_SECRET_KEY, (process.env.RAVE_PRODUCTION == 'true'))
        }else{
            this.ravepayObj = new RavePay(process.env.RAVE_PUBLIC_TEST_KEY, process.env.RAVE_SECRET_TEST_KEY, (process.env.RAVE_PRODUCTION == 'true'))
        }

        console.log(this.raveBankApi, process.env.RAVE_PRODUCTION == 'true'? process.env.RAVE_SECRET_KEY: process.env.RAVE_SECRET_TEST_KEY)

        // this.axiosClient = Axios.default.create({
        //     baseURL: <string> this.raveBankApi,
        //     headers: {
        //         Authorization: `Bearer ${process.env.RAVE_PRODUCTION == 'true'? process.env.RAVE_SECRET_KEY: process.env.RAVE_SECRET_TEST_KEY}`
        //     }
        // })
    }

    async verifyTransactionReference(refumber: any){
        const payload = {
            txref: refumber
        }

        try{
            logger.info(JSON.stringify(payload))
            const response = await this.ravepayObj.VerifyTransaction.verify(payload)
            return response
        }catch(err){
            logger.error(err.message)
            return err
        }
        // await rave.VerifyTransaction.verify(payload, rave)
    }

    getBankAccounts () {
        return new Promise(async (resolve, reject) => {
            try{
                let options = {
                    'method': 'GET',
                    'url': `${this.raveBankApi}/banks/NG`,
                    'headers': {
                        'Authorization': `Bearer ${process.env.RAVE_SECRET_KEY}`
                    }
                };
        
                // let resp = await request.get(options.url, options)
                request(options, function (error, response) { 
                    if (error) throw new Error(error);

                    if(response.statusCode == 503){
                        throw new Error("Can't retrieve bank data at the moment")
                    }

                    logger.info(JSON.stringify(response));
    
                    let data = JSON.parse(response.body).data.map( (item:any) => ({
                        bankname: item.name,
                        bankcode: item.code,
                        internetbanking: true
                    }));
                    resolve(data)
                  });
            }catch(e){
                logger.error(e.message)
                reject(e)
            }
        })
    }

    getBankDetails (input: {account_no: string, account_bank: string}): Promise<typeof ResolvedAccount> {
        return new Promise(async (resolve, reject) => {
            try{
                let options = {
                    'method': 'POST',
                    'form': {
                        account_number: input.account_no,
                        account_bank: input.account_bank
                    },
                    'url': `${this.raveBankApi}/accounts/resolve`,
                    'headers': {
                        'Authorization': `Bearer ${process.env.RAVE_SECRET_KEY}`
                    }
                };
        
                // let resp = await request.get(options.url, options)
                request(options, function (error, response) { 
                    if (error) throw new Error(error);
    
                    let data = JSON.parse(response.body).data;
                    resolve(data)
                  });
            }catch(e){
                logger.error(e.message)
                reject(e)
            }
        })
    }

    async getCardToken(txRef: String){
        const payload = {
            txref: txRef
        }
        try{
            logger.info(JSON.stringify(payload))
            const response = await this.ravepayObj.VerifyTransaction.verify(payload)
            if(response.data.card){
                return {
                    token: response.data.card.card_tokens[0].embedtoken,
                    expirymonth: response.data.card.expirymonth,
                    expiryyear: response.data.card.expiryyear,
                    last_numbers: response.data.card.last4digits,
                    type: response.data.card.type
                }
            }else{
                throw new Error("Card data missing")
            }
        }catch(err){
            logger.error(err.message)
            throw err
        }
    }

    async chargeCard(input: chargeInput){
        try{
            logger.info(JSON.stringify(input))
            let resp = await this.ravepayObj.TokenCharge.card(input)
            return resp
        }catch(err) {
            logger.error(err.message)
            throw err
        }
    }

    async transferFunds(input: any){
        try{
            logger.info(JSON.stringify(input))
            let resp = await this.ravepayObj.Transfer.initiate(JSON.parse(input))
            return resp
        }catch(err){
            logger.error(err.message)
            throw err
        }
    }

    async createPermanentVA(input: {email: string, tx_ref: string, is_permanent: boolean, amount?: number, duration?: number}){
        try {
            logger.info(JSON.stringify(input))
            let resp = await this.ravepayObj.VirtualAccount.accountNumber(input)
            logger.info(JSON.stringify(resp))
            return resp
        } catch (e) {
            logger.error(e.message)
            throw e
        }
    }
}

export default RaveService
