import winston from 'winston'
import Sentry from "winston-sentry-log"
// setting up the logger
const logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    defaultMeta: {
      service: 'user-service'
    },
    transports: [
      new winston.transports.File({filename: './log/error.log', level: "error"}),
      new winston.transports.File({filename: './log/warning.log', level: "warn"}),
      new winston.transports.File({filename: './log/debug.log', level: "debug"}),
      new winston.transports.File({filename: './log/info.log', level: "info"}),
      new winston.transports.File({filename: './log/combined.log'}),
      new Sentry({
          config: {
              dsn: process.env.SENTRY_LOG_URL ,
              environment: process.env.NODE_ENV
          },
          level: "error"
      })
    ]
  })

export default logger;