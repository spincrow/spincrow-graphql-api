import {prisma, Contract, Wallet, ID_Input} from "../generated/prisma-client"
import {generateBaseUUID} from "../utils"
import logger from "./logger"
import MonnifyService from "./monnify-service"


export const generateAccountNoForWallet = async (input: {wallet_id: ID_Input, user_id: ID_Input, email: string}) => {
    try {
        // const txRef = generateUUID()
        let user = await prisma.user({id: input.user_id})
        let wallet = await prisma.wallet({id: input.wallet_id})
        let monnifyService = new MonnifyService();
        const accountReference = generateBaseUUID()
        console.log("reached here")
        if(wallet){
            console.log("Wallet account creation reached")
            let monnifyAccountResponse = await monnifyService.createWalletAccountNo({
                accountReference: accountReference,
                name: user.first_name,
                email: input.email,
                owner_name: `${user.first_name} ${user.last_name}`
            })
            if(monnifyAccountResponse){
                console.log("confirmed account response")
                console.log(monnifyAccountResponse)
                let updatedWallet = await prisma.updateWallet({
                    where: {
                        id: input.wallet_id
                    },
                    data: {
                        va_account_number: monnifyAccountResponse.responseBody.accountNumber,
                        va_bank_name: monnifyAccountResponse.responseBody.bankName,
                        va_created_at: new Date(monnifyAccountResponse.responseBody.createdOn),
                        va_note: "monnify wallet account no",
                        va_account_name: monnifyAccountResponse.responseBody.accountName,
                        va_order_ref: monnifyAccountResponse.responseBody.accountReference
                    }
                })
                return updatedWallet
            }else{
                throw new Error("something went wrong at monnify")
            }
            // an account no had been created for your wallet.
        }
    }catch (e) {
        logger.error(e.message)
        throw e
    }
}

export const generateActivityForUser = async (input: {user_id: ID_Input, detail: string, amount: number | null }) : Promise<Boolean> => {
    try{
        let user = await prisma.user({id: input.user_id})
        // create the activity.
        let activity = await prisma.createActivity({
            user: {
                connect: {
                    id: user.id
                }
            },
            amount: input.amount,
            details: input.detail
        })
        if(activity){
            return true
        }
    }catch(e){
        throw e
    }
}
