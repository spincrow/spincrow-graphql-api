/**
 * The monnify service
 * This class is meant to perform the following.
 * 
 * Create reserved bank accounts for the wallets of the users
 * Setup webhook jobs which would run when a transaction is being processed.
 * Provide service to call monnity to confirm funds acceptance.
 */
import Monnify from "@legobox/monnify-nodejs"
import logger from "./logger"

 export default class MonnifyService {

    public monnifySdk: Monnify;

    constructor(){
        this.monnifySdk = new Monnify();
    }

    public async createContractAccountNo(input: {email: string, accountReference: string, name:string, owner_name: string}) {
        try {
            let monnifyAccount = await this.monnifySdk.reserveAccount({
                "accountRef": input.accountReference,
                "accountName": `${input.name}@Spincrow`,
                "clientEmail": input.email,
                "clientName": input.owner_name
            })
            logger.info(JSON.stringify(monnifyAccount))
            return monnifyAccount
        } catch (e) {
            logger.error(e.message)
            throw new Error("Unable to create account for the contract")
        }
    }

    public async createWalletAccountNo(input: {email: string, accountReference: string, name:string, owner_name: string}) {
        try {
            let monnifyAccount = await this.monnifySdk.reserveAccount({
                "accountRef": input.accountReference,
                "accountName": `${input.name}@Spincrow-Wallet`,
                "clientEmail": input.email,
                "clientName": input.owner_name
            })
            logger.info(JSON.stringify(monnifyAccount))
            return monnifyAccount
        } catch (e) {
            logger.error(e.message)
            throw new Error("Unable to create account for the wallet")
        }
    }
 }
