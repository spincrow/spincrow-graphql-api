import * as Sentry from '@sentry/node';

Sentry.init({ 
    dsn: 'https://c02ab1d4637943aebe300c2b8a2ce849@o412238.ingest.sentry.io/5288680' ,
    environment: "development"
});

export default Sentry