import redis from "redis"
import Twilio from "twilio"
import logger from "./logger"
import Nexmo from "nexmo";

let client:any = null;
if(process.env.REDIS_URL){
    client = redis.createClient({
        url: process.env.REDIS_URL
    });
}else{
    client = redis.createClient(parseInt(process.env.REDIS_PORT), process.env.REDIS_HOST, {
        retry_strategy: function (options) {
            if(options.error && options.error.code == 'ECONNREFUSED'){
                //console.log('Redis connection to ' + host + ':' + port + ' failed,The server refused the connection');
                return new Error('The server refused the connection');
            }
            if (options.total_retry_time > 1000 * 60 * 60) {
                // End reconnecting after a specific timeout and flush all commands with a individual error
                return new Error('Retry time exhausted');
            }
            if (options.times_connected > 10) {
                // End reconnecting with built in error
                return undefined;
            }
            // reconnect after
            return Math.max(options.attempt * 100, 3000);
        }
    });
}

client.on('connect', function() {
    logger.info('Redis client connected');
});

client.on("error", function(err: any) {
    logger.info(err)
});
  
class SMSVerify {

    private client: Nexmo | Twilio.Twilio
    // private sendingPhoneNumber: String
    // private appHash: String
    private extras: {
        sendingPhonenumber? : string,
        appHash?: string
    }

    constructor(client: Nexmo | Twilio.Twilio, input: { sendingPhonenumber: string, appHash : string}){
        this.client = client
        this.extras = input
        // this.sendingPhoneNumber = sendingPhonenumber
        // this.appHash = appHash
    }

    generateOneTimeCode(){
        const codelength = 6;
        return Math.floor(Math.random() * (Math.pow(10, (codelength - 1)) * 9)) + Math.pow(10, (codelength - 1));
    }

    getExpiration(){
        return 900
    }

    request(phone: string){
        return new Promise(async (resolve, reject) => {
            logger.info('Requesting SMS to be sent to ' + phone);
    
            const otp = this.generateOneTimeCode();
            client.set(phone, otp.toString());
            try {
                const smsMessage = `Hello, welcome to spincrow, please use the code ${otp} as your verification code`;
                logger.info(smsMessage);
                if(this.client instanceof Nexmo){
                   this.client.message.sendSms("SPINCROW", phone, smsMessage, {}, (err, data) => {
                        if(err){
                            reject(err)
                        }

                        resolve(data)
                   });
                }
                
                if(this.client instanceof Twilio.Twilio){
                    if(!phone.startsWith("+")){
                        phone = `+${phone}`
                    }
                    const message = await this.client.messages.create({
                        to: phone,
                        from: "SPINCROW",
                        // from: this.extras.sendingPhonenumber,
                        body: smsMessage,
                    })
                    if(message){
                        logger.info(message.sid)
                        resolve(message)
                    }
                }
            } catch (e) {
                reject(e)
            }
        })
    }

    normalRequest(phone: string, messageToSend: string){
        return new Promise(async (resolve, reject) => {
            logger.info('Requesting SMS to be sent to ' + phone);
    
            const otp = this.generateOneTimeCode();
            client.set(phone, otp.toString());
            try {
                const smsMessage = messageToSend;
                logger.info(smsMessage);
                if(this.client instanceof Nexmo){
                    if(this.client instanceof Nexmo){
                        this.client.message.sendSms("SPINCROW", phone, messageToSend, {}, (err, data) => {
                             if(err){
                                 reject(err)
                             }
     
                             resolve(data)
                        });
                     }
                }
                
                if(this.client instanceof Twilio.Twilio){
                    if(!phone.startsWith("+")){
                        phone = `+${phone}`
                    }
                    const message = await this.client.messages.create({
                        to: phone,
                        // from: this.extras.sendingPhonenumber,
                        from: "SPINCROW",
                        body: smsMessage,
                    })
                    if(message){
                        logger.info(message.sid)
                        resolve(message)
                    }
                }
            } catch (e) {
                reject(e)
            }
        })
    }

    verify( phone: string, otpCode: any){
       logger.info('Verifying ' + phone + ':' + otpCode);
        return new Promise(async (resolve, reject) => {
            client.get(phone, (err, data) => {
               logger.info(data);
                if(err){
                    reject (err)
                }
                if (data == null) {
                   logger.info('No cached otp value found for phone: ' + phone);
                    resolve(false);
                }
                if (data == otpCode) {
                   logger.info('Found otp value in cache');
                    resolve(true);
                } else {
                   logger.info('Mismatch between otp value found and otp value expected');
                    resolve(false);
                }
            });
        })
    }

    reset(phone: string){
       logger.info('Resetting code for:  ' + phone);
        const otp = client.get(phone);
        if (otp == null) {
           logger.info('No cached otp value found for phone: ' + phone);
            return false;
        }
        client.srem(phone);
        return true;
    }
}

class TwilioVerification {

    private twilloClient: any
    private sendingPhoneNumber: string
    private appHash: string
    private twilioApiKey: string
    private twilioApiSecret: string
    private twilioAccountSID: string
    private SMSVerify: SMSVerify

    constructor(){
        this.sendingPhoneNumber = process.env.SENDING_PHONE_NUMBER
        this.twilioApiKey = process.env.TWILIO_API_KEY
        this.twilioApiSecret = process.env.TWILIO_API_SECRET
        this.twilioAccountSID = process.env.TWILIO_ACCOUNT_SID
        this.appHash = process.env.APP_HASH

        this.twilloClient = Twilio(
            this.twilioApiKey,
            this.twilioApiSecret,
            {
                accountSid: this.twilioAccountSID
            }
        )

        this.SMSVerify = new SMSVerify(this.twilloClient, {
            sendingPhonenumber: this.sendingPhoneNumber,
            appHash: this.appHash
        })
    }

    async smsRequest(phone: string){
        try {
            await this.SMSVerify.request(phone)
            return {status: true, time: this.SMSVerify.getExpiration()}
        }catch(e){
            throw e
        }
    }

     verify(phone: string, code: string){
        return this.SMSVerify.verify(phone, code)
    }

    async request(phone: string, message: string) {
        try {
            await this.SMSVerify.normalRequest(phone, message)
            return {status: true}
        } catch (e){
            throw e
        }
    }

    
}

class NexmoVerification {

    private nexmoClient: any
    private sendingPhoneNumber: string
    private appHash: string
    private nexmoApiKey: string
    private nexmoApiSecret: string
    // private twilioAccountSID: string
    private SMSVerify: SMSVerify

    constructor(){
        this.sendingPhoneNumber = process.env.SENDING_PHONE_NUMBER
        this.nexmoApiKey = process.env.NEXMO_API_KEY
        this.nexmoApiSecret = process.env.NEXMO_API_SECRET
        // this.twilioAccountSID = process.env.TWILIO_ACCOUNT_SID
        this.appHash = process.env.APP_HASH

        this.nexmoClient = new Nexmo({
            apiKey: this.nexmoApiKey,
            apiSecret: this.nexmoApiSecret,
          });

        this.SMSVerify = new SMSVerify(this.nexmoClient, {
            sendingPhonenumber: "",
            appHash: this.appHash
        })
    }

    async smsRequest(phone: string){
        try {
            await this.SMSVerify.request(phone)
            return {status: true, time: this.SMSVerify.getExpiration()}
        }catch(e){
            throw e
        }
    }

     verify(phone: string, code: string){
        return this.SMSVerify.verify(phone, code)
    }

    async request(phone: string, message: string) {
        try {
            await this.SMSVerify.normalRequest(phone, message)
            return {status: true}
        } catch (e){
            throw e
        }
    }

    
}

class SMSVerification extends TwilioVerification{
    constructor(){
        super()
    }
}

export {
    SMSVerification
}
