import Agenda from "agenda";
import {generateAccountNoForWallet} from "../services/extra-jobs"
import logger from "./logger";
import {formatCurrency} from "../utils"
import {SMSVerification} from "./sms-verfication"

import Aws from "aws-sdk";

Aws.config.update({region: process.env.AWS_DEFAULT_REGION})

let sqsService = new Aws.SQS({apiVersion: '2012-11-05'});

let agenda = null
try {
    agenda = new Agenda({db: {address: process.env.AGENDA_MONGO_URL}});
} catch (e) {
    console.log(e.message)
}

// Creating a signup mail
agenda.define('signup mail', async job => {
    let user = job.attrs.data.user, 
        subject = job.attrs.data.subject;
        // body = job.attrs.data.body ;
        // let body = `Hello ${user.name}, Welcome to spincrow, please verify your account. Use the code ${}`

    // console log signup.
    logger.info(process.env.QUEUE_URL);
    logger.info(process.env.AWS_ACCESS_KEY_ID);
    sqsService.sendMessage({
        MessageBody: JSON.stringify({
            type: "send_welcome_mail",
            email: user.email, 
            subject: subject,
            context: {
                name: user.name,
                code: user.verification
            }
        }),
        QueueUrl: process.env.QUEUE_URL
    }, (err, result) => {
        if(err){
            logger.info(err)
        }
    })
    // await MailService.sendWelcomeMail({
    //     email: user.email, 
    //     subject: subject,
    //     context: {
    //         name: user.name,
    //         code: user.verification
    //     }
    // })
});

agenda.define("generate wallet account no", async job => {
    let input = job.attrs.data.input
    await generateAccountNoForWallet(input);
})

// Creating the first mails
agenda.define('send contract mail', async job => {
    let user = job.attrs.data.user,
        contract = job.attrs.data.contract,
        subject = job.attrs.data.subject,
        owner = job.attrs.data.owner;
        // let body = `Hello ${user.name}, A contract has been created on your behalf, use this link to access it ${process.env.FRONTEND_URL}/client/${contract.id} and here's the code to access it. ${contract.client_code}`

    // await MailService.sendContractMail({
    //         email: user.email, 
    //         subject: subject,
    //         context: {
    //             client_name: user.name,
    //             name: owner.first_name,
    //             link: `${process.env.FRONTEND_URL}/client/${contract.id}`,
    //             code: contract.client_code
    //         }
    //     })
        sqsService.sendMessage({
            MessageBody: JSON.stringify({
                type: "send_contract_mail",
                email: user.email, 
                subject: subject,
                context: {
                    client_name: user.name,
                    name: owner.first_name,
                    link: `${process.env.FRONTEND_URL}/client/${contract.id}`,
                    code: contract.client_code
                }
            }),
            QueueUrl: process.env.QUEUE_URL
        }, (err,result) => {
    if (err) {
      logger.error(err.message);
      return
    }
    console.log(result)
  })

})
    
agenda.define('notify_client_on_contract_creation', async job => {
    try {
        let contract = job.attrs.data.contract,
            owner = job.attrs.data.owner;
            let smsClient = new SMSVerification()
            await smsClient.request(contract.client_phone, `${owner.first_name} just created a contract titled ${contract.name} for you, click the link ${process.env.FRONTEND_URL}/client/${contract.id} to access the contract, and use the code ${contract.client_code} to gain access`)
    } catch (e) {
        throw e
    }
})

agenda.define('retractionForMilestone', async job => {
    let recipient = job.attrs.data.recipient,
        actor = job.attrs.data.actor,
        contract = job.attrs.data.contract,
        subject = job.attrs.data.subject,
        owner = job.attrs.data.owner;
        // let body = `Hello ${recipient.name}, A contract has been created on your behalf, use this link to access it ${process.env.FRONTEND_URL}/client/${contract.id} and here's the code to access it. ${contract.client_code}`

        sqsService.sendMessage({
            MessageBody: JSON.stringify({
                type: "send_retraction_mail_milestone",
                email: recipient.email, 
                subject: subject,
                context: {
                    client_name: recipient.name,
                    contract_name: contract.name,
                    name: actor.name,
                    link: actor.sendcode ? `${process.env.FRONTEND_URL}/client/${contract.id}` : null,
                    code: actor.sendcode ? contract.client_code : null
                }
            }),
            QueueUrl: process.env.QUEUE_URL
        }, (err,result) => {
    if (err) {
      logger.error(err.message);
      return
    }
    console.log(result)
  })
    // await MailService.sendRetractionMailForMilestone({
    //         email: recipient.email, 
    //         subject: subject,
    //         context: {
    //             client_name: recipient.name,
    //             contract_name: contract.name,
    //             name: actor.name,
    //             link: actor.sendcode ? `${process.env.FRONTEND_URL}/client/${contract.id}` : null,
    //             code: actor.sendcode ? contract.client_code : null
    //         }
    //     })

        let smsClient = new SMSVerification()
            let messages = []
            if(actor.sendcode){
                messages.push(` Access the contract via the link ${process.env.FRONTEND_URL}/client/${contract.id}, and the code ${contract.client_code}`)
            }
            await smsClient.request(recipient.phone, `${actor.name} just requested a retraction for a milestone on the contract ${contract.name}.`.concat(...messages))
})

agenda.define('retractionForContract', async job => {
    let recipient = job.attrs.data.recipient,
        actor = job.attrs.data.actor,
        contract = job.attrs.data.contract,
        subject = job.attrs.data.subject,
        owner = job.attrs.data.owner;
        // let body = `Hello ${recipient.name}, A contract has been created on your behalf, use this link to access it ${process.env.FRONTEND_URL}/client/${contract.id} and here's the code to access it. ${contract.client_code}`

        sqsService.sendMessage({
            MessageBody: JSON.stringify({
                type: "send_retraction_mail_contracts",
                email: recipient.email, 
                subject: subject,
                context: {
                    client_name: recipient.name,
                    contract_name: contract.name,
                    name: actor.name,
                    link: actor.sendcode ? `${process.env.FRONTEND_URL}/client/${contract.id}` : null,
                    code: actor.sendcode ? contract.client_code : null
                }
            }),
            QueueUrl: process.env.QUEUE_URL
        }, (err,result) => {
    if (err) {
      logger.error(err.message);
      return
    }
    console.log(result)
  })
    // await MailService.sendRetractionMailForContract({
    //         email: recipient.email, 
    //         subject: subject,
    //         context: {
    //             client_name: recipient.name,
    //             contract_name: contract.name,
    //             name: actor.name,
    //             link: actor.sendcode ? `${process.env.FRONTEND_URL}/client/${contract.id}` : null,
    //             code: actor.sendcode ? contract.client_code : null
    //         }
    //     })

        let smsClient = new SMSVerification()
        let messages = []
        if(actor.sendcode){
            messages.push(` Access the contract via the link ${process.env.FRONTEND_URL}/client/${contract.id}, and the code ${contract.client_code}`)
        }
        await smsClient.request(recipient.phone, `${actor.name} just requested a retraction for the contract ${contract.name}.`.concat(...messages))
})

// Creating the first mails
agenda.define('forgot password mail', async job => {
    let user = job.attrs.data.user,
        subject = job.attrs.data.subject
        // let body = `Hello ${user.name}, A contract has been created on your behalf, use this link to access it ${process.env.FRONTEND_URL}/client/${contract.id} and here's the code to access it. ${contract.client_code}`

        sqsService.sendMessage({
            MessageBody: JSON.stringify({
                type: "forgot_password_mail",
                email: user.email, 
                subject: subject,
                context: {
                    name: user.name,
                    link: `${process.env.FRONTEND_URL}/resetpassword?resetcode=${user.verification}`,
                }
            }),
            QueueUrl: process.env.QUEUE_URL
        }, (err,result) => {
    if (err) {
      logger.error(err.message);
      return
    }
    console.log(result)
  })    
        // await MailService.forgotPasswordMail({
        //     email: user.email, 
        //     subject: subject,
        //     context: {
        //         name: user.name,
        //         link: `${process.env.FRONTEND_URL}/resetpassword?resetcode=${user.verification}`,
        //     }
        // })
})

// send disbursement mail
agenda.define('sendDisburseNotification', async job => {
    let user = job.attrs.data.user,
    contract = job.attrs.data.contract,
    amount = job.attrs.data.amount,
    subject = job.attrs.data.subject,
    owner = job.attrs.data.owner;
    // let body = `Hello ${user.name}, A contract has been created on your behalf, use this link to access it ${process.env.FRONTEND_URL}/client/${contract.id} and here's the code to access it. ${contract.client_code}`

    sqsService.sendMessage({
        MessageBody: JSON.stringify({
            type: "send_disburse_mail",
            email: user.email, 
            subject: subject,
            context: {
                client_name: user.name,
                amount: amount,
                currency: "",
                name: owner.first_name,
                contract_name: contract.name,
                link: `${process.env.FRONTEND_URL}/client/${contract.id}`,
                code: contract.client_code
            }
        }),
        QueueUrl: process.env.QUEUE_URL
    })
    // await MailService.sendDisburseMail({
    //     email: user.email, 
    //     subject: subject,
    //     context: {
    //         client_name: user.name,
    //         amount: amount,
    //         currency: "",
    //         name: owner.first_name,
    //         contract_name: contract.name,
    //         link: `${process.env.FRONTEND_URL}/client/${contract.id}`,
    //         code: contract.client_code
    //     }
    // })
    let smsClient = new SMSVerification()
    await smsClient.request(contract.client_phone, `${owner.first_name} just disbursed funds of NGN ${amount} for your contract ${contract.name}`)
})

agenda.define('sendWalletTransferFunding',async job => {
    let user = job.attrs.data.user;
    // let body = `Hello ${user.name}, A contract has been created on your behalf, use this link to access it ${process.env.FRONTEND_URL}/client/${contract.id} and here's the code to access it. ${contract.client_code}`
    logger.info("called agenda")
    // await MailService.sendWalletFundedMail({
    //     email: user.email, 
    //     subject: "You just funded your wallet via transfer",
    //     context: {
    //         name: user.name,
    //         amount: formatCurrency(user.amount)
    //     }
    // })
    sqsService.sendMessage({
        MessageBody: JSON.stringify({
            type: "send_wallet_funded_mail",
            email: user.email, 
            subject: "You just funded your wallet via transfer",
            context: {
                name: user.name,
                amount: formatCurrency(user.amount)
            }
        }),
        QueueUrl: process.env.QUEUE_URL
    })
    let smsClient = new SMSVerification()
    await smsClient.request(user.phone, `You just funded your wallet with the amount of ${formatCurrency(user.amount)}`)
})


agenda.define('funded_contract',async job => {
    let user = job.attrs.data.user,
        actor = job.attrs.data.actor,
        contract = job.attrs.data.contract
    // let body = `Hello ${user.name}, A contract has been created on your behalf, use this link to access it ${process.env.FRONTEND_URL}/client/${contract.id} and here's the code to access it. ${contract.client_code}`
    logger.info("called agenda")
    // await MailService.fundedContract({
    //     email: user.email, 
    //     subject: `Contract ${contract.name} has been funded`,
    //     context: {
    //         name: actor.name,
    //         amount: `NGN ${formatCurrency(contract.amount)}`,
    //         client_name: user.name,
    //         link: actor.sendcode ? `${process.env.FRONTEND_URL}/client/${contract.id}` : null,
    //         code: actor.sendcode ? contract.client_code : null
    //     }
    // })
    sqsService.sendMessage({
        MessageBody: JSON.stringify({
            type: "fund_contract_mail",
            email: user.email, 
            subject: `Contract ${contract.name} has been funded`,
            context: {
                name: actor.name,
                amount: `NGN ${formatCurrency(contract.amount)}`,
                client_name: user.name,
                link: actor.sendcode ? `${process.env.FRONTEND_URL}/client/${contract.id}` : null,
                code: actor.sendcode ? contract.client_code : null
            }
        }),
        QueueUrl: process.env.QUEUE_URL
    })
    let smsClient = new SMSVerification()
    await smsClient.request(user.phone, `${actor.name} just funded your contract ${contract.name} with ${formatCurrency(contract.amount)}`)
})

agenda.define('funded_contract_by_transfer',async job => {
    let user = job.attrs.data.user,
        actor = job.attrs.data.actor,
        contract = job.attrs.data.contract
    // let body = `Hello ${user.name}, A contract has been created on your behalf, use this link to access it ${process.env.FRONTEND_URL}/client/${contract.id} and here's the code to access it. ${contract.client_code}`
    logger.info("called agenda")
    // await MailService.fundedContractByTransfer({
    //     email: user.email, 
    //     subject: `Contract ${contract.name} has been funded by bank transfer`,
    //     context: {
    //         name: actor.name,
    //         amount: `NGN ${formatCurrency(contract.amount)}`,
    //         client_name: user.name,
    //         link: actor.sendcode ? `${process.env.FRONTEND_URL}/client/${contract.id}` : null,
    //         code: actor.sendcode ? contract.client_code : null
    //     }
    // })
    sqsService.sendMessage({
        MessageBody: JSON.stringify({
            type: "funded_contract_by_transfer",
            email: user.email, 
            subject: `Contract ${contract.name} has been funded by bank transfer`,
            context: {
                name: actor.name,
                amount: `NGN ${formatCurrency(contract.amount)}`,
                client_name: user.name,
                link: actor.sendcode ? `${process.env.FRONTEND_URL}/client/${contract.id}` : null,
                code: actor.sendcode ? contract.client_code : null
            }
        }),
        QueueUrl: process.env.QUEUE_URL
    })
    let smsClient = new SMSVerification()
    await smsClient.request(user.phone, `Your contract ${contract.name} has just been funded by bank transfer - ${formatCurrency(contract.amount)}`)
});
//agenda.on('ready', () => {
    //(async () => {
    //    try {
    //    agenda._collection.ensureIndex({
    //        disabled: 1,
    //        lockedAt: 1,
    //        name: 1,
    //        nextRunAt: 1,
    //        priority: -1,
    //    }, {
    //        name: 'findAndLockNextJobIndex',
    //    });
    //    logger.info('Agenda successfully indexed');
    //    } catch (err) {
    //    logger.error('Failed to create agendajs index!', err);
    //    throw err;
    //    }
    // })();
    // agenda.start();
    // agenda.every('0 1 * * *', 'update_maturity_date', {}); // 0 0 * * *

export default agenda;
