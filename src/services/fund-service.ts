// @ts-ignore
import RaveService from './rave-service';
import { Context } from 'types';
import {generateUUID} from "../utils"
import ip from "ip"
// Process funds and funding operations.

export const processWalletFunding = async (ctx: Context, wallet_id: string, amount: number) => {
    try {
        let wallet = await ctx.prisma.wallet({id: wallet_id});
        if(wallet.amount >= amount){
            // process the funding.
            let updatedWallet = await ctx.prisma.updateWallet({
                where: {
                    id: wallet_id
                },
                data: {
                    amount: wallet.amount - amount
                }
            })
            return updatedWallet
        }else{
            throw new Error("Wallet balance is too low, try topping it up")
        }
    }catch(err){
        throw err
    }   
}

export const processCardFunding = async (ctx: Context, card_id: string, amount: number) => {
    let raveService = new RaveService()
    const txRef = generateUUID()
    try {
        // get card
        let card = await ctx.prisma.card({id: card_id}).$fragment(`{
            id
            token
            owner {
                email
                first_name
                last_name
            }
        }`);
        if(card){
            let response = await raveService.chargeCard({
                amount: amount, 
                token: card.token,
                email: card.owner.email,
                IP: ip.address(),
                txRef: txRef,
                currency: "NGN",
                country: "NG",
                narration: "Charging a card",
            })
            return response
        }
    }catch(err){
        throw err
    }
}

export const withdrawFunds = async (
        ctx: Context, 
        input: {
            bank_code: string,
            account_number: string,
            amount: string,
            account_name: string,
        }
) => {
    const raveService = new RaveService();
    const txRef = generateUUID()
    // set up transfer data
    const transferData = {
        "account_bank": input.bank_code,
        "account_number": input.account_number,
        "amount": parseInt(input.amount),
        "currency":"NGN",
        "reference":txRef,
        "beneficiary_name": input.account_name,
        "destination_branch_code": input.bank_code,
    }
    try {
        const response = await raveService.transferFunds(JSON.stringify(transferData))
        if(response.status == "error"){
            throw new Error(response.message)
        }
        if(response.status == "success"){
            return {status: "success", txRef: transferData.reference}
        }
    }catch(e){
        throw e
    }
}