/// <reference types="node" />

declare module 'nodemailer-express-handlebars' {
    declare module "nodemailer-express-handlebars" {

        function nodemailerExpressHandlebars(data: any): any;
    
        module nodemailerExpressHandlebars {}
        export = nodemailerExpressHandlebars;
    }
}