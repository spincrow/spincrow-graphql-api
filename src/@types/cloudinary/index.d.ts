/// <reference types="node" />

declare module 'cloudinary' {
    export const v2 : {
        config: Function,
        uploader: {
            upload_stream: UploadStreamFunc,
            upload: UploadFunc
        }
    }
    export type UploadFunc = (name: any, options?: Object, callback: Callback) => void
    export type UploadStreamFunc = ( options?: Object, callback: Callback) => any
    export type Callback = (error: any, result: any) => void
}