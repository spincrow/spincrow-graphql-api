import * as Yup from "yup"
import moment from "moment"
import buildErrorObject from "./buildErrorObjectFromValidationError"
import {createError} from "apollo-errors"
import {createContractValidation, signupValidation, loginValidation} from "./validationSchemas"

const AuthenticationError = createError("AuthenticationError", {
    message: "Invalid credentials"
})


export const FatalError = createError("FatalError", {
    message: "Something went wrong, please re-try later"
})

const helmet = (resolver: any) => async (...args: any) => {
  try {
    //
    // Try to execute the actual resolver and return
    // the result immediately.
    //
    return await resolver(...args);
  } catch (err) {
    //
    // Due to the fact that we are using Prisma, we can assume
    // that each error from this layer has a `path` attribute.
    //
    // Note: The `FatalError` has been created before by
    // using `apollo-errors` `createError` function.
    //
    if (err.path) {
      throw new FatalError({ data: { reason: err.message } });
    } else {
      throw err;
    }
  }
};


// create  a collection of mutation input validations.
const mutationValidationMap = {
    createContract: createContractValidation,
    signup: signupValidation,
    login: loginValidation
}

const customYupValidation = {
    async Mutation(resolve, root, args, context, info) {
        // console.log("reach here")
        
        // throw new Error("none");
        const mutationValidationSchema = mutationValidationMap[info.fieldName];
        
        if(mutationValidationSchema){
            try {
                // console.log(args)
                await mutationValidationSchema.validate(args)
            }catch(e){
                if(e instanceof Yup.ValidationError){
                    throw e
                }else{
                    throw e
                }
            }
        }
        
        return resolve(root, args, context, info)
    }
}

export { helmet, AuthenticationError };
export default customYupValidation