import * as Yup from "yup"
import moment from "moment"


const phoneRegx = RegExp(/^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/);

const createContractValidation = Yup.object().shape({
    input:  Yup.object().shape({
        name: Yup.string().required("Your contract name is required"),
        description: Yup.string().required("please describe your contract"),
        duration_start: Yup.date().default(moment()).required("please enter your contract starting date"),
        duration_end: Yup.date().default(moment().add(1, 'days')).required("please enter your contract ending date"),
        owner_type: Yup.string().required("What kinda owner? (Buyer or Seller)"),
        client: Yup.string().required("Please enter a client name"),
        client_email: Yup.string().email("Please enter a valid client email").required("Please enter your client's mail"),
        client_phone: Yup.string().matches(phoneRegx, "Invalid phone no").required("Please enter your client's phone no").min(10, "Your phone number needs a minimum of 10 numbers")
    })
})

const signupValidation = Yup.object().shape({
    input:  Yup.object().shape({
        first_name: Yup.string().required("Enter your first name"),
        last_name: Yup.string().required("Enter your last name"),
        email: Yup.string().email("This is not a valid email").required("Enter your email"),
        password: Yup.string().required("Enter your password")
        // phone: Yup.string().required("What kinda owner? (Buyer or Seller)"),
    })
})

const loginValidation = Yup.object().shape({
    input:  Yup.object().shape({
        email: Yup.string().email("This is not a valid email").required("Enter your email"),
        password: Yup.string().required("Enter your password")
        // phone: Yup.string().required("What kinda owner? (Buyer or Seller)"),
    })
})


export {createContractValidation, signupValidation, loginValidation}