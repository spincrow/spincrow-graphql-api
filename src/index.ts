require('dotenv').config()
import { ApolloServer, gql } from 'apollo-server-express'
import { makePrismaSchema } from 'nexus-prisma'
import http from "http"
import { applyMiddleware } from "graphql-middleware";
import * as path from 'path'
import datamodelInfo from './generated/nexus-prisma'
import { prisma } from './generated/prisma-client'
import * as Types from "./resolvers"
import {permissions} from "./permissions"
import agenda from "./services/agenda-service"
import Sentry from "./services/sentry"
import cors from "cors"
import Agendash from "agendash"
import customValidations from "./validation"
// sentry logger

import app from "./webhooks"

let corsOptions = {
  origin: "*",
  credentials: false // <-- REQUIRED backend setting
}

Sentry.init({ 
  dsn: process.env.SENTRY_LOG_URL ,
  environment: process.env.NODE_ENV
});

const schema  = applyMiddleware(makePrismaSchema({
  // Provide all the GraphQL types we've implemented
  types: Types,

  // Configure the interface to Prisma
  prisma: {
    datamodelInfo,
    client: prisma,
  },

  // Specify where Nexus should put the generated files
  outputs: {
    schema: path.join(__dirname, './generated/schema.graphql'),
    typegen: path.join(__dirname, './generated/nexus.ts'),
  },

  // Configure nullability of input arguments: All arguments are non-nullable by default
  nonNullDefaults: {
    input: false,
    output: false,
  },

  // Configure automatic type resolution for the TS representations of the associated types
  typegenAutoConfig: {
    sources: [
      {
        source: path.join(__dirname, './types.ts'),
        alias: 'types',
      },
    ],
    contextType: 'types.Context',
  },
}), permissions, customValidations)

const server = new ApolloServer({
  schema,
  context:  (request: any) => ({...request, prisma }),
  introspection: process.env.NODE_ENV == "production"? false : true,
  playground: process.env.NODE_ENV == "production"? false : true,
})

// server.listen({ port: process.env.PORT || 5000 }, async () => {
//   await agenda.start();
//   console.log(`🚀 Server ready, let roll 😎 on port ${process.env.PORT || 5000}`)
// })
app.use(cors(corsOptions));
app.options("*", cors(corsOptions));
app.use('/jobs', Agendash(agenda))
server.applyMiddleware({ app, cors: false});

const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);

 
httpServer.listen({ port: process.env.PORT || 5000 }, async () =>{
  await agenda.start();
  console.log(`🚀 Server ready at http://localhost:${process.env.PORT || 5000}${server.graphqlPath}`)
  console.log(`🚀 Web sockets at ws://localhost:${process.env.PORT || 5000}`)
  console.log(`🚀 Webhook running on http://localhost:${process.env.PORT || 5000}/webhook`)
  console.log(`🚀 Access jobs running on http://localhost:${process.env.PORT || 5000}/jobs`)
});