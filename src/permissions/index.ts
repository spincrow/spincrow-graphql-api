import {rule, shield, and, or} from 'graphql-shield'
import { getUserId } from "../utils"

const rules = {
    isAuthenticatedUser: rule()((parent, args, context) => {        
        const userId = getUserId(context)
        return Boolean(userId)
    }),

    isAuthenticatedUserOrClient: rule()((parent, args, context) => {
        const userId = getUserId(context)
        const clientToken = context.req.get('Client_token')
        const contract = context.prisma.contract({id: args.input.contract_id, client_code: clientToken});
        // 
        return (userId || contract) ? true : false;
    }),

    isContractOwner: rule()(async (parent, { id }, context) => {
        const userID = getUserId(context)
        const owner = await context.prisma.contract({ id }).owner()
        return (userID === owner.id) ? true : new Error("You do not own this contract") ;
    }),
}

export const permissions = shield({
    Query: {
        me: rules.isAuthenticatedUser,
        // contract: or(rules.isAuthenticatedUser, rules.isContractOwner),
        contracts: rules.isAuthenticatedUser,
        wallet: rules.isAuthenticatedUser,
        // bankaccounts: rules.isAuthenticatedUser,
        stats: rules.isAuthenticatedUser,
        cards: rules.isAuthenticatedUser,
        // bankcodes: rules.isAuthenticatedUser,
    },
    Mutation: {
        createContract: rules.isAuthenticatedUser,
        // createTransaction: rules.isAuthenticatedUser,
        // attemptTransaction: rules.isAuthenticatedUser,
        // updateContractMilestone: rules.isAuthenticatedUserOrClient,
        // resolveBankAccount: rules.isAuthenticatedUser,
        // disburseContractMilestoneForClient: rules.isAuthenticatedUserOrClient,
        // disburseContractWholePayment: rules.isAuthenticatedUserOrClient,
        fundContractAsUser: rules.isAuthenticatedUserOrClient,
        // fundContractAsClient: rules.isAuthenticatedUserOrClient
    },
})