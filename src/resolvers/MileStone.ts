import { prismaObjectType } from "nexus-prisma"

// @ts-ignore
const MileStone = prismaObjectType({
    name: "MileStone",
    definition: (t) => {
        t.prismaFields([
            'id',
            'target',
            'paid',
            'available',
            'status',
            'disburseStatus',
            'state',
            'progress',
            'withdrawalStatus',
            'retraction_status'
        ])
    }
})

export default MileStone;