import { prismaObjectType } from "nexus-prisma"

//@ts-ignore
const Contract = prismaObjectType({
    name: "Contract",
    definition: (t) => {
        t.prismaFields([
            'id',
            'name',
            'amount',
            'description',
            'created_at',
            'owner',
            'status',
            'client_code',
            'duration_start',
            'duration_end',
            'owner_type',
            'payment_type',
            'spread_type',
            'disburseStatus',
            'periodic_amount',
            'whole_amount',
            'periodic_type',
            'transactions',
            'client',
            'client_email',
            'client_phone',
            'transactions',
            'milestones',
            'withdrawalStatus',
            'client_account_name',
            'client_account_no',
            'client_bank_code',
            'automaticWithdrawal',
            'paid',

            'va_flw_ref',
            'va_order_ref',
            'va_expirydate',
            'va_frequency',
            'va_account_number',
            'va_bank_name',
            'va_created_at',
            'va_account_name',
            'va_note',

            'retraction_status'
        ])
    }
})


// enum RetractionStatusEnum {
//     REQUESTED  // requested a retraction
//     PENDING    // retraction is pending
//     SUCCESSFUL // retraction has been approved
//     INACTIVE // no retraction has been requested
//     REJECTED // owner rejected the requested retraction
//   }
  

export default Contract;
