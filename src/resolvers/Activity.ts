import { prismaObjectType } from "nexus-prisma"

const Activity = prismaObjectType({
    name: "Activity",
    definition: (t) => {
        t.prismaFields([
            'details',
            'amount',
            'created_at'
        ])
    }
})

export default Activity
