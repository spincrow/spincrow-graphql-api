/**
 * 
 * @param args 
 */
export function checkMilestones(args: { input: { amount: number; client: string; client_email: string; client_phone: string; description?: string; duration_end: string; duration_start: string; milestones?: { status?: "FAILED" | "PENDING" | "SUCCESSFUL"; target?: number; }[]; name: string; owner_type: string; payment_type: string; periodic_amount: number; periodic_type: string; spread_type: string; whole_amount: number; }; }) {
    if (args.input.milestones.length > 0) {
        let contractAmount = 0;
        for (let i in args.input.milestones) {
            if(args.input.milestones[i].target == 0){
                throw new Error("Please set an amount on your milestone, the value can't be 0 ")
            }
            contractAmount = contractAmount + args.input.milestones[i].target;
        }
        args.input.amount = contractAmount;
    }
    return args
}

export function checkWholePayments(args: { input: { 
        amount: number; 
        client: string; 
        client_email: string; 
        client_phone: string; 
        description?: string; 
        duration_end: string; 
        duration_start: string; 
        milestones?: { status?: "FAILED" | "PENDING" | "SUCCESSFUL"; target?: number; }[]; 
        name: string; 
        owner_type: string; 
        payment_type: string; 
        periodic_amount: number; 
        periodic_type: string; 
        spread_type: string; 
        whole_amount: number; 
    }; }) {
    if (args.input.whole_amount) {
        args.input.amount = args.input.whole_amount;
    }
    args.input.milestones = []
    return args
}