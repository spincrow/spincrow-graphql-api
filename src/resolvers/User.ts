import { prismaObjectType } from "nexus-prisma"

// @ts-ignore
const User = prismaObjectType({
    name: "User",
    definition: (t) => {
        t.prismaFields([
            'id',
            'first_name',
            'last_name',
            'phone',
            'profile_image',
            'email',
            'verification_code',
            'wallet',
            'contracts',
            'activities',
            'home_tour',
            'contract_tour',
            "single_contract_tour",
            "settings_tour",
            "wallet_tour",
            "password_verification_code",
            "email_verified"
        ])
    }
})

export default User;