import {subscriptionField} from 'nexus'
// import Wallet from 'resolvers/Wallet';
import {WalletUpdatedPayload} from "../extraTypes"


const walletUpdatedSubscription = subscriptionField("walletUpdated", {
    type: WalletUpdatedPayload,
    subscribe: async  (root, args, context) => {
      const response = await context.prisma.$subscribe.wallet({ mutation_in: "UPDATED" })
      console.log("response", response)
      return response
    },
    resolve: payload => {
      return payload
    }
});

// export const walletUpdatedSubscription = subscriptionField('walletUpdated', {

// })\

export default {
  walletUpdatedSubscription
}