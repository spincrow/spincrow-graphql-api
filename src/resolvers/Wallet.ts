import { prismaObjectType } from "nexus-prisma"

// @ts-ignore
const Wallet = prismaObjectType({
    name: "Wallet",
    definition: (t) => {
        t.prismaFields([
            'id',
            'owner',
            'amount',
            'transactions',
            'created_at',
            'updated_at',
            'va_flw_ref',
            'va_frequency',
            'va_expirydate',
            'va_order_ref',
            'va_account_number',
            'va_tx_ref',
            'va_bank_name',
            'va_account_name',
            'va_created_at',
            'va_note'
        ])
    }
})

export default Wallet;