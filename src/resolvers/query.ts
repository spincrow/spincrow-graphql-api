import { idArg, queryType, inputObjectType} from 'nexus'
import { getUserId } from '../utils';
import RaveService from "../services/rave-service"
import logger from "../services/logger"
import {intArg, stringArg} from "@prisma/nexus"

const Query = queryType({
    definition(t) {
      t.field("me", {
        type: "User",
        resolve: (parent, args, context) => {
          const userId = getUserId(context);
          return context.prisma.user({id: userId});
        }
      })

      t.field("wallet", {
        type: "Wallet",
        resolve: async (parents, args, ctx) => {
          const userID = getUserId(ctx)
          if(userID){
            let wallet = await ctx.prisma.user({id: userID}).wallet();
            return wallet
          }else{
            throw new Error("Unauthenticated user");
          }
        }
      })

      t.list.field("cards", {
        type: "Card",
        resolve: (parent, args, context) => {
          const userId = getUserId(context);
          let cards = context.prisma.cards({
              where:{
                owner: {
                  id: userId
                }
              }
            });
            return cards
          }
      })


      // get all activities currently running
      t.list.field('activities', {
        type: "Activity",
        resolve: async (parent, args, ctx) => {
          let userId = getUserId(ctx);
          let response = await ctx.prisma.activities({
            where: {
              user: {
                id: userId
              }
            },
            orderBy: "id_DESC"
          })

          return response
        }
      })
      
      // Get all contracts that belong to a certain user.
      t.list.field('contracts', {
        type: 'Contract',
        resolve: (parent, args, ctx) => {
          let userId = getUserId(ctx);
          return ctx.prisma.contracts({
            where: { owner: {id: userId}},
          })
        },
      });

      t.field('contract', {
        type: 'Contract',
        nullable: true,
        args: { id: idArg() },
        resolve: (parent, { id }, ctx) => {
          return ctx.prisma.contract({ id })
        },
      })

      t.list.field('bankcodes', {
        type: 'BankCode',
        resolve: async (parents, args, ctx) => {
          // get the current list of bank accounts.
            try {
                let raveService = new RaveService();
                let response = await raveService.getBankAccounts();
                return response
            } catch (e) {
                logger.error(e.message)
                throw e
            }
        }
      })

      t.list.field('bankaccounts', {
        type: 'BankAccount',
        resolve: async (parents, args, ctx) => {
          try {
            const userId = getUserId(ctx);
            let bankaccounts = ctx.prisma.bankAccounts({
                where:{
                  owner: {
                    id: userId
                  }
                }
              });
              return bankaccounts
          } catch (err){
            logger.error(err.message)
            throw err
          }
        }
      })

      t.field("stats", {
        type: "Stats",
        resolve: async (parent, args, context) => {
            const userId = getUserId(context);
            if(userId){
              let wallet = await context.prisma.user({id: userId}).wallet()
              let contracts = await context.prisma.contracts({ where: {owner: {id: userId}, status_not: "INACTIVE"}})
              let completed_contracts = await context.prisma.contracts({ where: {owner: {id: userId}, status: "COMPLETED"}})
  
              return {
                wallet_balance: wallet.amount,
                active_contracts: contracts.length,
                completed_contracts: completed_contracts.length
              }
            }
            throw new Error("UNAUTHENTICATED USER")
        }
      })

    },
  });

  export default Query;
