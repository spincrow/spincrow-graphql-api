// @ts-nocheck
import {  mutationField } from 'nexus'
import { getUserId} from "../../utils"
import {BankResolveInput} from "../input"
import RaveService from "../../services/rave-service"
import logger from "../../services/logger"

/**
 * Creating a contract
 */
export const resolveBankAccount = mutationField("resolveBankAccount",{
    type: "ResolvedAccount",
    args: {
        input: BankResolveInput.asArg({required: true})
    },
    resolve: async (parents, {input}, ctx) => {
        try {
            const raveService = new RaveService()
            let response = await raveService.getBankDetails({
                account_bank: input.account_bank,
                account_no: input.account_no
            })
            if(response){
                return response
            }
        } catch (e) {
            throw e
        }
    }
});
