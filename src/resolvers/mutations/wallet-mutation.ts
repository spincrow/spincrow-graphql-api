// @ts-nocheck
import {  mutationField } from 'nexus'
import { getUserId} from "../../utils"
import {TopUpInput, WithdrawInput, TopUpWithCardInput} from "../input"
import RaveService from "../../services/rave-service"
import logger from "../../services/logger"
import ip from "ip"
import {generateUUID} from "../../utils"
import logger from '../../services/logger'



/**
 * Top up a wallet
 */
export const topUpWallet = mutationField("topupWallet",{
    type: "Wallet",
    args: {
        input: TopUpInput.asArg({required: true})
    },
    resolve: async (parents, {input}, ctx) => {
        let userId = getUserId(ctx),
        // get the wallet, get the card.
        const raveService = new RaveService();
        try {
            if((input.amount <= 0) || input.amount == null){
                throw new Error("Enter an amount ")
            }
            const walletUser = await ctx.prisma.user({id: userId}).$fragment(`{
                id 
                email
                wallet {
                    id
                    amount
                }
            }`)

            switch (input.card_type) {
                case "NEW_CARD":
                    let resp = await raveService.getCardToken(input.tx_ref)
                    if(resp){
                        await ctx.prisma.createCard({
                            expirymonth: resp.expirymonth,
                            expiryyear: resp.expiryyear,
                            owner: {
                                connect: {
                                    id: userId,
                                }
                            },
                            token: resp.token,
                            last_number: resp.last_numbers,
                            type: resp.type
                        });
                        const wallet = await ctx.prisma.updateWallet({
                            data: {
                                amount: ( parseFloat(walletUser.wallet.amount) + parseFloat(input.amount)),
                                transactions: {
                                    create: {
                                        amount: parseFloat(input.amount),
                                        flow: "CREDIT",
                                        transaction_code: input.tx_ref,
                                        details: `Wallet top-up from new card`,
                                        status: "SUCCESSFUL"
                                    }
                                }
                            },
                            where: {
                                id: walletUser.wallet.id
                            }
                        })
                        return wallet
                    }
                    break;
                case "CARD":
                    const txRef = generateUUID()
                    const card = await ctx.prisma.card({id: input.card_id})
                    // charge the card with the token to top up the wallet
                    if(card){
                        const response = await raveService.chargeCard({
                            token: card.token,
                            amount: input.amount,
                            email: walletUser.email,
                            IP: ip.address(),
                            txRef: txRef
                        })
                        if(response.status == "success"){
                            // add the value to the wallet
                            const wallet = await ctx.prisma.updateWallet({
                                data: {
                                    amount: ( parseFloat(walletUser.wallet.amount) + parseFloat(input.amount)),
                                    transactions: {
                                        create: {
                                            amount: parseFloat(input.amount),
                                            flow: "CREDIT",
                                            transaction_code: txRef,
                                            details: `Wallet top-up from card`,
                                            status: "SUCCESSFUL"
                                        }
                                    }
                                },
                                where: {
            
                                    id: walletUser.wallet.id
                                }
                            })
                            return wallet
                        }else{
                            throw new Error(`Wallet wasn't funded, ${response.message}`)
                        }
                    }else{
                        throw new Error("card doesn't exist")
                    }
                default:
                    throw new Error("You have to select a card type in your request")
            }
        } catch (e) {
            logger.error(e.message)
            throw e
        }
    }
});
/**
 * Withdraw funds from a wallet.
 */
export const withdrawFunds = mutationField("withdrawFunds",{
    type: "Wallet",
    args: {
        input: WithdrawInput.asArg({required: true})
    },
    resolve: async (parents, {input}, ctx) => {
        let userId = getUserId(ctx),
        // get the wallet, get the card.
        const raveService = new RaveService();
        const txRef = generateUUID()
        try {
            let user = await ctx.prisma.user({id: userId})
            const walletUser = await ctx.prisma.user({id: userId}).$fragment(`{
                id 
                email
                wallet {
                    id
                    amount
                }
            }`)
            if(walletUser.wallet.amount < input.amount){
                throw new Error("Your wallet balance is not enough");
            }

            switch (input.withdrawal_type) {
                case "NORMAL_WITHDRAWAL":
                    const bankAccount = await ctx.prisma.bankAccount({id: input.bank_account_id})
                    const transferData = {
                        "account_bank": bankAccount.bank_code,
                        "account_number": bankAccount.account_number,
                        "amount": parseInt(input.amount),
                        "currency":"NGN",
                        "reference":txRef,
                        "beneficiary_name": bankAccount.account_name,
                        "destination_branch_code": bankAccount.bank_code,
                    }
                    const response = await raveService.transferFunds(JSON.stringify(transferData))
                    if(response.status == "success"){
                        // add the value to the wallet
                        const wallet = await ctx.prisma.updateWallet({
                            data: {
                                amount:( parseFloat(walletUser.wallet.amount) - parseFloat(input.amount)) ,
                                transactions: {
                                    create: {
                                        amount: parseFloat(input.amount),
                                        flow: "DEBIT",
                                        transaction_code: txRef,
                                        details: `Withdrawal from wallet initiated`,
                                        status: "PENDING"
                                    }
                                }
                            },
                            where: {
                                id: walletUser.wallet.id
                            }
                        })
                        return wallet
                    }else{
                        throw new Error(response.message)
                    }
                case "ADD_ACCOUNT":
                    const bankAccount = await ctx.prisma.createBankAccount({
                        ...{
                            account_number: input.account_number,
                            bank_name: input.bank_name,
                            account_name: input.account_name,
                            bank_code: input.bank_code,
                        },
                        owner: {
                            connect: {
                                id: userId
                            }
                        }
                    });
                    const transferData = {
                        "account_bank": bankAccount.bank_code,
                        "account_number": bankAccount.account_number,
                        "amount": parseInt(input.amount),
                        "currency":"NGN",
                        "reference":txRef,
                        "beneficiary_name": bankAccount.account_name,
                        "destination_branch_code": bankAccount.bank_code,
                    }
                    const response = await raveService.transferFunds(JSON.stringify(transferData))
                    if(response.status == "success"){
                        // add the value to the wallet
                        const wallet = await ctx.prisma.updateWallet({
                            data: {
                                amount:( parseFloat(walletUser.wallet.amount) - parseFloat(input.amount)) ,
                                transactions: {
                                    create: {
                                        amount: parseFloat(input.amount),
                                        flow: "DEBIT",
                                        transaction_code: txRef,
                                        details: `Withdrawal from wallet initiated`,
                                        status: "PENDING"
                                    }
                                }
                            },
                            where: {
                                id: walletUser.wallet.id
                            }
                        })
                        return wallet
                    }else{
                        throw new Error(response.message)
                    }
                default:
                    throw new Error("You selected no withdrawal type, please select one")
            }
        } catch (e) {
            logger.error(e.message)
            throw e
        }
    }
});
