import {mutationField } from 'nexus'
import { APP_SECRET, getUserId} from '../../utils'
import {generateNormalUUID} from "../../utils"
import { hash} from 'bcrypt'
import { sign } from 'jsonwebtoken'
import {SignupInput, ForgotPasswordEmailInput, LoginInput, PasswordUpdate, ConfirmCodeInput, SMSVerificationInput, ForgotPasswordUpdateInput} from "../input"
import agenda from "../../services/agenda-service"
import { compare} from 'bcrypt'
import {SMSVerification} from "../../services/sms-verfication"
import logger from "../../services/logger"
import { AuthenticationError } from '../../validation'


export const signup = mutationField("signup",{
    type: "AuthPayload",
    args:  {
        input: SignupInput.asArg({required: true})
    },
    resolve: async (parent, { input } , ctx) => {
        const hashedPassword = await hash(input.password, 10)
        let verificationCode = Math.floor(100000 + Math.random() * 900000).toString();
        // get any user who has the current email.
        let currentUser = (await ctx.prisma.users({where: {email: input.email}}))[0]
        if(currentUser){
            throw new Error("There already exists a user with that email")
        }
        const user =  await ctx.prisma.createUser({
            first_name: input.first_name,
            last_name: input.last_name,
            email: input.email,
            password: hashedPassword,
            wallet: {
               create: {
                   amount: 0
               }
            },
            verification_code: verificationCode
        });
        let userWallet = await ctx.prisma.user({id: user.id}).wallet()
        agenda.now('generate wallet account no', {
            input: {
                wallet_id: userWallet.id,
                user_id: user.id,
                email: input.email
            }
        })
        agenda.now('signup mail', {
            user: {
                email: user.email,
                name: user.first_name,
                verification: verificationCode
            }, 
            subject: "Spincrow - please verify your mail"
        })
        // send mail to user concerning code.
        return {
            token: sign({userId: user.id}, APP_SECRET),
            user
        }
    },
});

export const login = mutationField("login",{
    type: "AuthPayload",
    args: {
        input: LoginInput.asArg({required: true})
    },
    resolve: async (parent, {input}, ctx) => {
        let user = await ctx.prisma.user({email: input.email})
        if (!user) {
        throw new Error(`No user found for email: ${input.email}`)
        }
        const passwordValid = await compare(input.password, user.password)
        if (!passwordValid) {
            throw new AuthenticationError()
        }
        return {
            token : sign({userId: user.id}, APP_SECRET),
            user
        }
    }
});

export const confirmCode = mutationField("confirmEmailCode", {
    type: "User",
    args: {
        input: ConfirmCodeInput.asArg({required: true})
    },
    resolve: async (parent, {input}, ctx) => {
        try {
            let userId = getUserId(ctx)
            let user = await ctx.prisma.user({id: userId})
            // confirm the value of the input.
            if(user.verification_code == input.code){
                // update user.
                let updatedUser = await ctx.prisma.updateUser({
                    data: {
                        email_verified: true
                    },
                    where: {
                        id: user.id
                    }
                })
                if(!updatedUser){
                    throw new Error("verified code couldn't be updated");
                }
                return updatedUser
            }else{
                throw new Error("Verification code couldn't be confirmed, try again")
            }
        } catch (e) {
            logger.error(e.message)
            throw e
        }
    }
})

export const sendPhoneConfirmation = mutationField("sendPhoneConfirmation", {
    type: "ConfirmResponse",
    args: {
        input: "String"
    },
    resolve: async (parents, {input}, ctx) => {
        try {
            let userId = getUserId(ctx)
            // create code and send it for phone number
            // save the user's phone number
            const user = await ctx.prisma.updateUser({
                data: {
                    phone: input,
                },
                where: {
                    id: userId
                }
            })
            if(user){
                const smsVerification = new SMSVerification()
                let response = await smsVerification.smsRequest(input.toString())
                if(response.status == true){
                    return {
                        state: true
                    }
                }
            }
        }catch(err){
            logger.error(err.message)
            throw err
        }
    }
})

export const verifySMSCode = mutationField("verifySMSCode", {
    type: "ConfirmResponse",
    args: {
        input: SMSVerificationInput.asArg({required: true})
    },
    resolve: async (parents, {input}, ctx) => {
        try {
            let userId = getUserId(ctx)
            const user = await ctx.prisma.user({id: userId});
            if(user){
                // create code and send it for phone number
                const smsVerification = new SMSVerification()
                let response = await smsVerification.verify(user.phone, input.code )
                if(response == true){
                    return {
                        state: true
                    }
                }else{
                    throw new Error("Mobile code is not correct, please verify")
                }
            }
        }catch(err){
            logger.error(err.message)
            throw err
        }
    }
})

export const passwordUpdate = mutationField("passwordUpdate", {
    type: "User",
    args: {
        input: PasswordUpdate.asArg({required: true})
    },
    resolve: async (parent, {input}, ctx) => {
        const USER_ID = getUserId(ctx);
        if(!USER_ID){
            // update the user's password.
            throw new Error('User not Authenticated')
        }
        let user = await ctx.prisma.user({id: USER_ID})
        // let proceed to update the passowrd.
        const passwordValid = await compare(input.currentPassword, user.password)
        if (!passwordValid) {
            throw new Error('Invalid password')
        }
        const hashedPassword = await hash(input.newPassword, 10)
        user = await ctx.prisma.updateUser({
            data: {
                password: hashedPassword,
            },
            where: {
                id: USER_ID
            }
        })
        return user
    }
})

export const forgotPasswordEnterEmail = mutationField("forgotPasswordEnterEmail", {
    type: "Boolean",
    args: {
        input: ForgotPasswordEmailInput.asArg({required: true})
    },
    resolve: async (parent, {input}, ctx) => {
        // end the email to the backend.
        // get user with that password.
        try {
            let verificationCode = generateNormalUUID()
            let users = await ctx.prisma.users({where: {
                email: input.email
            }})
            if(users[0]){
                // update the user with a code for the veirid
                let user = await ctx.prisma.updateUser({
                    where: {
                        id: users[0].id
                    },
                    data: {
                        password_verification_code: verificationCode
                    }
                })
                if(user){
                    agenda.now('forgot password mail', {
                        user: {
                            email: user.email,
                            name: user.first_name,
                            verification: verificationCode
                        }, 
                        subject: "Spincrow - Forgot Password?, Reset it"
                    })
                    return true
                }else{
                    throw new Error("Verification code could not be created")
                }
            }else{
                throw new Error("no user with that email exists")
            }
        } catch (e) {
            logger.error(e.message)
            throw e
        }
    }
})

export const updateForgottenPassword = mutationField("updateForgottenPassword", {
    type: "Boolean",
    args: {
        input: ForgotPasswordUpdateInput.asArg({required: true})
    },
    resolve: async (parent, {input}, ctx) => {
        try {
            // update the password with the new password.
            // get user with the verification code.
            let users = await ctx.prisma.users({where: {
                password_verification_code: input.verification_code
            }})
            const hashedPassword = await hash(input.newpassword, 10)
            if(users[0]){
                // update the user.
               await ctx.prisma.updateUser({
                    where: {
                        id: users[0].id
                    },
                    data:{
                        password: hashedPassword,
                        password_verification_code: null
                    }
                })
                return true
            }else{
                throw new Error("The verification code doesn't belong to any user.")
            }
        } catch (e) {
            logger.error(e.message)
            throw e
        }
    }
})

export const resendEmailVerificationCode = mutationField('resendEmailVerificationCode', {
    type: "ConfirmResponse",
    args: {},
    resolve: async (parent, args, ctx) => {
        const USER_ID = getUserId(ctx);
        try {
            const user = await ctx.prisma.user({id: USER_ID})
            if(!user){
                throw new AuthenticationError()
            }

            // recreate a new code and send it to the user's email
            let verificationCode = Math.floor(100000 + Math.random() * 900000).toString();
            let updatedUser = await ctx.prisma.updateUser({
                data: {
                    verification_code: verificationCode
                },
                where: {
                    id: user.id
                }
            })
            if(!updatedUser){
                throw new Error("Something went wrong with sending your new code")
            }
            agenda.now('signup mail', {
                user: {
                    email: user.email,
                    name: user.first_name,
                    verification: verificationCode
                }, 
                subject: "Spincrow - please verify your mail"
            })

            return {state: true}
        } catch (e) {
            throw e
        }
    }
})