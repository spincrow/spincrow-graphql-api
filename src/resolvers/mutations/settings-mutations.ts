// @ts-nocheck
import {  mutationField } from 'nexus'
import { getUserId} from "../../utils"
import {ContractInput, CardInput, BankAccountInput} from "../input"
import agenda from "../../services/agenda-service"
import { parse } from 'path';
import {checkMilestones} from "../helpers"
import RaveService from "../../services/rave-service"
import logger from "../../services/logger"

/**
 * Creating a contract
 */
export const updateUserInformation = mutationField("createContract",{
    type: "Contract",
    args: {
        input: ContractInput.asArg({required: true})
    },
    resolve: async (parents, args, ctx) => {
        let userId = getUserId(ctx),
        client_code = Math.floor(100000 + Math.random() * 900000);
        args = checkMilestones(args);
        try{
            let user = await ctx.prisma.users({where: {id: userId}})
            let contract = await ctx.prisma.createContract({
                ...args.input, 
                owner: { connect: { id: userId}},  
                milestones: { create: args.input.milestones}, 
                client_code: client_code.toString()
            });
            if(contract){
                agenda.now('send contract mail', {
                    user: {
                        email: args.input.client_email,
                        name: args.input.client
                    }, 
                    contract: contract,
                    subject: "New mail from contracts creation"
                })
                return contract
            }else{
                throw new Error("Something went horribly wrong.")
            }
        }catch(err ){
            logger.error(err.message)
            throw err
        }
    }
});

export const addCard = mutationField("addCard", {
    type: "Card",
    args: {
        input: CardInput.asArg({required: true})
    },
    resolve: async (parents, {input}, ctx) => {
        let USER_ID = getUserId(ctx)
        if(USER_ID){
            let user = await ctx.prisma.user({id: USER_ID}).$fragment(`{
                id 
                email
                wallet {
                    id
                    amount
                }
            }`)
            try {
                let raveService = new RaveService()
                let resp = await raveService.getCardToken(input.txRef)
                if(resp){
                    // create a card with the transaction reference.
                    let card = await ctx.prisma.createCard({
                        expirymonth: resp.expirymonth,
                        expiryyear: resp.expiryyear,
                        owner: {
                            connect: {
                                id: USER_ID,
                            }
                        },
                        token: resp.token,
                        last_number: resp.last_numbers,
                        type: resp.type
                    });
                    let currentWalletAmount = user.wallet.amount
                    await ctx.prisma.updateWallet({
                        data: {
                            amount: currentWalletAmount + 100,
                            transactions: {
                                create: {
                                    amount: parseFloat("100"),
                                    flow: "CREDIT",
                                    details: `Added a new card`,
                                    status: "SUCCESSFUL"
                                }
                            }
                        },
                        where: {
                            id: user.wallet.id
                        }
                    })
                    
                    return card
                }
            } catch (e) {
                throw e
            }
        }else{
            throw new Error("UNAUTHENTICATED USER")
        }
    }
})

export const addBankAccount = mutationField("addBankAccount", {
    type: "BankAccount",
    args: {
        input: BankAccountInput.asArg({required: true})
    },
    resolve: async (parents, args, ctx) => {
        let USER_ID = getUserId(ctx)
        if(USER_ID){
            let user = await ctx.prisma.user({id: USER_ID})
            let account = await ctx.prisma.createBankAccount({
                ...args.input,
                owner: {
                    connect: {
                        id: USER_ID,
                    }
                }
            });
            return account
        }else{
            throw new Error("UNAUTHENTICATED USER")
        }
    }
})

export const deleteCard = mutationField("deleteCard", {
    type: "Card",
    args: {
        input: "ID"
    },
    resolve: async (parents, {input}, ctx) => {
        let USER_ID = getUserId(ctx)
        if(USER_ID){
            let user = await ctx.prisma.user({id: USER_ID})
            try{
                let card = await ctx.prisma.deleteCard({id: input})
                return card
            }catch(err){
                throw new Error("Card doesn't exist for this user.")
            }
        }else{
            throw new Error("UNAUTHENTICATED USER")
        }
    }
})

export const deleteBankAccount = mutationField("deleteBankAccount", {
    type: "BankAccount",
    args: {
        input: "ID"
    },
    resolve: async (parents, {input}, ctx) => {
        let USER_ID = getUserId(ctx)
        if(USER_ID){
            let user = await ctx.prisma.user({id: USER_ID})
            let account = await ctx.prisma.deleteBankAccount({id: input})
            return account
        }else{
            throw new Error("UNAUTHENTICATED USER")
        }
    }
})
