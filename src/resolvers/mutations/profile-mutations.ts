import { mutationField, arg } from 'nexus'
import { getUserId, uploader} from "../../utils"
import logger from "../../services/logger"

export const updateProfile = mutationField("updateProfile",{
    type: "User",
    args: {
        input: "UserWhereInput"
    },
    resolve: async (parent, {input}, ctx) => {
        let userId = getUserId(ctx);
        let user = await ctx.prisma.updateUser({
            data: {
                ...input
            },
            where: {
                id: userId
            }
        });
        return user;
    }
});

export const uploadProfilePic = mutationField("uploadProfilePic",{
    type: "File",
    args: {
        file: arg({type: "Upload"})
    },
    resolve: async (parent, {file}, ctx) => {
        const userId = getUserId(ctx)
        // upload the files base to cloudinary assign the file instance to the user
        try {
            let result = await uploader(file)
            const user = await ctx.prisma.updateUser({
                data: {
                profile_image: result.path
                },
                where: {
                id: userId
                }
            })
            return result;
        }catch(error){
            logger.error(error.message)
            throw error
        }
    }
});