import { 
    signup, 
    login, 
    passwordUpdate, 
    confirmCode, 
    sendPhoneConfirmation, 
    verifySMSCode,
    updateForgottenPassword,
    forgotPasswordEnterEmail,
    resendEmailVerificationCode
} from './auth-mutations'
import { 
    createContract, 
    updateContractMilestone, 
    addContractClientAccount,
    withdrawFromContractToAccount,
    withdrawFromMilestoneToAccount,
    disburseContractMilestoneForClient,
    fundContractAsClient,
    disburseContractWholePayment,
    fundContractAsUser,
    fundWithCard,
    requestRetractionAsClient,
    requestRetractionAsUser,
    approveRetractionAsClient,
    approveRetractionAsUser,
    cancelRetractionAsClient,
    cancelRetractionAsUser
 } from './contracts-mutations'
import {contractsTourUpdate, homeTourUpdate, singleContractTourUpdate, walletTourUpdate} from "./tour-mutations"
import {addCard, deleteCard, addBankAccount, deleteBankAccount} from "./settings-mutations"
import { uploadProfilePic, updateProfile } from './profile-mutations'
import { topUpWallet, withdrawFunds } from "./wallet-mutation"
import {resolveBankAccount} from "./extra-mutations"

export const Mutation = {
    signup,
    login,
    updateForgottenPassword,
    forgotPasswordEnterEmail,
    addCard,
    topUpWallet,
    withdrawFunds,
    deleteCard,
    sendPhoneConfirmation,
    verifySMSCode,
    confirmCode,
    createContract,
    addContractClientAccount,
    withdrawFromContractToAccount,
    withdrawFromMilestoneToAccount,
    uploadProfilePic,
    addBankAccount,
    deleteBankAccount,
    passwordUpdate,
    updateProfile,
    updateContractMilestone,
    disburseContractMilestoneForClient,
    fundContractAsClient,
    fundWithCard,
    disburseContractWholePayment,
    fundContractAsUser,
    homeTourUpdate,
    contractsTourUpdate,
    singleContractTourUpdate,
    walletTourUpdate,
    requestRetractionAsClient,
    requestRetractionAsUser,
    approveRetractionAsClient,
    approveRetractionAsUser,
    cancelRetractionAsClient,
    cancelRetractionAsUser,
    resolveBankAccount,
    resendEmailVerificationCode
}

export default Mutation