// @ts-nocheck
import {  mutationField, idArg } from 'nexus'
import { getUserId} from "../../utils"
import {    
    ContractInput, 
    ContractUpdateInput, 
    MilestoneUpdateInput, 
    MilestoneUpdateInputClient, 
    ContractStateUpdate,  
    ContractStateUpdateWithPay, 
    ContractWholeInput,
    AccountDetail,
    withdrawalRequestInput,
    ContractStateUpdateWithCard,
    RetractionRequestInput,
    RetractionApprovalRequestInput,
    CancelRetractionInput
} from "../input"
import MonnifyService from "../../services/monnify-service"
import agenda from "../../services/agenda-service"
import { parse } from 'path';
import {checkMilestones, checkWholePayments} from "../helpers"
import logger from "../../services/logger"
import RaveService from '../../services/rave-service'
import {generateUUID, generateBaseUUID} from "../../utils"
import {processCardFunding, processWalletFunding, withdrawFunds} from "../../services/fund-service"
import { generateActivityForUser } from '../../services/extra-jobs'
import { helmet } from '../../validation'
/**
 * Creating a contract
 */
export const createContract = mutationField("createContract",{
    type: "Contract",
    args: {
        input: ContractInput.asArg({required: true})
    },
    resolve: helmet(async (parents, args, ctx) => {
        let userId = getUserId(ctx), client_code = Math.floor(100000 + Math.random() * 900000);
        let user = await ctx.prisma.user({id: userId})
        if(!user){
            throw new Error("Not Authorised: The user doesn't exist")
        }
        switch (args.input.payment_type) {
            case "whole":
                args = checkWholePayments(args);
                break;
            case "spread":
                args = checkMilestones(args);
                break;
            default:
                break;
        }
        try{
            if(parseFloat(args.input.amount) == 0){
                throw new Error("Please specify an amount on your contract for either whole payment or milestones")
            }
            // get virtual accounts here
            let monnifyService = new MonnifyService();
            const accountReference = generateBaseUUID()
            let monnifyAccountResponse = await monnifyService.createContractAccountNo({
                email: user.email,
                accountReference: accountReference,
                name: args.input.name.replace(/ /g,"_"),
                owner_name: `${user.first_name} ${user.last_name}`
            })
            logger.info(JSON.stringify(monnifyAccountResponse))
            // let monnifyAccountResponse = {}
            if(!monnifyAccountResponse.requestSuccessful){
                throw new Error("Couldn't create your contract's account no")
            }

            let contract: Contract | null
            contract = await ctx.prisma.createContract({
                ...args.input, 
                va_created_at: new Date(monnifyAccountResponse.responseBody.createdOn),
                va_account_number: monnifyAccountResponse.responseBody.accountNumber,
                va_order_ref: monnifyAccountResponse.responseBody.accountReference,
                va_account_name: monnifyAccountResponse.responseBody.accountName,
                va_bank_name: monnifyAccountResponse.responseBody.bankName,
                va_note: "monnify contract account no",
                owner: { connect: { id: userId}},
                milestones: { create: args.input.milestones}, 
                client_code: client_code.toString()
            });
            if(contract){
                await generateActivityForUser({
                    user_id: user.id,
                    detail: `You just create a new contract ${contract.name}`
                })
                agenda.now('send contract mail', {
                    user: {
                        email: args.input.client_email,
                        name: args.input.client
                    }, 
                    contract: contract,
                    owner: user,
                    subject: "New mail from contracts creation"
                })
                agenda.now('notify_client_on_contract_creation', {
                    contract: contract,
                    owner: user
                })
                return contract
            }else{
                throw new Error("Something went horribly wrong.")
            }
        }catch(err ){
            logger.error(err.message)
            throw err
        }
    })
});

export const updateContract = mutationField("updateContract", {
    type: "Contract",
    args: {
        input: ContractUpdateInput
    },
    resolve: async  (parent, args, ctx) => {
        let contract = await ctx.prisma.updateContract({
                where: {
                    id: args.input.contract_id
                },
                data:{
                    ...args.input.data
                }
            })
        return contract
    }
})

export const updateContractMilestone = mutationField("updateContractMilestone", {
    type: "MileStone",
    args: {
        input: MilestoneUpdateInput
    },
    resolve: async (parents, args, ctx) => {
        // get the current contract, then get the milestone in the contract.
        let milestone = await ctx.prisma.updateMileStone({ 
            where : { 
                id: args.input.milestone_id
            },
            data: {
                progress: args.input.progress,
                state: args.input.state
            }
        })
        return milestone
    }
});

// disburse funds for client milestones
export const disburseContractMilestoneForClient = mutationField("disburseContractMilestoneForClient", {
    type: "Contract",
    args: {
        input: MilestoneUpdateInputClient
    },
    resolve: async (parents, args, ctx) => {
        // get the current contract, then get the milestone in the contract.
        // on disbursing funds transfer value to wallet.
        try {
            let contract = await ctx.prisma.contract({id:  args.input.contract_id}).$fragment(
                `{ id amount owner {id first_name} owner_type milestones { disburseStatus } name status client_phone client client_email}`
            );
            let user = await ctx.prisma.user({id: contract.owner.id}).$fragment(
                `{id wallet {amount id} first_name last_name}`
            );

            if(contract.status == "INACTIVE"){
                throw new Error("This contract hasn't been funded")
            }
            let updatedContract = null;
            let milestone = await ctx.prisma.updateMileStone({ 
                where : { 
                    id: args.input.milestone_id
                },
                data: {
                    disburseStatus: args.input.disburseStatus,
                    status: args.input.status,
                }
            })
    
            // based on the type of contract we have to update the contract disbursement logic
            switch (contract.owner_type) {
                case "buyer":
                    await buyerDisbursementMilestoneProcess(ctx, user, milestone, contract)
                    break;
                case "seller":
                    await sellerDisbursementMilestoneProcess(ctx, user, milestone, contract)
                    break;
                default:
                    break;
            }
            let updatedContract = await ctx.prisma.contract({id:  args.input.contract_id})
            return updatedContract
        } catch (e) {
            logger.error(e.message)
            throw e
        }
    }
});

// Whole disbursements for buyers and sellers
export const disburseContractWholePayment = mutationField("disburseContractWholePayment", {
    type: "Contract",
    args: {
        input: ContractWholeInput.asArg({required: true})
    },
    resolve: async (parents, args, ctx) => {
        try {
            let contract = await ctx.prisma.contract({id:  args.input.contract_id}).$fragment(
                `{ id amount owner {id first_name} owner_type milestones { disburseStatus } name status client_phone client_email client }`
            );
            let user = await ctx.prisma.user({id: contract.owner.id}).$fragment(
                `{id wallet {amount id} first_name last_name}`
            );

            if(contract.status == "INACTIVE"){
                throw new Error("This contract hasn't been funded")
            }

            switch (contract.owner_type) {
                case "buyer":
                    await buyerDisbursementWholeProcess(ctx, user,args, contract)
                    break;
                case "seller":
                    sellerDisbursementWholeProcess(ctx, user, args, contract)
                    break;
                default:
                    break;
            }
            let updatedContract = await ctx.prisma.contract({id:  args.input.contract_id})
            return updatedContract
        } catch (e) {
            logger.error(e.message)
            throw e
        }
        // get the current contract, then get the milestone in the contract.
    }
});

export const fundContractAsClient = mutationField("fundContractAsClient", {
    type: "Contract",
    args: {
        input: ContractStateUpdate.asArg({required: true})
    },
    resolve: async (parents, args, ctx) => {
        let contractUpdated = null;
        let owner = await ctx.prisma.contract({id: args.input.contract_id}).owner()
        if(args.input.contract_state == 'ACTIVE' && args.input.amount){
            // confirm the transaction is clean
            let raveService = new RaveService()

            let resp = await raveService.verifyTransactionReference(args.input.transactionCode)
            if(resp.data.status == "successful"){
                contractUpdated = await ctx.prisma.updateContract({
                    where: {
                        id: args.input.contract_id
                    },
                    data:{
                        status: args.input.contract_state,
                        transactions: {
                            create: {
                                amount: args.input.amount,
                                flow: "CREDIT",
                                details: "FUNDED CONTRACT",
                                transaction_code: args.input.transactionCode,
                                status: "SUCCESSFUL"
                            }
                        }
                    }
                })


                // job to notify user about funding.
                await agenda.now('funded_contract', {
                    user: {
                        name: owner.first_name,
                        phone: owner.phone,
                        email: owner.email
                    },
                    actor: {
                        name: contractUpdated.client.split(" ")[0],
                        sendcode: false
                    },
                    contract: {
                        name: contractUpdated.name,
                        amount: contractUpdated.amount,
                        client_code : contractUpdated.client_code,
                        id: contractUpdated.id
                    }
                })
            }else{
                logger.error("Transaction is invalid")
                throw new Error("The Transaction is invalid")
            }
        }else{
            contractUpdated = await ctx.prisma.updateContract({
                where: {
                    id: args.input.contract_id
                },
                data:{
                    status: args.input.contract_state
                }
            })
        }
        return contractUpdated
    }
})

export const fundContractAsUser = mutationField("fundContractAsUser", {
    type: "Contract",
    args: {
        input: ContractStateUpdateWithPay.asArg({required: true})
    },
    resolve: async (parents, args, ctx) => {
        let contractUpdated = null;
        let userId = getUserId(ctx)
        let user = await ctx.prisma.user({id: userId}).$fragment(`{
            id
            wallet {
                id
            }
        }`)
        // make a payment withdrawal from the payment method, card or wallet.
        // if payment succeed update the contract accordingly.
        try {
            let contractStatus = await ctx.prisma.contract({id: args.input.contract_id})
            let owner = await ctx.prisma.contract({id: args.input.contract_id}).owner()
            if(contractStatus){
                if(contractStatus.status == "INACTIVE"){
                    switch (args.input.funding_type) {
                        case "WALLET":
                            let resp = await processWalletFunding(
                                ctx, 
                                user.wallet.id,
                                 args.input.amount
                            )
                            if(resp){
                                contractUpdated = await ctx.prisma.updateContract({
                                    where: {
                                        id: args.input.contract_id
                                    },
                                    data:{
                                        status: "ACTIVE",
                                        transactions: {
                                            create: {
                                                amount: args.input.amount,
                                                flow: "CREDIT",
                                                details: "FUNDED CONTRACT by wallet",
                                                status: "SUCCESSFUL"
                                            }
                                        }
                                    }
                                })
                                await agenda.now('funded_contract', {
                                    user: {
                                        name: contractUpdated.client.split(" ")[0],
                                        phone: contractUpdated.client_phone,
                                        email: contractUpdated.client_email,
                                    },
                                    actor: {
                                        name: owner.first_name,
                                        sendcode: true
                                    },
                                    contract: {
                                        name: contractUpdated.name,
                                        amount: contractUpdated.amount,
                                        client_code : contractUpdated.client_code,
                                        id: contractUpdated.id
                                    }
                                })
                                await generateActivityForUser({
                                    user_id: owner.id,
                                    detail: `Just funded your contract ${contractUpdated.name} by wallet transfer`,
                                    amount: args.input.amount
                                })
                            }
                            break;
                        case "CARD":
                            let resp = await processCardFunding(
                                ctx, 
                                args.input.card_id,
                                args.input.amount
                            )
                            if(resp.status == "success"){
                                contractUpdated = await ctx.prisma.updateContract({
                                    where: {
                                        id: args.input.contract_id
                                    },
                                    data:{
                                        status: "ACTIVE",
                                        transactions: {
                                            create: {
                                                amount: args.input.amount,
                                                transaction_code: resp.txRef,
                                                flow: "CREDIT",
                                                details: "FUNDED CONTRACT by card",
                                                status: "SUCCESSFUL"
                                            }
                                        }
                                    }
                                })
                                await generateActivityForUser({
                                    user_id: owner.id,
                                    detail: `Just funded your contract ${contractUpdated.name} card`,
                                    amount: args.input.amount
                                })
                            }else{
                                throw new Error("Your card balance might be too low")
                            }
                            break; 
                        default:
                            break;
                    }
                    return contractUpdated;
                }else{
                    throw new Error("This contract has already been funded")
                }
            }
        } catch (e) {
            logger.error(e.message)
            throw e
        }
    }
})

export const fundWithCard = mutationField("fundWithCard", {
    type: "Contract",
    args: {
        input: ContractStateUpdateWithCard.asArg({required: true})
    },
    resolve: async (parents, {input}, ctx) => {
        let USER_ID = getUserId(ctx)
        if(USER_ID){
            let user = await ctx.prisma.user({id: USER_ID}).$fragment(`{
                id 
                email
                wallet {
                    id
                    amount
                }
            }`)
            try {
                let raveService = new RaveService()
                let resp = await raveService.getCardToken(input.tx_ref)
                if(resp){
                    // create a card with the transaction reference.
                    await ctx.prisma.createCard({
                        expirymonth: resp.expirymonth,
                        expiryyear: resp.expiryyear,
                        owner: {
                            connect: {
                                id: USER_ID,
                            }
                        },
                        token: resp.token,
                        last_number: resp.last_numbers,
                        type: resp.type
                    });
                    // let currentWalletAmount = user.wallet.amount
                    let contractUpdated = await ctx.prisma.updateContract({
                        where: {
                            id: input.contract_id
                        },
                        data:{
                            status: "ACTIVE",
                            transactions: {
                                create: {
                                    amount: input.amount,
                                    flow: "CREDIT",
                                    details: "FUNDED CONTRACT by card",
                                    status: "SUCCESSFUL"
                                }
                            }
                        }
                    })

                    await generateActivityForUser({
                        user_id: user.id,
                        detail: `You just funded the contract ${contractUpdated.name} by card`,
                        amount: input.amount
                    })
                    
                    return contractUpdated
                }
            } catch (e) {
                throw e
            }
        }else{
            throw new Error("UNAUTHENTICATED USER")
        }
    }
})

export const addContractClientAccount = mutationField("addContractClientAccount", {
    type: "Contract",
    args: {
        input: AccountDetail.asArg({required: true})
    },
    resolve: async (parents, args, ctx) => {
        try {
            let updatedContract = await ctx.prisma.updateContract({
                where: {
                    id: args.input.contract_id
                },
                data: {
                    client_account_no: args.input.account_no,
                    client_bank_code: args.input.bank_code,
                    client_account_name: args.input.account_name,
                    automaticWithdrawal: true
                }
            })
            // if contract is set let go
            return updatedContract
        } catch (e) {
            logger.error(e.message)
            throw e
        }
    }
})

export const withdrawFromMilestoneToAccount = mutationField("withdrawFromMilestoneToAccount", {
    type: "Contract",
    args: {
        input: withdrawalRequestInput.asArg({required: true})
    },
    resolve: helmet(async (parents, {input}, ctx) => {
        try {
            // let owner = await ctx.prisma.contract({id: input.contract_id}).owner();
            let contract = await ctx.prisma.contract({id: input.contract_id})
            let owner = await ctx.prisma.contract({id: input.contract_id}).owner()
            let milestones = await ctx.prisma.contract({id: input.contract_id}).milestones()
            if(!owner){
                throw new Error("Owner of the contract, is inaccessible")
            }
            if(input.client_code == contract.client_code){
                // the client can make the withdrawal.
                let filteredMilestones = milestones.filter((milestone) => { 
                    return (milestone.id == input.milestone_id) && (milestone.available == true)
                })
                if(filteredMilestones.length > 0){
                    // the milestone is for the contract.
                    let milestone = filteredMilestones[0]
                    // make sure its available.
                    // make the withdrawal for the milestone
                    if(milestone.paid == true){
                        throw new Error("funds have already been paid for this milestone")
                    }
                    if(milestone.withdrawalStatus == "PENDING" || milestone.withdrawalStatus == "COMPLETED"){
                        throw new Error("The withdrawal has already been processed")
                    }
                    if(contract.client_account_name == null || contract.client_account_no == null){
                        throw new Error("Setup your account details to withdraw funds");
                    }
                    let withdrawalStatus = await withdrawFunds(
                        ctx,
                        {
                            bank_code: contract.client_bank_code,
                            account_name: contract.client_account_name,
                            account_number: contract.client_account_no,
                            amount: milestone.target
                        }
                    )

                    // update the milestone.
                    if(withdrawalStatus.status == "success"){
                        let updatedcontract = await ctx.prisma.updateContract({
                            where: {id: input.contract_id},
                            data: {
                                transactions: {
                                    create: {
                                        amount: input.amount,
                                        transaction_code: withdrawalStatus.txRef,
                                        milestone: {
                                            connect: {
                                                id: input.milestone_id
                                            }
                                        },
                                        flow: "DEBIT",
                                        details: `Milestone funds of ${input.amount} have been withdrawn`,
                                        status: "PENDING"
                                    }
                                },
                            }
                        })
                         await ctx.prisma.updateMileStone({
                            where: {
                                id: input.milestone_id
                            },
                            data: {
                                withdrawalStatus: "PENDING"
                            }
                        })
                        await generateActivityForUser({
                            user_id: owner.id,
                            detail: `Withdrawal of milestone from contract ${updatedContract.name} was initiated`,
                            amount: input.amount
                        })
                        return updatedcontract
                    }
                }else{
                    throw new Error("No milestones match your contract milestone")
                }
            }else{
                throw new Error("you don't have the credentials to manipulate this contract.")
            }
        } catch (e) {
            logger.error(e.message)
            throw e
        }
    })
})

export const withdrawFromContractToAccount = mutationField("withdrawFromContractToAccount", {                                                                                                                                                                                                                                                                                                  
    type: "Contract",
    args: {
        input: withdrawalRequestInput.asArg({required: true})
    },
    resolve: helmet(
        async (parents, {input}, ctx) => {
            try {
                let contract = await ctx.prisma.contract({id: input.contract_id})
                let owner = await ctx.prisma.contract({id: input.contract_id}).owner()
                if(!owner){
                    throw new Error("Owner of the contract, is inaccessible")
                }
                if((input.client_code == contract.client_code) && (contract.payment_type == "whole")){
                    // the client can make the withdrawal.
                    // check if the contract has already been withdrawn
                    if(contract.paid == true){
                        throw new Error("funds have already been paid for this contract")
                    }
                    if(contract.withdrawalStatus == "PENDING" || contract.withdrawalStatus == "COMPLETED"){
                        throw new Error("The withdrawal has already been processed")
                    }
    
                    if(contract.client_account_name == null || contract.client_account_no == null){
                        throw new Error("Setup your account details to withdraw funds");
                    }
                    let withdrawalStatus = await withdrawFunds(
                        ctx,
                        {
                            bank_code: contract.client_bank_code,
                            account_name: contract.client_account_name,
                            account_number: contract.client_account_no,
                            amount: contract.amount
                        }
                    )
    
                    if(withdrawalStatus.status == "success"){
                        contract = await ctx.prisma.updateContract({
                            where:{id: input.contract_id},
                            data: {
                                transactions: {
                                    create: {
                                        amount: input.amount,
                                        transaction_code: withdrawalStatus.txRef,
                                        flow: "DEBIT",
                                        details: `Contract funds of ${input.amount} have been withdrawn`,
                                        status: "PENDING"
                                    }
                                },
                                withdrawalStatus: "PENDING"
                            }
                        })
                        await generateActivityForUser({
                            user_id: owner.id,
                            detail: `Withdrawn from contract ${contract.name} to client has been intitiated`,
                            amount: input.amount
                        })
                        return contract
                    }
                }else{
                    throw new Error("you don't have the credentials to manipulate this contract.")
                }
            } catch (e) {
                logger.error(e.message)
                throw e
            }
        }
    )
})

// The retraction feature set
export const requestRetractionAsClient = mutationField("requestRetractionAsClient", {
    type: "Contract",
    args: { 
        input: RetractionRequestInput.asArg({required: true})
    },
    resolve: async (parent, {input}, ctx) => {
        // the retraction would be required for both buyer and seller depending on if the user is logged In.
        let contract  = await ctx.prisma.contract({id: input.contract_id}).$fragment(`{ name id client client_code owner_type amount status retraction_status client_email owner {
            first_name
            last_name
            client
            owner {
                id
                first_name
                phone
                email
            }
            phone
            email
        }}`)
        let milestones = await ctx.prisma.contract({id: input.contract_id}).milestones({where: {id: input.milestone_id}})
        if(contract.status == "INACTIVE"){
            throw new Error("This contract hasn't been funded")
        }
        // the request is not from a user.
        // request the retraction for a client side user
        // make sure the contract is a seller type.
        if(contract.owner_type == "seller"){
            // I am retracting from owner.
            if(input.milestone_id){
                // we are retracting for a milestone.
                // send mail request to approve a retraction for the milestone.
                // if a milestone retraction is in progress request mail to get the milestone retracted.
                let updatedMilestone = await ctx.prisma.updateMileStone({
                    where: {
                        id: milestones[0].id
                    },
                    data: {
                        retraction_status: "REQUESTED"
                    }
                })
                // send mail to request retraction.
                await generateActivityForUser({
                    user_id: contract.owner.id,
                    detail: `Retraction requested on contract ${updatedContract.name} by ${contract.client.split(" ")[0]}`,
                })
                agenda.now("retractionForMilestone", {
                    contract: {
                        ...contract
                    },
                    recipient: {
                        name: contract.owner.first_name,
                        email: contract.owner.email,
                        phone: contract.owner.phone
                    },
                    actor: {
                        name: contract.client.split(" ")[0],
                        sendcode: false
                    },
                    subject: `${contract.client} just requested a retraction from the contract ${contract.name}`
                });
                return await ctx.prisma.contract({id: input.contract_id})
            }else{
                // retraction for contract, if the contract is whole payment.
                let updatedContract = await ctx.prisma.updateContract({
                    where: {
                        id: input.contract_id
                    },
                    data: {
                        retraction_status: "REQUESTED"
                    }
                })
                await generateActivityForUser({
                    user_id: contract.owner.id,
                    detail: `Retraction requested on contract ${contract.name} by ${contract.client.split(" ")[0]}`,
                })
                agenda.now("retractionForContract", {
                    contract: {
                        ...contract
                    },
                    recipient: {
                        name: contract.owner.first_name,
                        email: contract.owner.email
                    },
                    actor: {
                        name: contract.client.split(" ")[0],
                        sendcode: false
                    },
                    subject: `${contract.client} just requested a retraction from the contract ${contract.name}`
                });
                return updatedContract
            }
        }else{
            throw new Error("You can only request retractions on contracts where you are the buyer")
        }
    }
})

export const requestRetractionAsUser = mutationField("requestRetractionAsUser", {
    type: "Contract",
    args: { 
        input: RetractionRequestInput.asArg({required: true})
    },
    resolve: async (parent, {input}, ctx) => {
        // the retraction would be required for both buyer and seller depending on if the user is logged In.
        let userId  = getUserId(ctx)
        let contract  = await ctx.prisma.contract({id: input.contract_id}).$fragment(`{ name id client client_code owner_type amount status retraction_status client_email owner {
            first_name
            last_name
            phone
            email
        }}`)
        let milestones = await ctx.prisma.contract({id: input.contract_id}).milestones({where: {id: input.milestone_id}})
        if(contract.status == "INACTIVE"){
            throw new Error("This contract hasn't been funded")
        }
        if(userId){
            // the request is from a user
            // check is he's a buyer or a seller.
            if(contract.owner_type == "buyer"){
                // cool therefore this user can request a retraction.
                // retraction for milestone or retraction for contract.
                if(input.milestone_id){
                    // we are retracting for a milestone.
                    // send mail request to approve a retraction for the milestone.
                    // if a milestone retraction is in progress request mail to get the milestone retracted.
                    await ctx.prisma.updateMileStone({
                        where: {
                            id: milestones[0].id
                        },
                        data: {
                            retraction_status: "REQUESTED"
                        }
                    })
                    // send mail to request retraction.
                    agenda.now("retractionForMilestone", {
                        contract: {
                            ...contract
                        },
                        recipient: {
                            name: contract.client.split(" ")[0],
                            email: contract.client_email
                        },
                        actor: {
                            name: contract.owner.first_name,
                            sendcode: true
                        },
                        subject: `${contract.owner.first_name} just requested a retraction from the contract ${contract.name}`
                    });
                    return await ctx.prisma.contract({id: input.contract_id})
                }else{
                    // retraction for contract, if the contract is whole payment.
                    let updatedContract = await ctx.prisma.updateContract({
                        where: {
                            id: input.contract_id
                        },
                        data: {
                            retraction_status: "REQUESTED"
                        }
                    })
                    agenda.now("retractionForContract", {
                        contract: {
                            ...contract
                        },
                        recipient: {
                            name: contract.client.split(" ")[0],
                            email: contract.client_email
                        },
                        actor: {
                            name: contract.owner.first_name,
                            sendcode: true
                        },
                        subject: `${contract.owner.first_name} just requested a retraction from the contract ${contract.name}`
                    });
                    return updatedContract
                }
            }else{
                throw new Error("You can only request retractions on contracts where you are the buyer")
            }
        }else{
           throw new Error("The user is not authenticated")
        }
    }
})

// Approve retractions
/**'
 * The approval is being done by the user for the client, so the funds would be transferred to the clients bank account
 * A mail is sent to the client based on the approval state.
 */
export const approveRetractionAsUser = mutationField("approveRetractionAsUser", {
    type: "Contract",
    args: {
        input: RetractionApprovalRequestInput.asArg({required: true})
    },
    resolve: async (parent, {input}, ctx) => {
        // approve for the user trying to approve a clients retraction, is it a milestone retraction or a contract retraction
        let userId  = getUserId(ctx)
        let contract  = await ctx.prisma.contract({id: input.contract_id}).$fragment(`{ 
            name 
            id 
            client 
            client_code 
            owner_type 
            amount 
            status 
            retraction_status 
            client_email 
            client_bank_code
            client_account_name
            client_account_no
            owner {
                first_name
                last_name
                phone
                email
            }
        }`)
        if(contract.status == "INACTIVE"){
            throw new Error("This contract hasn't been funded")
        }
        let milestones = await ctx.prisma.contract({id: input.contract_id}).milestones({where: {id: input.milestone_id}})
        if(userId){
            // the request is from a user
            // check is he's a buyer or a seller.
            if(contract.owner_type == "seller"){
                // cool therefore this user can request a retraction.
                // retraction for milestone or retraction for contract.
                if(input.milestone_id){
                    if(milestones[0].retraction_status == "REQUESTED" || milestones[0].retraction_status == "REJECTED"){
                        // approving a retraction means the money is sent to the clients bank account, it's like a withdrawal.
                        switch (input.approve_status) {
                            case "APPROVED":
                                // approve retraction and fund account pending
                                let withdrawalStatus = await withdrawFunds(
                                    ctx,
                                    {
                                        bank_code: contract.client_bank_code,
                                        account_name: contract.client_account_name,
                                        account_number: contract.client_account_no,
                                        amount: milestones[0].target
                                    }
                                )
                                if(withdrawalStatus.status == "success"){
                                    await ctx.prisma.updateMileStone({
                                        where: {
                                            id: input.milestone_id
                                        },
                                        data: {
                                            retraction_status: "PENDING"
                                        }
                                    })
                                    let updatedContract = await ctx.prisma.updateContract({
                                        where:{id: input.contract_id},
                                        data: {
                                            transactions: {
                                                create: {
                                                    amount: milestones[0].target,
                                                    milestone: {
                                                        connect: {
                                                            id: input.milestone_id
                                                        }
                                                    },
                                                    transaction_code: withdrawalStatus.txRef,
                                                    flow: "DEBIT",
                                                    details: `Milestone funds of ${milestones[0].target} have been retracted`,
                                                    status: "PENDING"
                                                }
                                            }
                                        }
                                    })
                                    return updatedContract
                                }
                                break;
                            case "REJECTED":
                                // reject retraction and wait
                                await ctx.prisma.updateMileStone({
                                    where: {
                                        id: input.milestone_id
                                    },
                                    data: {
                                        retraction_status: "REJECTED"
                                    }
                                })
                                return await ctx.prisma.contract({id: input.contract_id})
                                break;
                            default:
                                return await ctx.prisma.contract({id: input.contract_id})
                                break;
                        }
                        return await ctx.prisma.contract({id: input.contract_id})
                    }else{
                        throw new Error("You can't approve a retraction for this milestone at the moment")
                    }
                }else{
                    // retraction for contract, if the contract is whole payment.
                    switch (input.approve_status) {
                        case "APPROVED":
                            // approve retraction and fund account pending
                            let withdrawalStatus = await withdrawFunds(
                                ctx,
                                {
                                    bank_code: contract.client_bank_code,
                                    account_name: contract.client_account_name,
                                    account_number: contract.client_account_no,
                                    amount:contract.amount
                                }
                            )
                            if(withdrawalStatus.status == "success"){
                                await ctx.prisma.updateContract({
                                    where: {
                                        id: input.contract_id
                                    },
                                    data: {
                                        retraction_status: "PENDING"
                                    }
                                })
                                let updatedContract = await ctx.prisma.updateContract({
                                    where:{id: input.contract_id},
                                    data: {
                                        transactions: {
                                            create: {
                                                amount: contract.amount,
                                                transaction_code: withdrawalStatus.txRef,
                                                flow: "DEBIT",
                                                details: `Contract funds ${contract.amount} are being retracted`,
                                                status: "PENDING"
                                            }
                                        },
                                        status: "COMPLETED"
                                    }
                                })
                                return updatedContract
                            }
                            break;
                        case "REJECTED":
                            // reject retraction and wait
                            let updatedContract = await ctx.prisma.updateContract({
                                where: {
                                    id: input.contract_id
                                },
                                data: {
                                    retraction_status: "REJECTED"
                                }
                            })
                            return updatedContract
                            break;
                        default:
                            return await ctx.prisma.contract({id: input.contract_id})
                            break;
                    }
                    // return await ctx.prisma.contract({id: input.contract_id})
                }
            }else{
                throw new Error("You can only request retractions on contracts where you are the buyer")
            }
        }else{
           throw new Error("The user is not authenticated")
        }
    }
})

/**
 * The Approval here is from the client, so the money is transfered to the user's wallet and an approval message is sent to the user.
 */
export const approveRetractionAsClient = mutationField("approveRetractionAsClient", {
    type: "Contract",
    args: {
        input: RetractionApprovalRequestInput.asArg({required: true})
    },
    resolve: async (parent, {input}, ctx) => {
        // approve for the user trying to approve a clients retraction, is it a milestone retraction or a contract retraction
        // let userId  = getUserId(ctx)
        let contract  = await ctx.prisma.contract({id: input.contract_id}).$fragment(`{ 
            name 
            id 
            client 
            client_code 
            owner_type 
            amount 
            status 
            retraction_status 
            client_email 
            client_bank_code
            client_account_name
            client_account_no
            owner {
                wallet {
                    id
                    amount
                }
                first_name
                last_name
                phone
                email
            }
        }`)
        if(contract.status == "INACTIVE"){
            throw new Error("This contract hasn't been funded")
        }
        let milestones = await ctx.prisma.contract({id: input.contract_id}).milestones({where: {id: input.milestone_id}})
        if(contract.owner_type == "buyer"){
            // cool therefore this user can request a retraction.
            // retraction for milestone or retraction for contract.
            if(input.milestone_id){
                if((milestones[0].retraction_status == "REQUESTED" || milestones[0].retraction_status == "REJECTED")){
                     // approving a retraction means the money is sent to the clients bank account, it's like a withdrawal.
                    switch (input.approve_status) {
                        case "APPROVED":
                        await ctx.prisma.updateWallet({
                                data: {
                                    amount: ( parseFloat(contract.owner.wallet.amount) + parseFloat(milestones[0].target)),
                                    transactions: {
                                        create: {
                                            amount: parseFloat(milestones[0].target),
                                            flow: "CREDIT",
                                            transaction_code: input.tx_ref,
                                            details: `Wallet top-up from from retraction on ${contract.name}`,
                                            status: "SUCCESSFUL"
                                        }
                                    }
                                },
                                where: {
                                    id: contract.owner.wallet.id
                                }
                            })
                            await ctx.prisma.updateMileStone({
                                where: {
                                    id: input.milestone_id
                                },
                                data: {
                                    retraction_status: "SUCCESSFUL",
                                    disburseStatus:"CONFIRMED"
                                }
                            })
                            let contractFilter = await ctx.prisma.contract({id:  input.contract_id}).$fragment(
                                `{ id amount owner {id first_name} owner_type milestones { disburseStatus } name status client_phone client client_email}`
                            );
                            let updatedContract = await ctx.prisma.updateContract({
                                where:{id: input.contract_id},
                                data: {
                                    transactions: {
                                        create: {
                                            amount: milestones[0].target,
                                            milestone: {
                                                connect: {
                                                    id: input.milestone_id
                                                }
                                            },
                                            flow: "DEBIT",
                                            details: `Milestone funds of ${milestones[0].target} have been retracted`,
                                            status: "SUCCESSFUL"
                                        },
                                        status: contractFilter.milestones.filter(item => item.disburseStatus != "CONFIRMED").length == 0 ? "COMPLETED" : contractFilter.status
                                    }
                                }
                            })
                            return updatedContract;
                            break;
                        case "REJECTED":
                            // reject retraction and wait
                            await ctx.prisma.updateMileStone({
                                where: {
                                    id: input.milestone_id
                                },
                                data: {
                                    retraction_status: "REJECTED"
                                }
                            })
                            return await ctx.prisma.contract({id: input.contract_id})
                            break;
                        default:
                            return await ctx.prisma.contract({id: input.contract_id})
                            break;
                    }
                    return await ctx.prisma.contract({id: input.contract_id})
                }else{
                    throw new Error("You can't approve a retraction for this milestone at the moment")
                }
            }else{
                // retraction for contract, if the contract is whole payment.
                switch (input.approve_status) {
                    case "APPROVED":
                        // approve retraction and fund account pending
                        await ctx.prisma.updateWallet({
                            data: {
                                amount: ( parseFloat(contract.owner.wallet.amount) + parseFloat(contract.amount)),
                                transactions: {
                                    create: {
                                        amount: parseFloat(contract.amount),
                                        flow: "CREDIT",
                                        transaction_code: input.tx_ref,
                                        details: `Wallet top-up from from retraction on ${contract.name}`,
                                        status: "SUCCESSFUL"
                                    }
                                }
                            },
                            where: {
                                id: contract.owner.wallet.id
                            }
                        })
                        let updatedContract = await ctx.prisma.updateContract({
                            where:{id: input.contract_id},
                            data: {
                                retraction_status: "SUCCESSFUL",
                                transactions: {
                                    create: {
                                        amount: contract.amount,
                                        // transaction_code: withdrawalStatus.txRef,
                                        flow: "DEBIT",
                                        details: `Contract funds ${contract.amount} are being retracted`,
                                        status: "SUCCESSFUL"
                                    },
                                },
                                status: "COMPLETED"
                            }
                        })
                        return updatedContract
                        break;
                    case "REJECTED":
                        // reject retraction and wait
                        let updatedContract = await ctx.prisma.updateContract({
                            where: {
                                id: input.contract_id
                            },
                            data: {
                                retraction_status: "REJECTED"
                            }
                        })
                        return updatedContract
                        break;
                    default:
                        return await ctx.prisma.contract({id: input.contract_id})
                        break;
                }
                // return await ctx.prisma.contract({id: input.contract_id})
            }
        }else{
            throw new Error("You can only request retractions on contracts where you are the buyer")
        }
    }
})


// Mutations to cancel retractions.
export const cancelRetractionAsUser = mutationField("cancelRetractionAsUser", {
    type: "Contract",
    args: {
        input: CancelRetractionInput.asArg({required: true})
    },
    resolve: async (parent, {input}, ctx) => {
        let userId = getUserId(ctx)
        
        if(userId){
            let contract  = await ctx.prisma.contract({id: input.contract_id}).$fragment(`{ 
                name 
                id 
                client 
                client_code 
                owner_type 
                amount 
                status 
                retraction_status 
                client_email 
                client_bank_code
                client_account_name
                client_account_no
                owner {
                    wallet {
                        id
                        amount
                    }
                    first_name
                    last_name
                    phone
                    email
                }
            }`)
            if(contract.status == "INACTIVE"){
                throw new Error("This contract hasn't been funded")
            }
            let milestones = await ctx.prisma.contract({id: input.contract_id}).milestones({where: {id: input.milestone_id}})
            // for a particular retraction object controact or milestone
            if(contract.owner_type == "buyer"){
                // cool therefore this user can request a retraction.
                // retraction for milestone or retraction for contract.
                if(input.milestone_id){
                    if((milestones[0].retraction_status == "REQUESTED" || milestones[0].retraction_status == "REJECTED")){
                        // approving a retraction means the money is sent to the clients bank account, it's like a withdrawal.
                        await ctx.prisma.updateMileStone({
                            where: {
                                id: input.milestone_id
                            },
                            data: {
                                retraction_status: "INACTIVE",
                            }
                        })
                        let contractBase = await ctx.prisma.contract({id:  input.contract_id})
                        return contractBase;
                    }else{
                        throw new Error("You can't cancel this retraction for this milestone at the moment")
                    }
                }else{
                    // retraction for contract, if the contract is whole payment.
                    let updatedContract = await ctx.prisma.updateContract({
                        where:{id: input.contract_id},
                        data: {
                            retraction_status: "INACTIVE",
                        }
                    })
                    return updatedContract
                    // return await ctx.prisma.contract({id: input.contract_id})
                }
            }else{
                throw new Error("You can only cancel retractions on contracts where you are the buyer")
            }
        }
        // return true
    }
})

export const cancelRetractionAsClient = mutationField("cancelRetractionAsClient", {
    type: "Contract",
    args: {
        input: CancelRetractionInput.asArg({required: true})
    },
    resolve: async (parent, args, ctx) => {
        let contract  = await ctx.prisma.contract({id: input.contract_id}).$fragment(`{ 
            name 
            id 
            client 
            client_code 
            owner_type 
            amount 
            status 
            retraction_status 
            client_email 
            client_bank_code
            client_account_name
            client_account_no
            owner {
                wallet {
                    id
                    amount
                }
                first_name
                last_name
                phone
                email
            }
        }`)
        if(contract.status == "INACTIVE"){
            throw new Error("This contract hasn't been funded")
        }
        let milestones = await ctx.prisma.contract({id: input.contract_id}).milestones({where: {id: input.milestone_id}})
        // for a particular retraction object controact or milestone
        if(contract.owner_type == "seller"){
            // cool therefore this user can request a retraction.
            // retraction for milestone or retraction for contract.
            if(input.milestone_id){
                if((milestones[0].retraction_status == "REQUESTED" || milestones[0].retraction_status == "REJECTED")){
                    // approving a retraction means the money is sent to the clients bank account, it's like a withdrawal.
                    await ctx.prisma.updateMileStone({
                        where: {
                            id: input.milestone_id
                        },
                        data: {
                            retraction_status: "INACTIVE",
                        }
                    })
                    let contractBase = await ctx.prisma.contract({id:  input.contract_id})
                    return contractBase;
                }else{
                    throw new Error("You can't cancel this retraction for this milestone at the moment")
                }
            }else{
                // retraction for contract, if the contract is whole payment.
                let updatedContract = await ctx.prisma.updateContract({
                    where:{id: input.contract_id},
                    data: {
                        retraction_status: "INACTIVE",
                    }
                })
                return updatedContract
                // return await ctx.prisma.contract({id: input.contract_id})
            }
        }else{
            throw new Error("You can only cancel retractions on contracts where you are the buyer")
        }
    }
})

// helper functions
const sellerDisbursementMilestoneProcess  = async (ctx, user, milestone, contract) => {
        // to update the wallet of the contract owner, perform a withdrawal.
        // update wallet.
        // because we are updating wallet directly
        await ctx.prisma.updateMileStone({ 
            where : { 
                id: milestone.id
            },
            data: {
                available: false,
                paid: true
            }
        })
        let currentWalletAmount = user.wallet.amount
        await ctx.prisma.updateWallet({
            data: {
                amount: currentWalletAmount + milestone.target,
                transactions: {
                    create: {
                        amount: milestone.target,
                        flow: "CREDIT",
                        details: `Disburse to wallet from ${contract.name}`,
                        status: "SUCCESSFUL"
                    }
                }
            },
            where: {
                id: user.wallet.id
            }
        })
        // create transaction 
        return await ctx.prisma.updateContract({
            data: {
                transactions: {
                    create: {
                        amount: milestone.target,
                        flow: "DEBIT",
                        details: `Disbursed funds for milestone ${milestone.target}`,
                        milestone: {
                            connect: {
                                id: milestone.id
                            }
                        },
                        status: "SUCCESSFUL"
                    }
                },
                status: contract.milestones.filter(item => item.disburseStatus != "CONFIRMED").length == 0 ? "COMPLETED" : contract.status
            },
            where: {
                id: contract.id
            }
        })
}

const buyerDisbursementMilestoneProcess = async (ctx, user, milestone: MileStone, contract) => {
    // change the milestone state of the contract to disbursed so the client can accept the funds 
    // (they can set automatic withdrawal on a contract to a certain account no)
    try{
        let milestoneUpdate = await ctx.prisma.updateMileStone({ 
            where : { 
                id: milestone.id
            },
            data: {
                available: true,
                paid: false
            }
        })
    
        if(milestoneUpdate){
            let updatedContract = await ctx.prisma.updateContract({
                data: {
                    transactions: {
                        create: {
                            amount: milestone.target,
                            milestone: {
                                connect: {
                                    id: milestone.id
                                }
                            },
                            flow: "DEBIT",
                            details: `Disbursed funds for milestone ${milestone.target} to ${contract.client}`,
                            status: "SUCCESSFUL"
                        }
                    },
                    status: contract.milestones.filter(item => item.disburseStatus != "CONFIRMED").length == 0 ? "COMPLETED" : contract.status
                },
                where: {
                    id: contract.id
                }
            })
    
            agenda.now("sendDisburseNotification", {
                user: {
                    email: updatedContract.client_email,
                    name: updatedContract.client
                }, 
                contract: contract,
                amount: milestone.target,
                currency: "",
                owner: user,
                subject: "Funds have been disbursed to you - Attempt a withdrawal"
            })
    
            return updateContract
        }else{
            throw new Error(`The milestone has already been disbursed`)
        }
    }catch(e){
        logger.error(e.message)
        throw new Error(`${e.message}- Your milestone couldn't be updated updated`);
    }
}

const sellerDisbursementWholeProcess  = async (ctx, user, args,  contract) => {
    // to update the wallet of the contract owner, perform a withdrawal.
    // update wallet.
    try {
        let currentWalletAmount = user.wallet.amount
        await ctx.prisma.updateWallet({
            data: {
                amount: currentWalletAmount + contract.amount,
                transactions: {
                    create: {
                        amount: contract.amount,
                        flow: "CREDIT",
                        details: `Disburse to wallet from ${contract.name}`,
                        status: "SUCCESSFUL"
                    }
                }
            },
            where: {
                id: user.wallet.id
            }
        })
        // create transaction 
        return await ctx.prisma.updateContract({ 
            where : { 
                id: args.input.contract_id
            },
            data: {
                disburseStatus: "CONFIRMED",
                status: args.input.status,
                transactions: {
                    create: {
                        amount: contract.amount,
                        flow: "DEBIT",
                        details: `Disbursed funds for contract ${contract.id} -  ${contract.name}`,
                        status: "SUCCESSFUL"
                    }
                },
                status:  "COMPLETED"
            }
        })
    } catch (e) {
        logger.error(e.message)
        throw new Error(`${e.message}- Your contract couldn't be updated updated`)
    }
}

const buyerDisbursementWholeProcess = async (ctx, user, args, contract) => {
    // change the milestone state of the contract to disbursed so the client can accept the funds 
    try {
        let updatedContract = await ctx.prisma.updateContract({ 
            where : { 
                id: args.input.contract_id
            },
            data: {
                disburseStatus: "CONFIRMED",
                available: true,
                status: args.input.status,
                transactions: {
                    create: {
                        amount: contract.amount,
                        flow: "DEBIT",
                        details: `Disbursed funds for contract ${contract.id} -  ${contract.name}`,
                        status: "SUCCESSFUL"
                    }
                },
                status:  "COMPLETED"
            }
        })
        agenda.now("sendDisburseNotification", {
            user: {
                email: updatedContract.client_email,
                name: updatedContract.client
            }, 
            amount: contract.amount,
            currency: "",
            contract: contract,
            owner: user,
            subject: "Funds have been disbursed to you - Attempt a withdrawal"
        })
        return updateContract
    } catch (e) {
        logger.error(e.message)
        throw new Error(`${e.message}- Your contract couldn't be updated updated`)
    }
}
