import {mutationField } from 'nexus'
import { getUserId} from '../../utils'


export const homeTourUpdate = mutationField("homeTourUpdate", {
    type: "User",
    args: {},
    resolve: async (parent, {input}, ctx) => {
        let userId = getUserId(ctx)
        if(userId){
            let user = ctx.prisma.updateUser({
                where: {
                    id: userId
                },
                data: {
                    home_tour: true
                }
            })
            return user
        }else{
            throw new Error("UNAUTHENTICATED USER")
        }
    }
})

export const contractsTourUpdate = mutationField("contractsTourUpdate", {
    type: "User",
    args: {},
    resolve: async (parent, {input}, ctx) => {
        let userId = getUserId(ctx)
        if(userId){
            let user = ctx.prisma.updateUser({
                where: {
                    id: userId
                },
                data: {
                    contract_tour: true
                }
            })
            return user
        }else{
            throw new Error("UNAUTHENTICATED USER")
        }
    }
})


export const singleContractTourUpdate = mutationField("singleContractTourUpdate", {
    type: "User",
    args: {},
    resolve: async (parent, {input}, ctx) => {
        let userId = getUserId(ctx)
        if(userId){
            let user = ctx.prisma.updateUser({
                where: {
                    id: userId
                },
                data: {
                    single_contract_tour: true
                }
            })
            return user
        }else{
            throw new Error("UNAUTHENTICATED USER")
        }
    }
})

export const walletTourUpdate = mutationField("walletTourUpdate", {
    type: "User",
    args: {},
    resolve: async (parent, {input}, ctx) => {
        let userId = getUserId(ctx)
        if(userId){
            let user = ctx.prisma.updateUser({
                where: {
                    id: userId
                },
                data: {
                    wallet_tour: true
                }
            })
            return user
        }else{
            throw new Error("UNAUTHENTICATED USER")
        }
    }
})
