import { prismaObjectType } from "nexus-prisma"

// @ts-ignore
const BankAccount = prismaObjectType({
    name: "BankAccount",
    definition: (t) => {
        t.prismaFields([
            'id',
            'account_number',
            'account_name',
            'bank_name',
            'bank_code',
            'created_at',
            'updated_at',
            'owner'
        ])
    }
})

export default BankAccount