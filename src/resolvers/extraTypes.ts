import {objectType, scalarType} from "nexus"
import {GraphQLUpload} from "graphql-upload"


export const AuthPayload = objectType({
    name: "AuthPayload",
    definition: t => {
        t.string("token")
        t.field("user", {
            type: "User"
        })
    }
})

export const WalletUpdatedPayload = objectType({
    name: "WalletUpdatedPayload",
    definition: t => {
        t.string("mutation")
        t.field("node", {type: "Wallet"})
        t.list.string("updatedFields")
    }
})

export const ResolvedAccount = objectType({
    name: "ResolvedAccount",
    definition: t => {
        t.string("account_name"),
        t.string("account_number")
    }
})

export const ConfirmationResponse = objectType({
    name: "ConfirmResponse",
    definition: t => {
        t.boolean("state")
    }
})

export const BankCode = objectType({
    name: "BankCode",
    definition: t => {
        t.string("bankname")
        t.string("bankcode")
        t.boolean("internetbanking")
    }
})

export const File = objectType({
    name: "File",
    definition: t => {
        t.id("id")
        t.string("path")
        t.string("filename")
        t.string("mimetype")
        t.string("encoding")
    }
})

export const Upload = GraphQLUpload;

export const Stats = objectType({
    name: "Stats",
    definition: t => {
        t.float("wallet_balance"),
        t.int("active_contracts"),
        t.int("completed_contracts")
    }
})