import { prismaObjectType } from "nexus-prisma"

// @ts-ignore
const Card = prismaObjectType({
    name: "Card",
    definition: (t) => {
        t.prismaFields([
            'id',
            'owner',
            'token',
            'card_name',
            'expirymonth',
            'expiryyear',
            'last_number',
            'type'
        ])
    }
})

export default Card;