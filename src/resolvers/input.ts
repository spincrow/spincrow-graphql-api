
import {inputObjectType, enumType} from "nexus"
import {prismaInputObjectType} from "nexus-prisma"
// Input types

 const FundingTypeEnum = enumType({
  name: "FundingTypeEnum",
  members: ["WALLET", "CARD"]
})

const RetractionApprovedEnum = enumType({
  name: "RetractionApprovedEnum",
  members: ["APPROVED", "REJECTED"]
})



const TopUpCardTypeEnum = enumType({
  name: "TopUpCardTypeEnum",
  members: ["NEW_CARD", "CARD"]
})

const WithdrawalTypeEnum = enumType({
  name: "WithdrawalTypeEnum",
  members: ["NORMAL_WITHDRAWAL", "ADD_ACCOUNT"]
})

//  -- AUTHENTICATION INPUTS --
export const SignupInput = inputObjectType({
  name: 'SignupInput',
  definition: (t)=>{
    t.string('first_name', {nullable: false}),
    t.string('last_name', {nullable: false}),
    t.string('email', {nullable: false}),
    t.string('password', {nullable: false})
    t.string('phone', {nullable: true})
  }
})


export const LoginInput = inputObjectType({
  name: 'LoginInput',
  definition: (t) => {
    t.string("password", {nullable: false}),
    t.string("email", {nullable: false})
  }
})

export const PasswordUpdate = inputObjectType({
  name: "PasswordUpdateInput",
  definition: (t) => {
    t.string("currentPassword", {required: true})
    t.string("newPassword", {required: true})
  }
})

export const ForgotPasswordEmailInput = inputObjectType({
  name: "ForgotPasswordEmailInput",
  definition: t => {
    t.string("email")
  }
})

export const ForgotPasswordUpdateInput = inputObjectType({
  name: "ForgotPasswordUpdateInput",
  definition: t => {
    t.string("newpassword"),
    t.string("verification_code")
  }
})

// -- CONTRACT INPUTS -- 
export const MilestoneInput = inputObjectType({
  name: 'MileStoneInput',
  definition: (t) => {
    t.float("target"),
    t.field("status", {
      type: "TransactionStateEnum",
      description: "Status represented by the TransactionStateEnum",
      required: false
    })
  }
})

export const AccountDetail = inputObjectType({
  name: "AccountNoInput",
  definition: t => {
    t.string("account_no")
    t.string("bank_code")
    t.id("contract_id", {required: true})
    t.string("bank_name")
    t.string("account_name")
  }
})

export const withdrawalRequestInput = inputObjectType({
  name: "WithdrawRequestInput",
  definition: t => {
    t.id("milestone_id")
    t.id("contract_id", {required: true})
    t.string("client_code")
    t.string("client_email")
    t.float("amount")
  }
})

export const ContractInput = inputObjectType({
  name: "ContractInput",
  definition: (t) => {
    t.string("name", {
      required: true
    }),
    t.float("amount", {required: true})
    t.string('description', {nullable: true}),
    t.string('duration_start', {required: true}),
    t.string('duration_end', {required: true}),
    t.field('status', {
      type: "ContractStateEnum",
      description: "The contract's state",
      required: false,
      default: 'INACTIVE'
    }),
    t.string('owner_type', {required: true}),
    t.string('payment_type', {required: true}),
    t.string('spread_type', {required: true})
    t.float('periodic_amount', {required: true}),
    t.float('whole_amount', {required: true}),
    t.string('periodic_type', {required: true}),
    t.list.field("milestones", { type: MilestoneInput}),
    t.string("client", {required: true}),
    t.string("client_email", {required: true}),
    t.string("client_phone", {required: true})
  }
})

export const TransactionInput = inputObjectType({
  name: "TransactionInput",
  definition: t => {
    t.string("contract", {nullable: false}),
    t.float("amount", {nullable: false}),
    t.field("status", {
      type: "TransactionStateEnum",
      description: "Status represented by the TransactionStateEnum"
    }),
    t.string("type", {nullable: false})
  }
})

export const MilestoneUpdateInput = inputObjectType({
  name: "MilestoneUpdateInput",
  definition: t => {
    t.id('contract_id', {required: true}),
    t.id('milestone_id', {required: true}),
    t.float("progress", {required: false}),
    t.field("state", {
      type: "MilestoneStateEnum",
      description: "Detects if the state of the project is completed or incomplete",
      required: false
    })
  }
})


export const MilestoneUpdateInputClient = inputObjectType({
  name: "MilestoneUpdateInputClient",
  definition: t => {
    t.id('contract_id', {required: true}),
    t.id('milestone_id', {required: true}),
    t.field("disburseStatus", {
      type: "DisburseStatusEnum",
      description: "Status of the disbursement",
      required: false
    }),
    t.field("status", {
      type: "TransactionStateEnum",
      description: "this set's the transaction state of the milestone PENDING, SUCCESSFUL, or FAILED ",
      required: false
    })
  }
})

export const ContractWholeInput = inputObjectType({
  name: "ContractWholeInput",
  definition: t => {
    t.id("contract_id", {required: true})
  }
})

export const ContractUpdateInput = inputObjectType({
  name: "ContractUpdateInput",
  definition: t => {
    t.id('contract_id', {required: true}),
    t.field('data', {type: "ContractInput"})
  }
})

export const ContractStateUpdate = inputObjectType({
  name: "ContractActivateInput",
  definition: t => {
    t.id("contract_id", {required: true}),
    t.float('amount',{nullable: true}),
    t.string('transactionCode'),
    t.field('contract_state', {
      type: "ContractStateEnum",
      required: true
    })
  }
})

export const ContractStateUpdateWithPay = inputObjectType({
  name: "ContractActivateWithPay",
  definition: t => {
    t.id("contract_id", {required: true})
    t.float('amount',{nullable: true})
    t.field('funding_type', {type: FundingTypeEnum, required: true})
    t.id('card_id', {required: false})
    // t.id('wallet_id', {required: false})
  }
})

export const ContractStateUpdateWithCard = inputObjectType({
  name: "ContractActivateWithCard",
  definition: t => {
    t.id("contract_id", {required: true})
    t.float('amount',{nullable: true})
    t.string('tx_ref', {required: false})
    // t.id('wallet_id', {required: false})
  }
})


//  ---- USER INFORMATION INPUTS -- 
export const userUpdateInput = inputObjectType({
  name: "UserUpdateInput",
  definition: t => {
    t.id("user_id", {required: true})
    t.string("first_name")
    t.string("last_name")
    t.string("phone_num")
  }
})

export const SMSVerificationInput  = inputObjectType({
  name: "SMSVerificationInput",
  definition: t => {
    t.string("code", {required: true})
    t.float('amount',{nullable: true})
  }
})

export const CardInput = inputObjectType({
  name: "CardInput",
  definition: t => {
    t.string("txRef", {required: true})
  }
})

export const CardFullInput = inputObjectType({
  name: "CardFullInput",
  definition: t => {
    t.string("txRef")
  }
})

export const ConfirmCodeInput = inputObjectType({
  name: "ConfirmCodeInput",
  definition: t => {
    t.string("code", {required: true})
  }
})

export const BankAccountInput = inputObjectType({
  name: "BankAccountInput",
  definition: t => {
    t.string('account_number', {required: true})
    t.string('bank_name', {required: true})
    t.string("account_name")
    t.string('bank_code')
  }
})

export const TopUpInput = inputObjectType({
  name: "TopupInput",
  definition: t => {
    t.id("card_id")
    t.string("tx_ref")
    t.field('card_type', {type: TopUpCardTypeEnum, required: true, default: "CARD"})
    t.float("amount", {required: true})
  }
})

export const WithdrawInput = inputObjectType({
  name: "WithdrawInput",
  definition: t => {
    t.id("bank_account_id")
    t.field('withdrawal_type', {type: WithdrawalTypeEnum, required: true, default: "NORMAL_WITHDRAWAL"})
    t.string('account_number')
    t.string('bank_name')
    t.string("account_name")
    t.string('bank_code')
    t.float("amount", {required: true})
  }
})

export const RetractionRequestInput = inputObjectType({
  name: "RetractionRequestInput",
  definition: t => {
    t.id("contract_id", {required: true})
    t.id("milestone_id")
  }
})

export const RetractionApprovalRequestInput = inputObjectType({
  name: "RetractionApprovalRequestInput",
  definition: t => {
    t.id("contract_id", {required: true})
    t.id("milestone_id"),
    t.field("approve_status", {type: RetractionApprovedEnum, required: true, default: "APPROVED"})
  }
})

export const CancelRetractionInput = inputObjectType({
  name: "CancelRetractionInput",
  definition: t => {
    t.id("contract_id", {required: true})
    t.id("milestone_id")
  }
})

export const BankResolveInput = inputObjectType({
  name: "BankResolveInput",
  definition: t => {
    t.string("account_no", {required: true}),
    t.string("account_bank", {required: true})
  }
})