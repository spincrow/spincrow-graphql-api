import User from "./User"
import Wallet from "./Wallet"
import Contract from "./Contract"
import Activity from "./Activity"
import Card from "./Card"
import BankAccount from "./BankAccount"
import {WalletTransaction, ContractTransaction} from "./Transaction"
import MileStone from "./MileStone"
import Query from "./query";
import Mutation from "./mutations"
import {AuthPayload, Upload, File, BankCode, ConfirmationResponse, Stats, ResolvedAccount, WalletUpdatedPayload} from "./extraTypes"
import Subscriptions from "./subscriptions"


export default [
    User,
    Contract,
    Activity,
    BankCode,
    Card,
    BankAccount,
    File,
    Upload,
    MileStone,
    WalletTransaction,
    ContractTransaction,
    ConfirmationResponse,
    Query,
    ResolvedAccount,
    Wallet,
    Subscriptions.walletUpdatedSubscription,
    Mutation,
    AuthPayload,
    Stats,
    WalletUpdatedPayload
]
