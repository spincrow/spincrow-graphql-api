import { prismaObjectType } from "nexus-prisma"

//@ts-ignore
const ContractTransaction = prismaObjectType({
    name: "ContractTransaction",
    definition: (t) => {
        t.prismaFields([
            'id',
            'amount',
            'contract',
            'flow',
            'details',
            'status',
            'created_at',
            'updated_at'
        ])
    }
})

const WalletTransaction = prismaObjectType({
    name: "WalletTransaction",
    definition: (t) => {
        t.prismaFields([
            'id',
            'amount',
            'wallet',
            'flow',
            'details',
            'status',
            'created_at',
            'updated_at'
        ])
    }
})

export {ContractTransaction, WalletTransaction}