import { verify } from "jsonwebtoken"
import {Context} from "./types"
import {v2} from "cloudinary"
import { FileUpload } from "graphql-upload"
import {File as FileType} from "./types"
import logger from "./services/logger"
// import * as fs from "fs"
import uuidGenerator from "uuid/v4";

export const APP_SECRET = process.env.JWT_SECRET

interface Token {
    userId: string
}

export function getUserId(context: Context){
    const Authorization = context.req.get('Authorization')
    if( Authorization ){
        const token = Authorization.replace('Bearer ', '')
        const verifiedToken = verify(token, APP_SECRET) as Token
        return verifiedToken && verifiedToken.userId
    }
}

export const uploader = async (file: any) : Promise<FileType> =>  {

    const {createReadStream, filename, mimetype, encoding } = await file;
    v2.config({
        cloud_name: process.env.CLOUDINARY_CLOUD, 
        api_key: process.env.CLOUDINARY_KEY, 
        api_secret: process.env.CLOUDINARY_SECRET
    })
    try{
        const result = <FileType> await new Promise(async (resolve, reject) => {
            let streamer = v2.uploader.upload_stream({public_id: `spincrow/${filename}`}, (error, result) => {
                if(error){
                    reject(error)
                }
                resolve({
                    id: result.public_id,
                    path: result.url,
                    filename: filename,
                    mimetype: mimetype,
                    encoding: encoding
                })
            })
            return createReadStream().pipe(streamer)
        })
        return result
    }catch(err){
        logger.error(err.message);
        throw err
    }
}

export const  generateUUID = () => {
    let generatedCode = uuidGenerator()
    const code = generatedCode.substring(generatedCode.length - 10);
    return ('TRANS_REF-'+code)
}

export const generateNormalUUID = () => {
    let generatedCode = uuidGenerator()
    const code = generatedCode.substring(generatedCode.length - 11);
    return code
}

export const generateBaseUUID = () => {
    // uuid.substring(uuid.length() - 16);
    let generatedUUIDBase = uuidGenerator()
    return generatedUUIDBase.substring(generateBaseUUID.length - 16);;
}

export const formatCurrency = (value: number) => {
    const options2 = { style: 'currency', currency: 'NGN' };
    const numberFormat2 = new Intl.NumberFormat('en-US', options2);

    return numberFormat2.format(value)
} 