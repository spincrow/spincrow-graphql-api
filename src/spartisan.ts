#!/usr/bin/env node
require('dotenv').config()
import  {generateAccountNoForWallet} from "./services/extra-jobs"
const {prisma} =require("./generated/prisma-client")
const chalk = require("chalk")
const clear = require("clear")
const figlet = require("figlet")
const path = require("path")
const program = require("commander").program

clear();
console.log(
  chalk.red(
    figlet.textSync('Spincrow-CLI', { horizontalLayout: 'full' })
  )
);

program
  .version('0.0.1')
  .description("The Cli for managing spincrow")
  .option('-p, --peppers', 'Add peppers')
  .option('-P, --pineapple', 'Add pineapple')
  .option('-b, --bbq', 'Add bbq sauce')
  .option('-c, --cheese <type>', 'Add the specified type of cheese [marble]')
  .option('-C, --no-cheese', 'You do not want any cheese')


program
  .command('GenerateWalletAccount <user_id>')
  .description('Generate wallet account for a user. ')
  .action(async (email: any) => {
    console.log(email);
    try {
        if(email){
            let users = await prisma.users({where: {email: email}}).$fragment(`{
                id
                wallet {
                    id
                }
            }`)
            if(users[0]){
                await generateAccountNoForWallet({
                    wallet_id: users[0].wallet.id,
                    user_id: users[0].id,
                    email: email
                })
                console.log("DONE")
            }
        }
    } catch (e) {
        console.log(e.message)
    }
  });


program.parse(process.argv);